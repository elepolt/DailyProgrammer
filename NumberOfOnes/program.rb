require 'byebug'
def brute_force(number)
    total_ones = 0
    (0..number).each { |n| total_ones += n.to_s.count('1') }

    total_ones
end

# Ok. Now let's math
def count_ones(number)
    # Get our digits into an array
    digits = number.to_s.split('').map(&:to_i)
    digit_length = digits.length
    power = digit_length - 1 # Our pow is always one less than the length of digits we're working with
    power_minus_one = power - 1 # Also weird, but a lot of time we want the likes of 999, which is 10**3 -, hence 10**2
    max_number = (10 ** (power) * digit_length )
    digits.each_with_index do |digit, idx|
        power = digit_length - idx - 1
        last_digits = digit_length - idx - 1
        if digit == 1
            max_number -= (10**power_minus_one) - (digits.last(last_digits).join('').to_i)
            max_number -= (9-digit) * (10 ** (power_minus_one) * power)
        elsif digit == 0
            max_number -= solve_zeroes(digits.last(last_digits+1))
        else
            max_number -= (9-digit) * (10 ** (power_minus_one) * power)
        end
    end

    max_number.to_i
end

# This is a weird way of thinking, but bear with me. For a number like 902 we want to take 999, then subtract 90
# but the way we're doing this our array comes in as something like [0, 9]. Meaning we're on the power of 1.
def solve_zeroes(digits)
    total = 0
    # 90 = 99 - 9
    # (10**1 * 2) - (10**0 * 1)
    digit_length = digits.length
    power = digit_length - 1
    if digit_length == 1
        total += 1 if digits[0] == 0
    else
        total += (10**power * digit_length) - ((10**(power - 1)) * (digit_length - 1))
    end

    total
end


require "minitest/autorun"
class TestProgram < Minitest::Test
    def test_zeros
        assert_equal 19, solve_zeroes([0,1])
        assert_equal 280, solve_zeroes([0,0,9])
    end

    def test_program
        assert_equal 1, count_ones(1)
        assert_equal 1, count_ones(5)
        assert_equal 2, count_ones(10)
        assert_equal 12, count_ones(20)
        assert_equal 20, count_ones(99)
        assert_equal 300, count_ones(999)
        assert_equal 280, count_ones(899)
        assert_equal 279, count_ones(889)
        assert_equal 689, count_ones(1234)
        assert_equal 1589, count_ones(1989)
        assert_equal 1600, count_ones(1999)
        assert_equal 1900, count_ones(2999)
        assert_equal 1480, count_ones(1899)
        assert_equal 3400, count_ones(7999)
        assert_equal 2557, count_ones(5123)
        assert_equal 93395, count_ones(123321)
        assert_equal 31, count_ones(109)
        assert_equal 160, count_ones(299)
        assert_equal 281, count_ones(909)
        assert_equal 141, count_ones(209)
        assert_equal 140, count_ones(199)
        assert_equal 1601, count_ones(2001)

        assert_equal 38000, count_ones(70000)
        assert_equal 90051450678399649, count_ones(3**35)
    end
end



=begin
Adding all of this so you can see my psychotic math

9    == 1      (10**0 * 1)
90   == 19     (10**1 * 2) - (10**0 * 1)
99   == 20     (10**1 * 2)
89   == 19     (10**1 * 2) - (9-8)*(10**0 * 1)
999  == 300    (10**2 * 3)
109  == 31     (10**2 * 3) - (9-1)*(10**1 * 2) - (10**1 * 2) - (10**0 * 1)
209  == 141    (10**2 * 3) - (9-2)*(10**1 * 2) - ((10**1 * 2) - (10**0 * 1)) # 999 - 7 iterations  of 99, - 90
199  == 140    (10**2 * 3) - (9-1)*(10**1 * 2)
899  == 280    (10**2 * 3) - (9-8)*(10**1 * 2)
900  == 280    (10**2 * 3) - (10**1 * 2)
799  == 280    (10**2 * 3) - (2)*(10**1 * 2)
879  == 279    (10**2 * 3) - (9-8)*(10**1 * 2) - (9-7)*(10**0 * 1)



9999 == 4000   (10**3 * 4)
9000 == 3700   (10**3 * 4) - (10**2 * 3)
8999 == 3700   (10**3 * 4) - (9-8)*(10**2 * 3)
7999 == 3400   (10**3 * 4) - (9-7)*(10**2 * 3)
2999 == 1900   (10**3 * 4) - (9-2)*(10**2 * 3)
1999 == 1600   (10**3 * 4) - (9-1)*(10**2 * 3)
1899 == 1480   (10**3 * 4) - (9-1)*(10**2 * 3) - (9-8)*(10**1 * 2) - ((10**3 - 1) - 899)
1989 == 1600   (10**3 * 4) - (9-1)*(10**2 * 3) - (9-8)*(10**0 * 1) 


1234 == 689
    (10**3 * 4) - (9-1)*(10**2 * 3) - (9-2)*(10**1 * 2) - (9-3)*(10**0 * 1) - 1


70000
    (10**4 * 5) - (10**3 * 4) - (10**2 * 3) - (10**1 * 2)


9000
9999 - 900 - 90 - 9
(10**3 * 4)

(10**3 * 4) - ((10**2 * 3) - (10**1 * 2) - (10**1 * 2) - (10**0 * 1) - (10**0 * 1))


900



# 909 = 999 - (99 - 9) - 9
# (10**2 * 3) - (9)*(10**1 * 2) - ((10**1 * 2) - (10**0 * 1))



1999 == 1600   (10**3 * 4) - (9-1)*(10**2 * 3)

2001 == 1601   (10**3 * 4) - (9-2)*(10**2 * 3)

2999
    
- 900
    (10**2 * 3) - (10**1 * 2)
- 90
    (10**1 * 2) - (10**0 * 1)

(10**3 * 4) - (9-2)*(10**2 * 3) - ((10**2 * 3) - (10**1 * 2)) - ((10**1 * 2) - (10**0 * 1))
=end





