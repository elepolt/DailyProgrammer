# frozen_string_literal: true

require 'byebug'
def anagram(input)
  first = input[0].gsub(/[^0-9A-Za-z]/, '').downcase.split('').sort
  second = input[1].gsub(/[^0-9A-Za-z]/, '').downcase.split('').sort
  first == second
end

require 'minitest/autorun'
class TestProgram < Minitest::Test
  def test_1
    assert_equal true, anagram(['wisdom', 'mid sow'])
    assert_equal true, anagram(['Seth Rogan', 'Gathers No'])
    assert_equal false, anagram(['Reddit', 'Eat Dirt'])
    assert_equal true, anagram(['Schoolmaster', 'The classroom'])
    assert_equal false, anagram(['Astronomers', 'Moon starer'])
    assert_equal true, anagram(['Vacation Times', "I'm Not as Active"])
    assert_equal false, anagram(['Dormitory', 'Dirty Rooms'])
  end
end
