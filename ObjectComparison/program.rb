'''
Json "Path":
{
  y: 10,
  z: {t: 12}
  a: {
    b: [
      4
    ]
  }
}

y = 10
z.t = 12
a.b[0] = 4



Part 1: Basic objects (no nesting)

{                               {
  "a" : 5,                        "a" : 8,
  "b" : 6,                        "b" : { "x" : 2 },
  "c" : 5                         "d" : 8
}                               }

a
< 5
> 8

b
< 6
> { "x" : 2 }

c
< 5

d
> 8

// If types are different OR both are primitive and value is different, print
// Things in left but not right
// Things in right but not left


Part 2: Objects with nesting

{                               {
  "foo" : {                       "foo" : {
    "bar" : 14,                     "bar" : 13,
  }                               }
}                               }

foo.bar
< 14
> 13

// Print full "path" from root json till the actual difference



Part 3: Lists

{                               {
  "foo" : {                       "foo" : {
    "bar" : [ 14, {t: 15} ],        "bar" : [ 13, {t: 18}, 19 ],
  }                               }
}                               }

foo.bar[0]
< 14
> 13

foo.bar[1].t
< 15
> 18

foo.bar[2]
> 19

// Compare by indices between left & right.
// Elements are JSON.
// Reminder: there can be objects and lists within a list

'''

j1 = {
  "a" => 5,
  "b" => 6,
  "c" => 5 
}         

j2 = {
  "a" => 8,
  "b" => { "x" => 2 },
  "d" => 8
}

j1nest = {
  "outer" => { "o2" => j1},
  "not nest" => 5
}


j2nest = {
  "outer" => {"o2" => j2},
  "not nest" => 6,
  "miss" => {"f" => 6}
}

require 'pp'
require 'set'

def keyDifferences(a,b, keyPath)
  aKeys = a.keys.to_set
  bKeys = b.keys.to_set
  aToBKeyDifferences = aKeys - bKeys
  
  aToBKeyDifferences.each do |k|
    keyOutput = ''
    keyOutput += "#{keyPath}." if !keyPath.nil?
    puts "#{keyOutput}#{k}"
    puts "< #{a[k]}"
  end
end

def compareKeyObjectValues(a, b, keyPath=nil)
  keyDifferences(a,b, keyPath)
  keyDifferences(b,a, keyPath)

  # First run thorugh will be 'outer'
  similarKeys = a.keys.to_set & b.keys.to_set
  similarKeys.each do |key|
    if a[key].is_a?(Hash) && b[key].is_a?(Hash)
      keyOutput = ''
      keyOutput += "#{keyPath}." if !keyPath.nil?
      compareKeyObjectValues(a[key], b[key], keyOutput+key)
    else
      if a[key] != b[key]
        keyOutput = ''
        keyOutput += "#{keyPath}." if !keyPath.nil?
        output = "#{keyOutput}#{key}
                  < #{a[key]}
                  > #{b[key]}"
        # puts "#{keyOutput}#{key}"
        # puts "< #{a[key]}"
        # puts "> #{b[key]}"
        return output
      end
    end
  end
end
# compareKeyObjectValues(j1, j2)
# puts "\n"
# compareKeyObjectValues(j1nest, j2nest)

require "minitest/autorun"
require 'byebug'
class TestProgram < Minitest::Test
  def test_zero
    j1 = {
      "a" => 5,
      "b" => 6,
      "c" => 5 
    }         

    j2 = {
      "a" => 8,
      "b" => { "x" => 2 },
      "d" => 8
    }
    final_value = 'a
              < 5
              > 8

              b
              < 6
              > { "x" : 2 }

              c
              < 5

              d
              > 8'
    byebug
    assert_equal final_value, compareKeyObjectValues(j1, j2)
  end
  
end


