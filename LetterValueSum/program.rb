# frozen_string_literal: true

require 'byebug'
def lettersum(chararcters)
  words = {}

  sum = 0
  # Probably a better way to do this? I dunno, this gets the alphabet as a dictionary with keys a-z matching values 1-26
  alphabet = (10...36).map { |i| [(i.to_s 36), i - 9] }.to_h
  chararcters_array = chararcters.split('')
  chararcters_array.each do |char|
    sum += alphabet[char]
  end
  sum
end

def lettersum_challenges
  sum_319 = ''
  odd_sums = 0
  sum_dict = {}
  eleven_letters = []
  File.readlines('dictionary.txt').each do |line|
    # byebug
    sum = lettersum(line.chomp)
    # spit out the word which has sum of 319
    sum_319 = line if sum == 319

    # Keep track of odd sums
    odd_sums += 1 if sum.odd?

    # Get a dict of every occurance of sums
    sum_dict[sum] = 0 unless sum_dict.keys.include?(sum)
    sum_dict[sum] += 1

    # if
  end

  puts("319: #{sum_319}")
  puts("Odd occurences: #{odd_sums}")
  # Sorts dict by descending value and prints the key
  sorted = sum_dict.sort_by { |_k, v| -v }
  puts("Most common number: #{sorted[0][0]}, #{sorted[0][1]} times")
end

lettersum_challenges

require 'minitest/autorun'
class TestProgram < Minitest::Test
  def test_1
    assert_equal 0, lettersum('')
    assert_equal 1, lettersum('a')
    assert_equal 26, lettersum('z')
    assert_equal 6, lettersum('cab')
    assert_equal 100, lettersum('excellent')
    assert_equal 317, lettersum('microspectrophotometries')
  end
end
