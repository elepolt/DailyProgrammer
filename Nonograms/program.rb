def nonogramrow(row)
    # Join the row to get a string of numbers
    # Split it on the zero, and remove any empty values
    ones = row.join('').split('0') - ['']
    # Map with a count of 1s of the remaining elements
    ones.map { |one_array| one_array.count('1') }
end

require "minitest/autorun"
class TestProgram < Minitest::Test
    def test_1
        assert_equal nonogramrow([]), []
    end
    def test_2
        assert_equal nonogramrow([0,0,0,0,0]), []
    end
    def test_3
        assert_equal nonogramrow([1,1,1,1,1]), [5]
    end
    def test_4
        assert_equal nonogramrow([0,1,1,1,1,1,0,1,1,1,1]), [5,4]
    end
    def test_5
        assert_equal nonogramrow([1,1,0,1,0,0,1,1,1,0,0]), [2,1,3]
    end
    def test_6
        assert_equal nonogramrow([0,0,0,0,1,1,0,0,1,0,1,1,1]), [2,1,3]
    end
    def test_7
        assert_equal nonogramrow([1,0,1,0,1,0,1,0,1,0,1,0,1,0,1]), [1,1,1,1,1,1,1,1]
    end
end