require "minitest/autorun"
require_relative "base"

class TestProgram < Minitest::Test
  def setup
    @parsed_array = [
      [2,1,9,9,9,4,3,2,1,0],
      [3,9,8,7,8,9,4,9,2,1],
      [9,8,5,6,7,8,9,8,9,2],
      [8,7,6,7,8,9,6,7,8,9],
      [9,8,9,9,9,6,5,6,7,8],
    ]
  end

  def test_code
    # Your regular 'ol get neighbors

    # XOX
    # XXX
    assert_equal [[0, 0], [0, 2], [1, 0], [1, 1], [1, 2]], traverse(@parsed_array, [0, 1])

    # Get neighbors and one more space in each direction

    # OXOO
    # OOO.
    # .O.O
    assert_equal [[0, 0], [0, 2], [0, 3], [1, 0], [1, 1], [2, 1], [1, 2], [2, 3]], traverse(@parsed_array, [0, 1], 1)

    # Get neighbors but only in the cardinal directions
    assert_equal [[0, 0], [0, 2], [1, 1]], traverse(@parsed_array, [0, 1], 0, false)
  end
end
