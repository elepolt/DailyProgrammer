from functools import reduce
import collections
from itertools import combinations
from itertools import product
import re
from datetime import datetime
from datetime import timedelta
import operator
import numpy
import copy

def import_items(file_location):
  # splitlines removes the \n
  return open(file_location, 'r').read().splitlines()

def convert_items_to_int_list(items):
  return list(map(int, items))

# Simply sum all frequencies
def frequency(frequencies):
  return reduce((lambda x, y: x + y), frequencies)

# Loop over inputs until a frequency is hit twice
def frequency_count(frequencies, current_frequency, frequency_count_list):
  for frequency in frequencies:
    current_frequency += frequency
    if current_frequency in frequency_count_list:
      return current_frequency
    frequency_count_list.add(current_frequency)
  return frequency_count(frequencies, current_frequency, frequency_count_list)

def checksum(checksum_list, checksum_checks):
  # checksum_checks == {2:0, 3:0}
  # Look through each string to build the dict
  for checksum_id in checksum_list:
    char_dict = collections.Counter(checksum_id)
    for key in checksum_checks.keys():
      if key in char_dict.values():
        checksum_checks[key] += 1
  return reduce((lambda x, y: x * y), checksum_checks.values())

import pdb
def overlapping(fabric_inputs):
  final_area = 0
  existing_coords = {}
  intact_coords = {}
  # I should really learn regex...
  fabric_claims = map(lambda s: map(int, re.findall(r'-?\d+', s)), fabric_inputs)
  for (id, padding_left, padding_top, square_width, square_height) in fabric_claims:

    # Get starting and ending width/height
    left_start = padding_left + 1
    left_end = left_start + square_width
    top_start = padding_top + 1
    top_end = top_start + square_height

    area_width = range(left_start, left_end)
    area_height = range(top_start, top_end)
    has_overlapped = False
    # Build a dict of all the coords, keep track how often they appear
    for coords in list(product(area_width, area_height)):
      if not coords in existing_coords:
        existing_coords[coords] = 0
      else:
        has_overlapped = True
      existing_coords[coords] += 1

    # Keep track of which coords were entered that did not overlap existing coords
    if not has_overlapped:
      intact_coords[id] = list(product(area_width, area_height))
  # find all the coords that have more than 1 hit
  d = dict((k, v) for k, v in existing_coords.items() if v >= 2)
  final_area = len(d)

  # Loop through the intact ids
  intact_id = None
  for id, coords in intact_coords.items():
    intact = True
    for coord in coords:
      # Check if they were overwritten at some point
      if existing_coords[coord] > 1:
        intact = False
    if intact:
      intact_id = id
  return [final_area, intact_id]


# Not a fan of this one.... would like to refactor it
def sleep_counter(guard_inputs):
  guard_time_dict = {}
  running_time_dict = {}
  # Loop through all inputs
  for guard_note in guard_inputs:
    time = datetime.strptime(guard_note.split(']')[0][1:], "%Y-%m-%d %H:%M")
    info = guard_note.split(']')[1].strip()
    # Create the time keyed into the dictionary
    guard_time_dict[time] = {}
    
    guard_id_match = re.findall(r'\d+', info)
    guard_id = None
    # If the info contains an id
    if len(guard_id_match) > 0:
      guard_id = int(guard_id_match[0])
      # Include that id with the time
      guard_time_dict[time]['id'] = guard_id
      if guard_id not in running_time_dict.keys():
        running_time_dict[guard_id] = {}
        running_time_dict[guard_id]['time_asleep'] = 0
        running_time_dict[guard_id]['asleep_minutes'] = []

    guard_time_dict[time]['info'] = info

  # So now we have a dictionary keyed on time.
  # Each time contains ['info']
  # And some contain ['id']

  # Keeps track of who slept the most
  max_time_asleep = 0
  # Will hold the id of who slept the most
  guard_id = None
  # Holds the id of the guard in the for-loop
  running_guard_id = None
  # Holds the time that the guard fell asleep
  falls_asleep = None
  for time, guard_info in sorted(guard_time_dict.items()):
    # If id exists we need to reset the guard_id and running_time_asleep
    if 'id' in guard_info.keys():
      if running_guard_id is not None and running_time_dict[running_guard_id]['time_asleep'] > max_time_asleep:
        max_time_asleep = running_time_dict[running_guard_id]['time_asleep']
        guard_id = running_guard_id
      running_guard_id = guard_info['id']
    else:
      if guard_info['info'] == 'wakes up':
        seconds = time - falls_asleep
        running_time_dict[running_guard_id]['time_asleep'] += int(seconds.seconds/60)
        # Keep track of all times the guard was asleep
        running_time_dict[running_guard_id]['asleep_minutes'].append(range(falls_asleep.minute, time.minute))
      else:
        # Note when guard fell asleep
        falls_asleep = time

  most_slept_minute = get_most_slept_minute(running_time_dict[guard_id]['asleep_minutes'])[0]
  part_1_answer = guard_id * most_slept_minute

  all_slept_minutes = {}
  for guard_id, guard_info in running_time_dict.items():
    all_slept_minutes[guard_id] = get_most_slept_minute(guard_info['asleep_minutes'])

  guard_id = None
  minute = 0
  minute_count = 0
  for running_guard_id, minute_info in all_slept_minutes.items():
    if minute_info[1] > minute_count:
      guard_id = running_guard_id
      minute = minute_info[0]
      minute_count = minute_info[1]

  part_2_answer = guard_id * minute
  return [part_1_answer, part_2_answer]

def get_most_slept_minute(time_ranges):
  most_slept_minute = 0
  minute_count = 0
  minutes_asleep_collection = collections.Counter()
  if len(time_ranges) > 0:
    for time_range in time_ranges:
      minutes_asleep_collection += collections.Counter(time_range)
    most_slept_minute = minutes_asleep_collection.most_common(1)[0][0]
    minute_count = minutes_asleep_collection.most_common(1)[0][1]
  return [most_slept_minute, minute_count]


# So this is correct, but also not correct.
# Correct in that it does return the common characters,
# but incorrect in that they're out of order and that order
# matters for the solution
def unique_check(checksum_list):
  for checksum, comparison in combinations(checksum_list, 2):
    checksum_dict = collections.Counter(checksum)
    comparison_dict = collections.Counter(comparison)
    common = checksum_dict & comparison_dict
    common_chars = ''.join(common.elements())
    if len(common_chars) == len(checksum) - 1:
      # my answer: umdryabviapkozistwcnihjqx
      return common_chars

def alchemical(chemical_string):
  # This solution is awesome
  # Initialize this stack
  stack = []
  # Loop through the string
  for chemical in chemical_string:
    # We check if the current chemical matches the previous chemical, but with the case swapped
    # -1 loops to the end of an array
    if stack and chemical == stack[-1].swapcase():
      # So if the current is the swapcased version of the last
      # simply remove the last
      stack.pop()
    else:
      # else append and we don't need to worry about what's coming next
      stack.append(chemical)
  return len(stack)

def alchemical_destruction(chemical_string):
  char_list = numpy.array(list(chemical_string.lower()))
  unique_chars = numpy.unique(char_list)

  min_polymer = None
  for char in unique_chars:
    # new_chemical_string = chemical_string.replace(char, "").replace(char.upper(), "")
    new_chemical_string = [c for c in chemical_string if c!=char.lower() and c!=char.upper()]
    length = alchemical(new_chemical_string)
    if min_polymer is None or length < min_polymer:
      min_polymer = length
  
  return min_polymer

def chronal_coords(coords_input):
  coords = map(lambda s: map(int, re.findall(r'-?\d+', s)), coords_input)
  P = []
  for coord_x, coord_y in coords:
    P.append((coord_x, coord_y))

  xlo = min([x for x,y in P])
  xhi = max([x for x,y in P])
  ylo = min([y for x,y in P])
  yhi = max([y for x,y in P])

  S2 = score_around(40, xlo, xhi, ylo, yhi, P)
  S3 = score_around(60, xlo, xhi, ylo, yhi, P)

  best = [(S2[k] if S2[k]==S3[k] else 0, k) for k in S2.keys()]
  best.sort()

  for area, p in best:
    print (area, p)

  return best[-1]

def closest(x,y, P):
  ds = [(distance(p, (x,y)), p) for p in P]
  ds.sort()
  if ds[0][0] < ds[1][0]:
    return ds[0][1]
  else:
    return (-1,-1)

def score_around(W, xlo, xhi, ylo, yhi, P):
  score = collections.defaultdict(int)
  for x in range(xlo-W, xhi+W):
    for y in range(ylo-W, yhi+W):
      score[closest(x,y, P)] += 1
  return score

def distance(p1, p2):
  # pdb.set_trace()
  return abs(p1[0]-p2[0]) + abs(p1[1]-p2[1])

def step_completion(step_input):
  steps = collections.defaultdict(list)
  dependents = collections.defaultdict(int)
  for step in step_input:
    words = step.split()
    x = words[1].upper()
    y = words[7].upper()
    steps[x].append(y)
    dependents[y] += 1

  Q = []
  for x in steps:
    if dependents[x] == 0:
      Q.append(x)

  answer = ''
  while Q:
    x = sorted(Q)[0]
    Q = [y for y in Q if y != x]
    answer += x
    for y in steps[x]:
      dependents[y] -= 1
      if dependents[y] == 0:
        Q.append(y)

  return answer

def get_next_steps(step, steps, dependents):
  # step: A
  next_steps = list(reversed(sorted(steps[step]))) # 'd' 'b'
  # pdb.set_trace()
  next_step = next_steps.pop() # 'b'
  if len(dependents[next_step]) == 1:
    return next_step
  else:
    return get_next_steps(next_step, steps, dependents)

if __name__ == "__main__":
  # Day 1
  # frequencies = convert_items_to_int_list(import_items('inputs/first_input.txt'))
  # print(frequency(frequencies))
  # print(frequency_count(frequencies, 0, set({0})))

  # # Day 2
  # checksum_list = import_items('inputs/second_input.txt')
  # print(checksum(checksum_list, {2:0,3:0}))
  # print(unique_check(checksum_list))

  # # Day 3
  # fabric_inputs = import_items('inputs/third_input.txt')
  # print(overlapping(fabric_inputs))

  # guard_inputs = import_items('inputs/fourth_input.txt')
  # print(sleep_counter(guard_inputs))

  # chemical_string = open('inputs/fifth_input.txt', 'r').read().strip('\n')
  # print(alchemical(chemical_string))

  # chemical_string = open('inputs/fifth_input.txt', 'r').read().strip('\n')
  # print(alchemical_destruction(chemical_string))
  
  # coords = import_items('inputs/sixth_input.txt')
  # print(chronal_coords(coords))

  steps = import_items('inputs/seventh_input.txt')
  print(step_completion(steps))