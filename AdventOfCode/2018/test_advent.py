import advent_of_code

def test_convert_items_to_int_list():
  assert advent_of_code.convert_items_to_int_list(['1', '2']) == [1,2]

def test_frequency():
  assert advent_of_code.frequency([+1, +1, +1]) == 3
  assert advent_of_code.frequency([+1, +1, -2]) == 0
  assert advent_of_code.frequency([-1, -2, -3]) == -6

def test_frequency_count():
  assert advent_of_code.frequency_count([+1, -1], 0, set({0})) == 0
  assert advent_of_code.frequency_count([+3, +3, +4, -2, -4], 0, set({0})) == 10
  assert advent_of_code.frequency_count([-6, +3, +8, +5, -6], 0, set({0})) == 5
  assert advent_of_code.frequency_count([+7, +7, -2, -7, -4], 0, set({0})) == 14

def test_checksum():
  checksum_list = ['abcdef','bababc','abbcde','abcccd','aabcdd','abcdee','ababab']
  assert advent_of_code.checksum(checksum_list, {2:0, 3:0}) == 12

def test_unique_check():
  checksum_list = ['abcde', 'fghij', 'klmno', 'pqrst', 'fguij', 'axcye', 'wvxyz']
  assert advent_of_code.unique_check(checksum_list) == 'fgij'

def test_overlapping():
  inputs = ['#1 @ 1,3: 4x4', '#2 @ 3,1: 4x4', '#3 @ 5,5: 2x2']
  assert advent_of_code.overlapping(inputs) == [4, 3]

def test_sleep_counter():
  guard_inputs = ['[1518-11-01 00:00] Guard #10 begins shift', '[1518-11-01 00:05] falls asleep', '[1518-11-01 00:25] wakes up', '[1518-11-01 00:30] falls asleep', '[1518-11-01 00:55] wakes up', '[1518-11-01 23:58] Guard #99 begins shift', '[1518-11-02 00:40] falls asleep', '[1518-11-02 00:50] wakes up', '[1518-11-03 00:05] Guard #10 begins shift', '[1518-11-03 00:24] falls asleep', '[1518-11-03 00:29] wakes up', '[1518-11-04 00:02] Guard #99 begins shift', '[1518-11-04 00:36] falls asleep', '[1518-11-04 00:46] wakes up', '[1518-11-05 00:03] Guard #99 begins shift', '[1518-11-05 00:45] falls asleep', '[1518-11-05 00:55] wakes up']
  assert advent_of_code.sleep_counter(guard_inputs) == [240, 4455]

def test_alchemical():
  assert advent_of_code.alchemical("dabAcCaCBAcCcaDA") == 10
  assert advent_of_code.alchemical("dabAcCaCBAcCcaDAa") == 9
  assert advent_of_code.alchemical("cCCccCCcCc") == 0

def test_alchemical_destruction():
  assert advent_of_code.alchemical_destruction("dabAcCaCBAcCcaDA") == 4
  assert advent_of_code.alchemical_destruction("dabAcCaCBAcCcaDAa") == 3

def test_chronal():
  coords = [
    '1, 1',
    '1, 6',
    '8, 3',
    '3, 4',
    '5, 5',
    '8, 9'
  ]
  assert advent_of_code.chronal_coords(coords) == (17, (5,5))

def test_step_completion():
  step_input = [
    'Step C must be finished before step A can begin.',
    'Step C must be finished before step F can begin.',
    'Step A must be finished before step B can begin.',
    'Step A must be finished before step D can begin.',
    'Step B must be finished before step E can begin.',
    'Step D must be finished before step E can begin.',
    'Step F must be finished before step E can begin.'
  ]
  assert advent_of_code.step_completion(step_input) == 'CABDFE'