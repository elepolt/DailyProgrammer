require 'byebug'
require 'pp' # Just in case
require 'set'
require_relative 'board'

# All the lines are coming in with /n at the end, so strip that off
def read_lines(file_name)
  File.readlines(file_name).map(&:strip)
end

# Takes in the file, and returns each line as an integer
def parse_intengers(array)
  array.map(&:to_i)
end

def split_string(string, delimiter)
  string.split(delimiter)
end

def build_board(array)
  board = []
  array.each do |row|
    board.append(row.split(''))
  end
  board
end

def traverse(grid, starting_coords, distance = 0, include_diagonals = true)
  neighbors = []
  # This neat nested loop traverses the surrounding box of a given index
  [-1, 0, 1].each do |idx|
    [-1, 0, 1].each do |idy|
      next if idx.zero? && idy.zero? # skip if it's the starting_coords
      next if (idx == idy || idx == -idy) && !include_diagonals # Skips diagonals

      x = starting_coords[0] + idx
      y = starting_coords[1] + idy
      # Skip if we're at an edge
      next if x.negative? || x >= grid.length
      next if y.negative? || y >= grid[0].length

      neighbors.push([x, y])
      # This is where Game of Life ultimately ends,

      # If we have a distance given to us we just continue going in whichever direction we were going
      distance.times do
        # Continue adding our idx/y value from above, which will move
        # us neatly in all directions
        x += idx
        y += idy
        # Break when we get to an edge
        break if x.negative? || x >= grid.length
        break if y.negative? || y >= grid[0].length

        neighbors.push([x, y])
      end
    end
  end
  neighbors
end

def out_of_range(board, coord)
  coord[0].negative? || coord[1].negative? || coord[0] >= board[0].length || coord[1] >= board[0].length
end

class String
  def is_integer?
    self.to_i.to_s == self
  end
end
