# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '########',
      '#..O.O.#',
      '##@.O..#',
      '#...O..#',
      '#.#.O..#',
      '#...O..#',
      '#......#',
      '########',
      '',
      '<^^>>>vv<v>>v<<'
    ]
    @test2 = [
      '##########',
      '#..O..O.O#',
      '#......O.#',
      '#.OO..O.O#',
      '#..O@..O.#',
      '#O#..O...#',
      '#O..O..O.#',
      '#.OO.O.OO#',
      '#....O...#',
      '##########',
      '',
      '<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^',
      'vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v',
      '><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<',
      '<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^',
      '^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><',
      '^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^',
      '>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^',
      '<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>',
      '^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>',
      'v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^',
    ]
  end

  def test_code
    # board_lines = get_board(@raw_input)
    # # pp board_lines
    # board = Board.new(board_lines, false)
    # directions = get_directions(@raw_input)
    # assert_equal [2, 2], starting_position(board)
    # move_boxes(board, [1, 3], right)
    # assert_equal '.', board.cells[[1, 3]]
    # assert_equal 'O', board.cells[[1, 4]]
    # move_boxes(board, [1, 4], right)
    # assert_equal '.', board.cells[[1, 4]]
    # assert_equal 'O', board.cells[[1, 5]]
    # assert_equal 'O', board.cells[[1, 6]]

    # assert_equal [2028, nil], program(@raw_input)
    # board_lines = get_board(@test2)
    # board = Board.new(board_lines, false)
    assert_equal [10_092, nil], program(@test2)
  end
end
