# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  board = Board.new(get_board(array), false)
  directions = get_directions(array)
  coord = starting_position(board)
  board.cells[coord] = '.'
  directions.each do |direction|
    next_coord = coord_addition(coord, dirs[direction])
    next if board.cells[next_coord] == '#'

    if board.cells[next_coord] == '.'
      coord = next_coord
      next
    end

    # Ok... easy part is done
    coord = next_coord if move_boxes(board, next_coord, dirs[direction])
  end
  ans1 = 0
  board.cells.each do |coord, cell|
    next unless cell == 'O'

    ans1 += 100 * coord[0] + coord[1]
  end
  [ans1, nil]
end

def move_boxes(board, coord, direction)
  # step in that direction until you find an empty space
  boxes = [coord]
  loop do
    next_coord = coord_addition(coord, direction)
    return false if board.cells[next_coord] == '#'

    if board.cells[next_coord] == 'O'
      boxes.append(next_coord)
      coord = next_coord
      next
    end

    pp 'goofed' unless board.cells[next_coord] == '.'

    board.cells[next_coord] = 'O'
    board.cells[boxes[0]] = '.'
    return true
  end

  false
end

def get_board(array)
  board = []
  array.each do |line|
    # pp line
    return board if line == ''

    board.append(line)
  end
end

def get_directions(array)
  directions = []
  array.each do |line|
    next if line == '' || line[0] == '#'

    directions += line.split('')
  end
  directions
end

def starting_position(board)
  board.cells.each do |coord, cell|
    return coord if cell == '@'
  end
end

def dirs
  { '^' => up, 'v' => down, '>' => right, '<' => left }
end

# 1512818 too low
puts "Part 1/2: #{program(input_array)}"
