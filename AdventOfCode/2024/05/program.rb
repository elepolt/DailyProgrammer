# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  rules, updates = parse_input(array)
  ans1 = 0
  incorrect_updates = []
  updates.each do |update|
    if check_update(update, rules)
      ans1 += update[update.length / 2]
      next
    end
    incorrect_updates.append(update)
  end
  ans2 = 0
  incorrect_updates.each do |update|
    update = fix_update(update, rules)
    ans2 += update[update.length / 2]
  end
  [ans1, ans2]
end

def parse_input(array)
  rules = {}
  updates = []
  updating = false
  array.each do |rule_str|
    if rule_str == ''
      updating = true
      next
    end
    if updating
      updates.append(rule_str.split(',').map(&:to_i))
      next
    end
    pages = rule_str.split('|').map(&:to_i)
    rules[pages[0]] = [] unless rules[pages[0]]
    rules[pages[0]].append(pages[1])
  end
  [rules, updates]
end

def check_update(update, rules)
  (0..update.length - 2).each do |idx|
    current = update[idx]
    idy = idx + 1
    update[idy..].each do |page|
      next if rules[page].nil?
      return false if rules[page].include?(current)
    end
  end
  true
end

# lol, brute force-ish? I just fix one number at a time and repeat the process until it's fixed
# A smarter way is to start from scratch and place each individual page where it can go
def fix_update(update, rules)
  25.times do
    return update if check_update(update, rules)

    update = fix_update_once(update, rules)
  end
  update
end

def fix_update_once(update, rules)
  (0..update.length - 2).each do |idx|
    current = update[idx]
    idy = idx + 1
    update[idy..].each_with_index do |page, idj|
      next if rules[page].nil?
      next unless rules[page].include?(current)

      new_idy = find_correct_location(update[idx..], rules, current)
      update.delete_at(idx)
      update.insert(idx + new_idy, current)
      return update
    end
  end
  update
end

def find_correct_location(update, rules, page)
  new_options = []
  (0..update.length - 1).each do |idx|
    next if rules[update[idx]].nil?

    new_options.append(idx) if rules[update[idx]].include?(page)
  end
  new_options.last
end
# 4375 too low
puts "Part 1/2: #{program(input_array)}"
