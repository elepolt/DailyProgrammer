# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '47|53',
      '97|13',
      '97|61',
      '97|47',
      '75|29',
      '61|13',
      '75|53',
      '29|13',
      '97|29',
      '53|29',
      '61|53',
      '97|53',
      '61|29',
      '47|13',
      '75|47',
      '97|75',
      '47|61',
      '75|61',
      '47|29',
      '75|13',
      '53|13',
      '',
      '75,47,61,53,29',
      '97,61,53,29,13',
      '75,29,13',
      '75,97,47,61,53',
      '61,13,29',
      '97,13,75,29,47'
    ]
  end

  def test_code
    rules, updates = parse_input(@raw_input)
    assert_equal rules[53], [29, 13]
    assert_equal updates[0], [75, 47, 61, 53, 29]
    assert_equal check_update(updates[0], rules), true
    assert_equal check_update(updates[1], rules), true
    assert_equal check_update(updates[2], rules), true
    assert_equal check_update(updates[3], rules), false
    assert_equal check_update(updates[4], rules), false
    assert_equal check_update(updates[5], rules), false
    # assert_equal find_correct_location(updates[3], rules, updates[3][0]), 1
    # assert_equal find_correct_location(updates[4][1..], rules, updates[4][1]), 1
    # assert_equal find_correct_location(updates[5][1..], rules, updates[5][1]), 3
    assert_equal [97,75,47,61,53], fix_update([75,97,47,61,53], rules)
    assert_equal [97,75,47,29,13], fix_update([97,13,75,29,47], rules)
    next_update = fix_update_once([97,13,75,29,47], rules)
    assert_equal [97,75,29,47,13], next_update
    next_update = fix_update_once(next_update, rules)
    assert_equal [97,75,47,29,13], next_update
    assert_equal [143, 123], program(@raw_input)
  end
end
