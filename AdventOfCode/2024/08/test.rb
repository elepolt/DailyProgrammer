# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '............',
      '........0...',
      '.....0......',
      '.......0....',
      '....0.......',
      '......A.....',
      '............',
      '............',
      '........A...',
      '.........A..',
      '............',
      '............'
    ]
  end

  def test_code
    board = build_board(@raw_input)
    assert_equal '0', board[1][8]
    antennas = find_antennas(board)
    assert_equal 4, antennas['0'].count
    assert_equal 3, antennas['A'].count
    assert_equal [2, -1], coord_math([2, 4], [4, 3])
    assert_equal [14, 34], program(@raw_input)
  end
end
