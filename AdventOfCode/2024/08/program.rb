# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  antinodes_p1 = []
  antinodes_p2 = []
  board = Board.new(array)
  antennas = find_antennas(board)
  antennas.each do |antenna_details|
    coords = antenna_details[1]
    coords.permutation(2).each do |combos|
      antinodes_p2.append(combos[1])
      difference = coord_math(combos[0], combos[1])
      antinode_location = [combos[0][0] - difference[0], combos[0][1] - difference[1]]
      next if board.out_of_range(antinode_location)

      antinodes_p1.append(antinode_location)
      # Keep walking in that direction to add more antinodes
      (2..600).each do |di|
        antinode_location = [combos[0][0] - (difference[0]) * di, combos[0][1] - (difference[1]) * di]
        next if board.out_of_range(antinode_location)

        antinodes_p2.append(antinode_location)
      end
    end
  end
  ans2 = (antinodes_p1 + antinodes_p2).uniq.count
  [antinodes_p1.uniq.count, ans2]
end

def find_antennas(board)
  antennas = {}
  board.cells.each do |coord, cell|
    next if cell == '.'

    antennas[cell] = [] unless antennas.keys.include?(cell)
    antennas[cell].append(coord)
  end
  antennas
end

def coord_math(a, b)
  dx = b[0] - a[0]
  dy = b[1] - a[1]
  [dx, dy]
end
puts "Part 1/2: #{program(input_array)}"
