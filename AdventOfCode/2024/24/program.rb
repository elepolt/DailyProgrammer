# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  gates, puzzle = parse_input(array)
  gates = part1(gates, puzzle)
  binding.irb
  ans1 = gates.keys.select { |k|k[0] == 'z'}.sort.reverse.map { |zk| gates[zk].to_s }.join.to_i(2)
  [ans1, part2(gates, puzzle)]
end

def part2(gates, puzzle)
  x_num = gates.keys.select { |k|k[0] == 'x'}.sort.reverse.map { |zk| gates[zk].to_s }.join.to_i(2)
  y_num = gates.keys.select { |k|k[0] == 'y'}.sort.reverse.map { |zk| gates[zk].to_s }.join.to_i(2)
  looking_for = x_num + y_num
  looking_for
end

def part1(gates, puzzle)
  mapping = {}
  until puzzle.empty?
    gate = puzzle.shift
    unless gates.keys.include?(gate[0]) && gates.keys.include?(gate[2])
      puzzle.append(gate)
      next
    end

    output = gates[gate[0]] & gates[gate[2]] if gate[1] == 'AND'
    output = [gates[gate[0]], gates[gate[2]]].max if gate[1] == 'OR'
    output = gates[gate[0]] ^ gates[gate[2]] if gate[1] == 'XOR'
    gates[gate[3]] = output
    # mapping[gate[3]] = [gate[0], gate[2]]
  end
  gates
end

def parse_input(array)
  input = {}
  puzzle = []
  puzzle_input = false
  array.each do |line|
    if line == ''
      puzzle_input = true
      next
    end
    if puzzle_input
      puzzle.append(parse_puzzle_string(line))
      next
    end
    input_val = parse_input_string(line)
    input[input_val[0]] = input_val[1].to_i
  end
  [input, puzzle]
end

def parse_input_string(string)
  string.split(': ')
end

def parse_puzzle_string(string)
  gates, output = string.split(' -> ')
  [gates.split(' '), output].flatten
end

# puts "Part 1/2: #{program(input_array)}"
