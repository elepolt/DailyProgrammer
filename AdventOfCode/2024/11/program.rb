# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(stones)
  stones = split_stones(stones)
  ans1 = 0
  75.times do |t|
    stones = iterate(stones)
    ans1 = stones.values.sum if t == 24
  end
  [ans1, stones.values.sum]
end

def iterate(stones)
  next_dict = Hash.new(0)
  stones.each do |stone, count|
    blink(stone).each do |new_stone|
      next_dict[new_stone] += count
    end
  end
  next_dict
end

def blink(stone)
  chars = stone.to_s.split('')
  return [1] if stone.zero?
  return [chars.shift((chars.length / 2)).join.to_i, chars.join.to_i] if chars.length.even?

  [stone * 2024]
end

def split_stones(stones)
  stones.split(' ').map(&:to_i).tally
end

puts "Part 1/2: #{program(input_array[0])}"
