# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = ['125 17']
  end

  def test_code
    tmp = { 125 => 1, 17 => 1 }
    assert_equal tmp, split_stones('125 17')
    assert_equal [253_000], blink(125)
    assert_equal [1, 7], blink(17)
    stones = { 253_000 => 1, 1 => 1, 7 => 1 }
    assert_equal stones, iterate(split_stones('125 17'))
    stones = iterate(stones)
    assert_equal({ 253 => 1, 0 => 1, 2024 => 1, 14_168 => 1 }, stones)
    stones = iterate(stones)
    assert_equal [512_072, 1, 20, 24, 28_676_032].tally, stones
    stones = iterate(stones)
    assert_equal [512, 72, 2024, 2, 0, 2, 4, 2867, 6032].tally, stones
    stones = iterate(stones)
    assert_equal [1_036_288, 7, 2, 20, 24, 4048, 1, 4048, 8096, 28, 67, 60, 32].tally, stones
    stones = iterate(stones)
    assert_equal [2_097_446_912, 14_168, 4048, 2, 0, 2, 4, 40, 48, 2024, 40, 48, 80, 96, 2, 8, 6, 7, 6, 0, 3, 2].tally,
                 stones
    assert_equal [55_312, 65_601_038_650_482], program(@raw_input[0])
  end
end
