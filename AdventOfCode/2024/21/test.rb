# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '029A',
      '980A',
      '179A',
      '456A',
      '379A'
    ]
  end

  def test_code
    # robot1 = numpad.find_cell('A')
    # assert_equal '<A^A>^^AvvvA', move_all_movements(robot1, '029A', numpad)
    # robot2 = arrows.find_cell('A')
    # assert_equal 'v<<A>>^A<A>AvA<^AA>A<vAAA>^A', move_all_movements(robot2, '<A^A>^^AvvvA', arrows)
    # assert_equal '<vA<AA>>^AvAA<^A>A<v<A>>^AvA^A<vA>^A<v<A>^A>AAvA^A<v<A>A>^AAAvA<^A>A', move_all_movements(robot2, 'v<<A>>^A<A>AvA<^AA>A<vAAA>^A', arrows)
    # pp dfs(robot1, '0', numpad)
    # assert_equal ['<', 'A'], dfs(robot1, '0', numpad)
    # robot1 = numpad.find_cell('0')
    # assert_equal ['^', 'A'], get_movements(robot1, '2', numpad)
    # robot1 = numpad.find_cell('2')
    # assert_equal ['>', '^', '^', 'A'], get_movements(robot1, '9', numpad)
    # robot1 = numpad.find_cell('9')
    # assert_equal ['v', 'v', 'v', 'A'], get_movements(robot1, 'A', numpad)
    # robot2 = arrows.find_cell('A')
    # assert_equal 'v<<A>>^A', move_robots(['<', 'A'], robot2).join
    # assert_equal '<vA<AA>>^AvAA<^A>A', move_robots('v<<A>>^A'.split(''), robot2).join
    # assert_equal '<vA<AA>>^AvAA<^A>A', do_the_thing('0')
    # assert_equal '<vA<AA>>^AvAA<^A>A<v<A>>^AvA^A<vA>^A<v<A>^A>AAvA^A<v<A>A>^AAAvA<^A>A', do_the_thing('029A')
    #             v<A<AA^>>A<Av>AA^Av<<A^>>AvA^Av<<A^>>AAv<A>A^A<A>Av<A<A^>>AAA<Av>A^A
    # assert_equal '<v<A>>^AAAvA^A<vA<AA>>^AvAA<^A>A<v<A>A>^AAAvA<^A>A<vA>^A<A>A', do_the_thing('980A')
    assert_equal '<v<A>>^A<vA<A>>^AAvAA<^A>A<v<A>>^AAvA^A<vA>^AA<A>A<v<A>A>^AAAvA<^A>A', do_the_thing('179A')
    #            "<<vAA>A>^AAvA<^A>AvA^A<<vA>>^AAvA^A<vA>^AA<A>A<<vA>A>^AAAvA<^A>A"
    # assert_equal '<v<A>>^AA<vA<A>>^AAvAA<^A>A<vA>^A<A>A<vA>^A<A>A<v<A>A>^AAvA<^A>A', do_the_thing('456A')
    # assert_equal '<v<A>>^AvA^A<vA<AA>>^AAvA<^A>AAvA^A<vA>^AA<A>A<v<A>A>^AAAvA<^A>A', do_the_thing('379A')
    #            "v<<A>>^AvA^Av<<A>>^AAv<A<A>>^AAvAA^<A>Av<A^>AA<A>Av<A<A>>^AAA<Av>A^A"
    # assert_equal [126384, nil], program(@raw_input)
  end
end
