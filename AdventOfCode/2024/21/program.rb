# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  ans1 = 0
  array.each do |code|
    score = (code.split('') - ['A']).join.to_i
    movements = do_the_thing(code)
    ans1 += score * movements.length
  end
  [ans1, nil]
end

def do_the_thing(code_str)
  robot1 = numpad.find_cell('A')
  robot2 = arrows.find_cell('A')
  robot2_movements = []
  robot3 = arrows.find_cell('A')
  robot3_movements = []
  me = arrows.find_cell('A')
  me_movements = []
  final_movements = []
  robot1_movements = move_all_movements(robot1, code_str, numpad)
  robot2_movements = move_all_movements(robot2, robot1_movements, arrows)
  robot3_movements = move_all_movements(robot3, robot2_movements, arrows)
  # code_str.split('').each do |code|
  #   movements = get_movements(robot1, code, numpad)
  #   robot1_movements += movements
  #   # robot1 = numpad.find_cell(code)
  #   # next_movements = move_robots(movements, robot2)
  #   # robot2_movements += next_movements
  #   # robot2 = arrows.find_cell(movements.last)
  #   # me_movements = move_robots(next_movements, robot3)
  #   # robot3_movements += me_movements
  #   # robot3 = arrows.find_cell(next_movements.last)
  #   # final_movements += move_robots(me_movements, me)
  #   # me = arrows.find_cell(final_movements.last)
  # end
  # pp robot1_movements.join
  # pp robot2_movements.join
  # v<<A>>^A<A>AvA<^AA>A<vAAA>^A
  # v<<A^>>A<A>A<AAv>A^Av<AAA^>A
  robot3_movements
end

def move_all_movements(robot, code_str, board)
  movements = []
  code_str.split('').each do |code|
    movements += get_movements(robot, code, board)
    robot = board.find_cell(code)
  end
  movements.join
end

def get_movements(coord, code, board)
  movements = []
  diff = coord_difference(coord, board.find_cell(code))
  if diff[0] != 0
    updown = diff[0].positive? ? ['v'] : ['^']
    movements += updown * diff[0].abs
  end
  if diff[1] != 0
    leftright = diff[1].positive? ? ['>'] : ['<']
    movements += leftright * diff[1].abs
  end
  # IE If we're at 0 and try to move to 1 and we go < ^, that is not ok
  movements.sort!
  # binding.irb
  # check = movements[0]
  movements.reverse! if movements.include?('v')
  # if (board.cells[coord] == '0' && check == '<') ||
  #    (board.cells[coord] == '1' && check == 'v') ||
  #    (board.cells[coord] == '<' && check == '^') ||
  #    (board.cells[coord] == '^' && check == '<')
  #    binding.irb
  #    pp 'blah'
  # end
  movements.append('A')
  movements
end

def dfs(coord, code, board)
  end_coord = board.find_cell(code)
  return ['A'] if coord == end_coord

  visited = {}
  unvisited = [coord]
  until visited.keys.include?(end_coord)
    until unvisited.empty?
      node = unvisited.shift
      neighbors = board.neighbors(node, 0, false)
      neighbors.each do |n_coord|
        next if board.cells[n_coord] == '#'
        next if unvisited.include?(n_coord)
        next if visited.keys.include?(n_coord)

        visited[n_coord] = node
        unvisited.append(n_coord)
      end
    end
  end
  route = []

  loop do
    route.append(math_to_dir(end_coord, visited[end_coord]))
    end_coord = visited[end_coord]
    break if end_coord == coord
  end
  route += ['A']
  route.tally
end

def math_to_dir(a1, a2)
  diff = coord_difference(a2, a1)
  return '<' if diff == [0, -1]
  return '>' if diff == [0, 1]
  return '^' if diff == [-1, 0]
  return 'v' if diff == [1, 0]
  binding.irb
  pp 'goofed'
end


def move_robots(movements, robot)
  next_movements = []
  movements.each do |arrow|
    next_movements += get_movements(robot, arrow, arrows)
    robot = arrows.find_cell(arrow)
  end
  next_movements
end

def numpad
  @numpad ||= begin
    array = [
      '789',
      '456',
      '123',
      '#0A'
    ]
    Board.new(array, false)
  end
end

def arrows
  @arrows ||= begin
    array = [
      '#^A',
      '<v>'
    ]
    Board.new(array, false)
  end
end

# end
# puts "Part 1/2: #{program(input_array)}"
