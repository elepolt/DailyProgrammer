# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '3   4',
      '4   3',
      '2   5',
      '1   3',
      '3   9',
      '3   3'
    ]
  end

  def test_code
    assert_equal [1, 2, 3, 3, 3, 4], get_lists(@raw_input)[0]
    assert_equal [3, 3, 3, 4, 5, 9], get_lists(@raw_input)[1]
    assert_equal [11, 31], program(@raw_input)
  end
end
