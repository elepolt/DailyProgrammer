# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  sum1 = 0
  sum2 = 0
  left, right = get_lists(array)
  right_counts = right.tally
  (0..left.length - 1).each do |idx|
    sum1 += (left[idx] - right[idx]).abs
    sum2 += left[idx] * right_counts[left[idx]] if right_counts[left[idx]]
  end
  [sum1, sum2]
end

def get_lists(array)
  left = []
  right = []
  array.map(&:split).each do |nums|
    left.append(nums.first.to_i)
    right.append(nums.last.to_i)
  end
  [left.sort, right.sort]
end

puts "Part 1/2: #{program(input_array)}"
