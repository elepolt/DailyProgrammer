# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '....#.....',
      '.........#',
      '..........',
      '..#.......',
      '.......#..',
      '..........',
      '.#..^.....',
      '........#.',
      '#.........',
      '......#...'
    ]
  end

  def test_code
    assert_equal [41, 6], program(@raw_input)
    board = build_board(@raw_input)
    pos = get_starting_pos(board)
    assert_equal [6, 4], pos
    assert_equal [5, 4], step_direction(board, [6, 4], [-1, 0])
    assert_equal false, step_direction(board, [0, 0], [-1, 0])
    # assert_equal false, patrol(board, pos)
    [[6, 3], [7, 6], [7, 7], [8, 1], [8, 3], [9, 7]].each do |box|
      board[box[0]][box[1]] = '#'
      assert_equal patrol(board, pos), true
      # puts("\n")
      board[box[0]][box[1]] = '.'
    end
    board[1][1] = '#'
    assert_equal patrol(board, pos), false
    board[1][1] = '.'

    # board[6][3] = '#'
    # assert_equal true, patrol(board, pos)
    # board[6][3] = '#'
  end
end
