# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  board = build_board(array)
  pos = get_starting_pos(board)
  directions = [[-1, 0], [0, 1], [1, 0], [0, -1]]
  direction = 0
  ans1 = 0
  board[pos[0]][pos[1]] = 'X'
  steps = Set.new
  6000.times do
    next_pos = step_direction(board, pos, directions[direction])
    break unless next_pos

    if board[next_pos[0]][next_pos[1]] == '#'
      direction += 1
      direction %= 4
      next
    end
    pos = next_pos
    steps.add(pos)
    board[pos[0]][pos[1]] = 'X'
  end

  board.each do |row|
    ans1 += row.count('X')
  end

  # reset board
  board = build_board(array)
  ans2 = brute_force(board, steps)

  [ans1, ans2]
end

def print_board(board)
  board.each do |row|
    # ans1 += row.count('X')
    pp row.join
  end
end

def build_board(array)
  board = []
  array.each do |row|
    board.append(row.split(''))
  end
  board
end

def brute_force(board, steps)
  ans2 = 0
  starting_pos = get_starting_pos(board)
  steps.each do |coord|
    next if coord == starting_pos

    tmp_board = board.map { |lines| lines.dup }
    tmp_board[coord[0]][coord[1]] = '#'
    ans2 += 1 if patrol(tmp_board, starting_pos)
  end

  ans2
end

def patrol(board, starting_pos)
  keys = Set.new
  pos = starting_pos
  directions = [[-1, 0], [0, 1], [1, 0], [0, -1]]
  direction = 0

  6000.times do
    break if keys.include?([pos, direction])

    keys.add([pos, direction])
    next_pos = step_direction(board, pos, directions[direction])
    return false unless next_pos

    if board[next_pos[0]][next_pos[1]] == '#'
      direction += 1
      direction %= 4
      next
    end
    pos = next_pos
    board[pos[0]][pos[1]] = 'X'
  end
  true
end

def get_starting_pos(board)
  board.each_with_index do |row, idx|
    row.each_with_index do |cell, idy|
      return [idx, idy] if cell == '^'
    end
  end
end

def step_direction(board, pos, direction)
  next_pos = [pos[0] + direction[0], pos[1] + direction[1]]
  return false if out_of_range(board, next_pos)

  next_pos
end

puts "Part 1/2: #{program(input_array)}"
