# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array, size, amount)
  board = build_board(array, size, amount)
  ans = travel(board)
  ans1 = ans[0]
  path = ans[1]
  ans2 = 0
  3000.times do
    ans2, path = blocking(array, size, path)
    puts 'done' if path.nil?
    break if path.nil?
  end
  [ans1, array[ans2 - 1]]
end

def travel(board)
  visited = {}
  unvisited = [[0, 0]]
  score = 0
  until visited.keys.include?([board.height, board.width])
    return false if unvisited.empty?

    score += 1
    next_unvisited = []
    unvisited.each do |coord|
      neighbors = board.neighbors(coord, 0, false)
      neighbors.each do |n_coord|
        next if visited.include?(n_coord)
        next unless board.cells[n_coord] == '.'

        # board.cells[n_coord] = score
        visited[n_coord] = coord
        next_unvisited.append(n_coord)
        break if n_coord == [board.height, board.width]
      end
    end
    unvisited = next_unvisited.uniq
  end

  [score, visited]
end

def blocking(array, size, path)
  na = next_amount(array, path) + 1
  board = build_board(array, size, na)
  next_stuff = travel(board)
  return na if next_stuff == false

  next_stuff
end

def next_amount(array, path)
  idx = -1
  array.each do |string|
    idx += 1
    coord = string.split(',').map(&:to_i).flatten.reverse
    break if path.include?(coord)
  end
  idx
end

def build_board(array, size, amount)
  board_array = []
  (size + 1).times do
    board_array.append('.' * (size + 1))
  end
  board = Board.new(board_array, false)
  array[0..amount - 1].each do |coords|
    coord = coords.split(',').map(&:to_i).flatten.reverse
    board.cells[coord] = '#'
  end

  board
end

# puts "Part 1/2: #{program(input_array, 70, 1024)}"
