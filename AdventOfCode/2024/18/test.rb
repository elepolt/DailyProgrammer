# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '5,4',
      '4,2',
      '4,5',
      '3,0',
      '2,1',
      '6,3',
      '2,4',
      '1,5',
      '0,6',
      '3,3',
      '2,6',
      '5,1',
      '1,2',
      '5,5',
      '2,5',
      '6,5',
      '1,4',
      '0,4',
      '6,4',
      '1,1',
      '6,1',
      '1,0',
      '0,5',
      '1,6',
      '2,0',
    ]
  end

  def test_code
    board = build_board(@raw_input, 6, 12)
    assert_equal '#', board.cells[[0,3]]
    assert_equal [22, "6,1"], program(@raw_input, 6, 12)
  end
end
