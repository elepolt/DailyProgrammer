# frozen_string_literal: true

require_relative '../../base'

input_array = parse_intengers(read_lines('input.txt'))
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  results = {}
  ans1 = 0
  ans2 = 0
  array.each do |number|
    number = number.to_i
    orig_num = number
    results[orig_num] = [orig_num]
    10.times do
      number = iterate(number)
      results[orig_num].append(number)
    end
    ans1 += number
    pp check_changes(results[orig_num])
  end
  [ans1, nil]
end

# Sigh? I'm not sure I understand the problem. You're supposed to keep track of changes of last digits
# of the secret number, and then you have to utilize that set of changes for each monkey to get the highest possible
def check_changes(numbers)
  possible_differences = Set.new
  numbers.each_cons(5) do |cons_five|
    last_digits = cons_five.map { |cf| cf.to_s[-1].to_i }
    differences = last_digits.each_cons(2).map { |ct| ct[1] - ct[0] }
    possible_differences.add([differences, last_digits.last])
  end
  possible_differences
end

def iterate(number)
  tmp_number = number * 64
  number ^= tmp_number
  number %= 16777216
  tmp_number = number / 32
  number ^= tmp_number
  number %= 16777216
  tmp_number = number * 2048
  number ^= tmp_number
  number % 16777216
end

# puts "Part 1/2: #{program(input_array)}"
