# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '1',
      '10',
      '100',
      '2024'
    ]
    @test2 = [
      '123',
      # '2',
      # '2024'
    ]
  end

  def test_code
    assert_equal 15887950, iterate(123)
    # assert_equal [37327623, nil], program(@raw_input)
    program(@test2)
    # assert_equal [37327623, nil], program(@test2)
  end
end
