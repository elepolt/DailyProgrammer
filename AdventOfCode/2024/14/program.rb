# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, height, width)
  robots = parse_coords(array)
  100.times do |idx|
    robots.each_value do |data|
      next_coord = coord_math(data[:coord], data[:velocity])
      data[:coord] = [next_coord[0]%height, next_coord[1]%width]
    end
  end
  ct = 0
  robots = parse_coords(array)
  # binding.irb
  10_000.times do |idx|
    robots.each_value do |data|
      next_coord = coord_math(data[:coord], data[:velocity])
      data[:coord] = [next_coord[0]%height, next_coord[1]%width]
    end
    if find_christmas_tree(robots)
      ct = idx + 1
      break
    end
  end

  quadrants = gather_quadrants(robots, height, width)

  # pp robots
  [quadrants.values.reduce(:*), ct]
end

def parse_coords(array)
  robots = {}
  array.each_with_index do |string, idx|
    data = parse_string(string)
    robots[idx] = { 'coord': data[0], 'velocity': data[1] }
  end
  robots
end

def parse_string(string)
  # p=0,4 v=3,-3
  pos = string.split(' ')[0][2..]
  pos = pos.split(',').map(&:to_i)
  vel = string.split(' ')[1][2..]
  vel = vel.split(',').map(&:to_i)
  [pos, vel]
end

def coord_math(a, b)
  dx = a[0] + b[0]
  dy = a[1] + b[1]
  [dx, dy]
end

def gather_quadrants(robots, height, width)
  quadrants = Hash.new(0)
  robots.each_value do |data|
    coord = data[:coord]
    x = coord[0]
    y = coord[1]

    quadrants[0] += 1 if x < height / 2 && y < width / 2
    quadrants[1] += 1 if x < height / 2 && y > width / 2
    quadrants[2] += 1 if x > height / 2 && y < width / 2
    quadrants[3] += 1 if x > height / 2 && y > width / 2
  end
  quadrants
end

def find_christmas_tree(robots)
 in_a_row = 0
 robots.values.map{ |r| r[:coord] }.sort.each_cons(2) do |robs|
    in_a_row += 1 if robs[1][1] - robs[0][1] == 1 && robs[1][0] == robs[0][0]
    in_a_row = 0 if robs[1][0] > robs[0][0]
    return true if in_a_row == 20
 end
 false
end

puts "Part 1/2: #{program(input_array, 101, 103)}"
