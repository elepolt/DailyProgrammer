# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(lines)
  # binding.irb
  pattern = /(mul\()+(\d+)+(,)+(\d+)+(\))/
  # pattern = /(mul\(\d,\d)\)/
  ans1 = 0
  lines.each do |code|
    code.scan(pattern).each do |match|
      puts(match.join)
      ans1 += match[1].to_i * match[3].to_i
    end
  end
  [ans1, nil]
end

def program_2(lines)
  pattern = /(mul\()+(\d+)+(,)+(\d+)+(\))/
  # pattern = /(mul\(\d,\d)\)/
  ans1 = 0
  enabled = true
  lines.each do |code|
    tmp_code = code
    loop do
      next_dont = tmp_code.index("don't()")
      next_do = tmp_code.index('do()')
      # If we're not enabled, jump to the next_do index
      unless enabled
        # If there aren't any more do()'s then we're done
        break if next_do.nil?

        tmp_code = tmp_code[next_do + 1..]
        enabled = true
        next
      end
      # now check all the mul() between here and the next_dont
      # binding.irb
      tmp_code[..next_dont].scan(pattern).each do |match|
        puts(match.join)
        ans1 += match[1].to_i * match[3].to_i
      end
      break if next_dont.nil?

      tmp_code = tmp_code[next_dont + 1..]
      enabled = false
      # binding.irb
      # break if next_do.zero? && next_dont.zero?
      # tmp_code = tmp_code[next_dont+1..]
    end
  end
  [ans1, nil]
end

def program_stepper(code)
  chars = code.split('')
  idx = 0
  multiplying = false
  ans1 = []
  loop do
    char = chars[idx]
    # binding.irb if multiplying
    if char == 'm'
      if chars[idx..idx + 3].join == ('mul(')
        multiplying = true
        idx += 4 # 3 instead of 4 because we do idx+=1 at the beginning
        num, new_idx = get_multiple(chars[idx..])
        ans1.append(num)
        idx += new_idx
        next
      end
    end
    idx += 1
    break if idx > chars.length
  end
  [ans1.sum, nil]
end

def get_multiple(chars)
  first_num = ''
  second_num = ''
  second = false
  idx = 0
  chars.each do |char|
    idx += 1
    # binding.irb
    if char == ','
      second = true
      next
    end
    break if char == ')'
    return [0, idx] unless char.is_integer?

    first_num += char unless second
    second_num += char if second
  end
  [first_num.to_i * second_num.to_i, idx]
end

# 27499548 too low
# 169021493
# puts "Part 1/2: #{program(input_array)}"
puts "Part 1/2: #{program_2(input_array)}"
