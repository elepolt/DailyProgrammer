# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = ['xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))']
  end

  def test_code
    # assert_equal [161, nil], program(@raw_input)
    assert_equal [48, nil], program_2(["xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"])

    chars = 'mul(2,4)%&mul['.split('')
    assert_equal [8, 4], get_multiple(chars[4..])
  end
end
