# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '###############',
      '#.......#....E#',
      '#.#.###.#.###.#',
      '#.....#.#...#.#',
      '#.###.#####.#.#',
      '#.#.#.......#.#',
      '#.#.#####.###.#',
      '#...........#.#',
      '###.#.#####.#.#',
      '#...#.....#.#.#',
      '#.#.#.###.#.#.#',
      '#.....#...#.#.#',
      '#.###.#.#.#.#.#',
      '#S..#.....#...#',
      '###############'
    ]
  end

  def test_code
    board = Board.new(@raw_input, false)
    assert_equal [1, 13], board.find_cell('E')
    assert_equal [7036, 45], program(@raw_input)
  end
end
