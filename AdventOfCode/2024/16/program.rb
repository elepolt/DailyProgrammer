# frozen_string_literal: true

require_relative '../../base'
require 'pqueue'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  board = Board.new(array, false)
  starting_node = board.find_cell('S')
  visited = run_maze(board, starting_node)
  ending = board.find_cell('E')

  coord = ending
  ans2 = 1
  while coord != starting_node
    binding.irb if visited[coord][:alternate_vertex]
    ans2 += 1
    board.cells[coord] = 'x'
    coord = visited[coord][:previous_vertex]
  end
  # board.print
  [visited[ending][:value], ans2]
end

def run_maze(board, starting_node)
  unvisited_nodes = PQueue.new
  unvisited_nodes.push([0, starting_node, 'east'])
  visited = {}
  visited[starting_node] = {
    'value': 0,
    'previous_vertex': starting_node
  }
  until unvisited_nodes.empty?
    risk, current_node, direction = unvisited_nodes.shift
    board.neighbors(current_node, 0, false).each do |neighbor|
      next if board.cells[neighbor] == '#'

      next_dir = next_direction(direction, current_node, neighbor)
      unless visited.key?(neighbor)
        visited[neighbor] = {
          'value': 9_999_999,
          'previous_vertex': current_node,
          'direction': direction,
          'alternate_vertex': nil
        }
      end

      value = 1 + risk
      value += 1000 if next_dir != direction
      # binding.irb if (value - 1000) == visited[neighbor][:value]
      if value == visited[neighbor][:value]
        unless visited[neighbor][:previous_vertex] == current_node
          visited[neighbor][:alternate_vertex] = current_node
        end
      end

      if value < visited[neighbor][:value]
        visited[neighbor][:value] = value
        visited[neighbor][:previous_vertex] = current_node
        unvisited_nodes.push([value, neighbor, next_dir])
      end
    end
  end

  visited
end

def next_direction(direction, coord, neighbor)
  return 'east' if coord_difference(coord, neighbor) == RIGHT
  return 'west' if coord_difference(coord, neighbor) == LEFT
  return 'north' if coord_difference(coord, neighbor) == UP

  'south'
end
# puts "Part 1/2: #{program(input_array)}"
