# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, check)
  board = Board.new(array, false)
  # This turns the board into a path of the scores
  _score, visited = travel(board)
  time_saves = find_cheats(board, visited)

  ans1 = 0
  time_saves.values.tally.each do |save, amount|
    ans1 += amount if save >= check
  end
  ans2 = 0
  time_saves2 = find_paths(visited)
  time_saves2.each do |save, amount|
    ans2 += amount if save >= check
  end
  [ans1, ans2]
end

def find_cheats(board, visited)
  time_saves = {}
  visited.each do |coord, val|
    # neighbors = board.neighbors(coord, 0, false)
    [UP, DOWN, LEFT, RIGHT].each do |dir|
      n1 = coord_addition(coord, dir)
      # Move on if we're not passing through a wall
      next unless board.cells[n1] == '#'

      # Move on if the other side of the wall is not part of our path
      n2 = coord_addition(n1, dir)
      next unless visited.keys.include?(n2)

      # -2 because it takes two steps to get to the other
      # side of the wall which counts against the amount of time saved
      time_save = visited[n2] - val - 2
      time_saves[n1] = time_save if time_save.positive?
    end
  end
  time_saves
end

def find_paths(visited)
  time_saves2 = Hash.new(0)
  visited.each do |coord, val|
    visited.each_key do |cheat|
      diff = manhattan(coord, cheat)
      next unless diff <= 20

      time_save = visited[cheat] - val - diff
      time_saves2[time_save] += 1 if time_save.positive?
    end
  end
  time_saves2
end

def manhattan(coord1, coord2)
  coord_diff = coord_difference(coord1, coord2)
  coord_diff[0].abs + coord_diff[1].abs
end

def travel(board)
  visited = {}
  visited[board.find_cell('S')] = 0
  end_coord = board.find_cell('E')
  unvisited = [board.find_cell('S')]
  score = 0
  until visited.keys.include?([board.height, board.width])
    return false if unvisited.empty?

    score += 1
    next_unvisited = []
    unvisited.each do |coord|
      return [score, visited] if coord == end_coord

      neighbors = board.neighbors(coord, 0, false)
      neighbors.each do |n_coord|
        next if visited.include?(n_coord) || board.cells[n_coord] == 'S'
        next unless board.cells[n_coord] == '.' || board.cells[n_coord] == 'E'

        board.cells[n_coord] = score
        visited[n_coord] = score
        next_unvisited.append(n_coord)
        return [score, visited] if n_coord == end_coord
      end
    end
    unvisited = next_unvisited.uniq
  end

  [score, visited]
end

# Part 2: 887206 too low...
puts "Part 1/2: #{program(input_array)}"
