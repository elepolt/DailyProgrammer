# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '###############',
      '#...#...#.....#',
      '#.#.#.#.#.###.#',
      '#S#...#.#.#...#',
      '#######.#.#.###',
      '#######.#.#...#',
      '#######.#.###.#',
      '###..E#...#...#',
      '###.#######.###',
      '#...###...#...#',
      '#.#####.#.###.#',
      '#.#...#.#.#...#',
      '#.#.#.#.#.#.###',
      '#...#...#...###',
      '###############'
    ]
  end

  def test_code
    board = Board.new(@raw_input, false)
    _score, visited = travel(board)
    time_saves = find_cheats(board, visited).values.tally
    assert_equal 14, time_saves[2]
    assert_equal 14, time_saves[4]
    assert_equal 2, time_saves[6]
    assert_equal 4, time_saves[8]
    assert_equal 2, time_saves[10]
    assert_equal 3, time_saves[12]
    assert_equal 1, time_saves[20]
    assert_equal 1, time_saves[36]
    assert_equal 1, time_saves[38]
    assert_equal 1, time_saves[40]
    assert_equal 1, time_saves[64]

    time_saves2 = find_paths(visited)
    assert_equal 32, time_saves2[50]
    assert_equal 31, time_saves2[52]
    assert_equal 29, time_saves2[54]
    assert_equal 39, time_saves2[56]
    assert_equal 25, time_saves2[58]
    assert_equal 23, time_saves2[60]
    assert_equal 20, time_saves2[62]
    assert_equal 19, time_saves2[64]
    assert_equal 12, time_saves2[66]
    assert_equal 14, time_saves2[68]
    assert_equal 12, time_saves2[70]
    assert_equal 22, time_saves2[72]
    assert_equal 4, time_saves2[74]
    assert_equal 3, time_saves2[76]
    assert_equal [nil, 285], program(@raw_input, 50)
  end
end
