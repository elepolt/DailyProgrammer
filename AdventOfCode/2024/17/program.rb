# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  a = array[0].split(' ').last
  b = array[1].split(' ').last
  c = array[2].split(' ').last
  op_codes = array.last.split(' ')[1].split(',').map(&:to_i)
  idx = -1
  output = []
  combo_operands = {}
  combo_operands[0] = 0
  combo_operands[1] = 1
  combo_operands[2] = 2
  combo_operands[3] = 3
  combo_operands[4] = a.to_i
  combo_operands[5] = b.to_i
  combo_operands[6] = c.to_i
  # combo_operands[7] is reserved and will not appear in valid programs.

  until idx >= op_codes.length
    idx += 1
    code = op_codes[idx]
    case code
    when 0
      idx += 1
      # The numerator is the value in the A register. The denominator is found by raising 2 to the power of the instruction's combo operand.
      combo_operands[4] = combo_operands[4] / (2**combo_operands[op_codes[idx]])
    when 1
      idx += 1
      combo_operands[5] = bitwize_xor(combo_operands[5], op_codes[idx])
    when 2
      idx += 1
      combo_operands[5] = combo_operands[op_codes[idx]] % 8
    when 3
      idx += 1
      # binding.irb
      idx = op_codes[idx] - 1 unless combo_operands[4].zero?
    when 4
      idx += 1
      combo_operands[5] = bitwize_xor(combo_operands[5], combo_operands[6])
    when 5
      idx += 1
      output.append(combo_operands[op_codes[idx]] % 8)
    when 6
      idx += 1
      combo_operands[5] = combo_operands[4] / (2**combo_operands[op_codes[idx]])
    when 7
      idx += 1
      # The numerator is the value in the A register. The denominator is found by raising 2 to the power of the instruction's combo operand.
      combo_operands[6] = combo_operands[4] / (2**combo_operands[op_codes[idx]])
    end
  end

  [combo_operands[4], combo_operands[5], combo_operands[6], output.join(',')]
  # [output.join(','), nil]
end


def bitwize_xor(op_one, op_two)
  op_one ^ op_two
end
puts "Part 1/2: #{program(input_array)}"
