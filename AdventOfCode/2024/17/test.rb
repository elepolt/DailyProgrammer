# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'Register A: 0',
      'Register B: 0',
      'Register C: 9',
      '',
      'Program: 2,6'
    ]
    @test2 = [
      'Register A: 10',
      'Register B: 0',
      'Register C: 0',
      '',
      'Program: 5,0,5,1,5,4'
    ]
    @test3 = [
      'Register A: 2024',
      'Register B: 0',
      'Register C: 0',
      '',
      'Program: 0,1,5,4,3,0'
    ]
    @test4 = [
      'Register A: 0',
      'Register B: 29',
      'Register C: 0',
      '',
      'Program: 1,7'
    ]
    @test5 = [
      'Register A: 0',
      'Register B: 2024',
      'Register C: 43690',
      '',
      'Program: 4,0'
    ]
    @test6 = [
      'Register A: 729',
      'Register B: 0',
      'Register C: 0',
      '',
      'Program: 0,1,5,4,3,0'
    ]
    @test7 = [
      'Register A: 117440',
      'Register B: 0',
      'Register C: 0',
      '',
      'Program: 0,3,5,4,3,0'
    ]
  end

  def test_code
    assert_equal 1, program(@raw_input)[1]
    assert_equal '0,1,2', program(@test2)[3]
    assert_equal 26, program(@test4)[1]
    assert_equal 44_354, program(@test5)[1]
    assert_equal '4,2,5,6,7,7,7,7,3,1,0', program(@test3)[3]
    assert_equal '4,6,3,5,6,3,5,2,1,0', program(@test6)[3]
    pp program(@test7)[3]
  end
end
