# frozen_string_literal: true

require_relative '../../base'
require 'set'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  board = build_board(array)
  xmas_coords = Set.new
  downleft = [1, -1]
  downright = [1, 1]
  upleft = [-1, -1]
  upright = [-1, 1]

  (0..board[0].length - 1).each do |dx|
    row = board[dx]
    (0..row.length - 1).each do |dy|
      next unless board[dx][dy] == 'M'

      up = [dx - 2, dy]
      down = [dx + 2, dy]
      left = [dx, dy - 2]
      right = [dx, dy + 2]
      [
        [down, left, downleft], [down, right, downright],
        [up, left, upleft], [up, right, upright],
        [right, up, upright], [right, down, downright],
        [left, up, upleft], [left, down, downleft]
      ].each do |square|
        next unless check_for_char(board, square[0], 'M')
        next unless check_for_char(board, square[1], 'S')

        diagonals = check_diagonals(board, [dx, dy], square[2])
        next unless diagonals

        xmas_coords.add(([[dx, dy], square[0], square[1]] + diagonals).sort)
      end
    end
  end

  [part1(board), xmas_coords.count]
end

def part1(board)
  ans1 = 0
  (0..board[0].length - 1).each do |dx|
    row = board[dx]
    (0..row.length - 1).each do |dy|
      next unless board[dx][dy] == 'X'

      traverse(board, [dx, dy]).each do |neighbor|
        letter = board[neighbor[0]][neighbor[1]]
        next unless letter == 'M'

        direction = [neighbor[0] - dx, neighbor[1] - dy]
        a = [neighbor[0] + direction[0], neighbor[1] + direction[1]]
        next if out_of_range(board, a)

        s = [a[0] + direction[0], a[1] + direction[1]]
        next if out_of_range(board, s)

        as = [board[a[0]][a[1]], board[s[0]][s[1]]]
        ans1 += 1 if as == %w[A S]
        # pp([dx, dy]) if as == %w[A S]
      end
    end
  end
  ans1
end

def out_of_range(board, coord)
  coord[0].negative? || coord[1].negative? || coord[0] >= board[0].length || coord[1] >= board[0].length
end

def check_for_char(board, coord, char)
  return false if out_of_range(board, coord)

  board[coord[0]][coord[1]] == char
end

def check_diagonals(board, starting_coord, direction)
  diagonals = coordinate_math(board, starting_coord, direction, 2)
  return nil unless diagonals.length == 2
  return nil unless board[diagonals[0][0]][diagonals[0][1]] == 'A'
  return nil unless board[diagonals[1][0]][diagonals[1][1]] == 'S'

  diagonals
end

def coordinate_math(board, starting_coord, direction, distance)
  coords = []
  coord = starting_coord
  distance.times do
    coord = [coord[0] + direction[0], coord[1] + direction[1]]
    next if out_of_range(board, coord)

    coords.append([coord[0], coord[1]])
  end
  coords
end

def build_board(array)
  board = []
  array.each do |line|
    board_line = []
    line.split('').each do |char|
      board_line.append(char)
    end
    board.append(board_line)
  end
  board
end

puts "Part 1/2: #{program(input_array)}"
