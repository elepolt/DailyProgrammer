# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'MMMSXXMASM',
      'MSAMXMSMSA',
      'AMXSXMAAMM',
      'MSAMASMSMX',
      'XMASAMXAMM',
      'XXAMMXXAMA',
      'SMSMSASXSS',
      'SAXAMASAAA',
      'MAMMMXMMMM',
      'MXMXAXMASX'
    ]
  end

  def test_code
    assert_equal [18, 9], program(@raw_input)
    board = build_board(@raw_input)
    dx = 0
    dy = 1
    right = [dx, dy + 2]
    assert_equal true, check_for_char(board, right, 'S')
    downright = [1, 1]
    assert_equal [[1, 2], [2, 3]], check_diagonals(board, [dx, dy], downright)
    assert_equal [[1, 1], [2, 2]], coordinate_math(board, [0, 0], [1, 1], 2)
  end
end
