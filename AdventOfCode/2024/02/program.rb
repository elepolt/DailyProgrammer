# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  ans1 = 0
  ans2 = 0
  array.each do |digits_str|
    digits = digits_str.split.map(&:to_i)
    if check_digits(digits)
      ans1 += 1
      ans2 += 1
    elsif check_digits2(digits)
      ans2 += 1
    end
  end
  [ans1, ans2]
end

def check_digits(digits)
  descending = 0
  ascending = 0
  digits.each_cons(2) do |pair|
    difference = (pair[0] - pair[1]).abs
    return false if difference > 3 || difference.zero?

    if pair[0] > pair[1]
      descending += 1
    else
      ascending += 1
    end
  end

  descending.zero? || ascending.zero?
end

# Brute force!
def check_digits2(digits)
  len = digits.count
  (0..len - 1).each do |idx|
    new_digits = digits.dup
    new_digits.delete_at(idx)
    return true if check_digits(new_digits)
  end
  false
end
puts "Part 1/2: #{program(input_array)}"
