# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '89010123',
      '78121874',
      '87430965',
      '96549874',
      '45678903',
      '32019012',
      '01329801',
      '10456732'
    ]
  end

  def test_code
    board = Board.new(@raw_input, true)
    starting_coords = find_starting_coords(board)
    assert_equal 9, starting_coords.count
    starting_coords.zip([5, 6, 5, 3, 1, 3, 5, 3, 5]).each do |tests|
      assert_equal tests[1], find_peak(board, tests[0])[0], "#{tests[0]} goofed"
    end
    starting_coords.zip([20, 24, 10, 4, 1, 4, 5, 8, 5]).each do |tests|
      assert_equal tests[1], find_peak(board, tests[0])[1], "#{tests[0]} goofed"
    end
  end
end
