# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  board = Board.new(array, true)
  ans1 = 0
  ans2 = 0
  find_starting_coords(board).each do |coord|
    peaks = find_peak(board, coord)
    ans1 += peaks[0]
    ans2 += peaks[1]
  end
  [ans1, ans2]
end

def find_peak(board, coord)
  peaks = []
  board.neighbors(coord, 0, false).each do |neighbor|
    step_up(board, coord, neighbor, peaks)
  end
  [peaks.uniq.count, peaks.count]
end

def step?(board, coord, next_coord)
  board.cells[next_coord] - board.cells[coord] == 1
end

def step_up(board, coord, next_coord, peaks)
  next_cell = board.cells[next_coord]
  return if next_cell <= board.cells[coord]
  return if (next_cell - board.cells[coord]) > 1

  if next_cell == 9 && board.cells[coord] == 8
    peaks.append(next_coord)
    return
  end

  board.neighbors(next_coord, 0, false).each do |neighbor|
    step_up(board, next_coord, neighbor, peaks)
  end
end

def find_starting_coords(board)
  starting_coords = []
  board.cells.each do |coord, cell|
    starting_coords.append(coord) if cell.zero?
  end
  starting_coords
end
puts "Part 1/2: #{program(input_array)}"