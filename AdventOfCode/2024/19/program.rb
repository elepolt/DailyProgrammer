# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  available = array[0].split(', ')
  logos = array[2..]
  ans1 = 0
  ans2 = 0
  logos.each do |logo|
    seen = Hash.new(0)
    ans1 += 1 if rearrange_forward(available, logo.split(''), 0, seen).positive?
    ans2 += rearrange_forward(available, logo.split(''), 0, seen)
  end
  [ans1, ans2]
end

def rearrange_forward(towels, logo, previous_idx, seen)
  # If we've already seen this index before, return the value
  return seen[previous_idx] if seen.keys.include?(previous_idx)

  (previous_idx..logo.length - 1).each do |idx|
    logo_comp = logo[previous_idx..idx].join('')
    next unless towels.include?(logo_comp)

    # If/when we make it to the end is when we initialize our first found index and return
    if idx == logo.length - 1
      seen[previous_idx] += 1
      break
    end
    # If we're not at the end we keep pushing forward to see where else we can add possibilities
    seen[previous_idx] += rearrange_forward(towels, logo, idx + 1, seen)
  end
  seen[previous_idx]
end

# puts "Part 1/2: #{program(input_array)}"
