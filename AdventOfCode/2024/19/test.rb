# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'r, wr, b, g, bwu, rb, gb, br, a, aq, x',
      '',
      'brwrr',
      'bggr',
      'gbbr',
      'rrbgbr',
      'ubwu',
      'bwurrg',
      'brgr',
      'bbrgwb'
    ]
  end

  def test_code
    available = @raw_input[0].split(', ')
    logos = @raw_input[2..]
    assert_equal 2, rearrange_forward(available, logos[0].split(''), 0, Hash.new(0))
    assert_equal 1, rearrange_forward(available, logos[1].split(''), 0, Hash.new(0))
    assert_equal 4, rearrange_forward(available, logos[2].split(''), 0, Hash.new(0))
    assert_equal 6, rearrange_forward(available, logos[3].split(''), 0, Hash.new(0))
    assert_equal 0, rearrange_forward(available, logos[4].split(''), 0, Hash.new(0))
    assert_equal 1, rearrange_forward(available, logos[5].split(''), 0, Hash.new(0))
    assert_equal 2, rearrange_forward(available, logos[6].split(''), 0, Hash.new(0))
    assert_equal 0, rearrange_forward(available, logos[7].split(''), 0, Hash.new(0))
    assert_equal [6, 16], program(@raw_input)
  end
end
