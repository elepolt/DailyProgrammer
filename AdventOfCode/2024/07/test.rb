# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '190: 10 19',
      '3267: 81 40 27',
      '83: 17 5',
      '156: 15 6',
      '7290: 6 8 6 15',
      '161011: 16 10 13',
      '192: 17 8 14',
      '21037: 9 7 18 13',
      '292: 11 6 16 20'
    ]
    @raw_input2 = [
      '190: 10 19',
      '3267: 81 40 27',
      '83: 17 5',
      '156: 15 6',
      '7290: 6 8 6 15',
      '161011: 16 10 13',
      '108: 4 5 3 9',
      '192: 17 8 14',
      '21037: 9 7 18 13',
      '292: 11 6 16 20',
      '108: 2 2 5 12'
    ]
  end

  def test_code
    inputs = parse_inputs(@raw_input)
    assert_equal [156, [15, 6]], inputs[3]
    assert_equal [192, [17, 8, 14]], inputs[6]
    assert_equal [29, 190].sort, get_possibles([10, 19], false)
    assert_equal [148, 3267, 3267, 87_480].sort, get_possibles([81, 40, 27], false)
    assert_equal [3749, 11_387], program(@raw_input)
    assert_equal [3965, 11_603], program(@raw_input2)
  end
end
