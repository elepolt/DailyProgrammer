# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  ans1 = 0
  ans2 = 0
  inputs = parse_inputs(array)
  inputs.each do |input|
    check = input[0]
    nums = input[1]

    possibles = get_possibles(nums.dup, false)
    ans1 += check if possibles.include?(check)
    possibles = get_possibles(nums.dup, true)
    ans2 += check if possibles.include?(check)
  end
  [ans1, ans2]
end

def get_possibles(nums, combine)
  possibles = [nums.shift]
  nums.each do |num|
    next_possibles = []
    possibles.each do |ans|
      next_possibles.append(ans + num)
      next_possibles.append(ans * num)
      next unless combine

      next_possibles.append("#{ans}#{num}".to_i)
    end
    possibles = next_possibles
  end
  possibles.sort
end

def parse_inputs(array)
  input = []
  array.each do |line|
    sum = line.split(': ')[0].to_i
    nums = line.split(': ')[1].split(' ').map(&:to_i)
    input.append([sum, nums])
  end
  input
end

# puts "Part 1/2: #{program(input_array)}"
