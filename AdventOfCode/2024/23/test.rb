# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'kh-tc',
      'qp-kh',
      'de-cg',
      'ka-co',
      'yn-aq',
      'qp-ub',
      'cg-tb',
      'vc-aq',
      'tb-ka',
      'wh-tc',
      'yn-cg',
      'kh-ub',
      'ta-co',
      'de-co',
      'tc-td',
      'tb-wq',
      'wh-td',
      'ta-ka',
      'td-qp',
      'aq-cg',
      'wq-ub',
      'ub-vc',
      'de-ta',
      'wq-aq',
      'wq-vc',
      'wh-yn',
      'ka-de',
      'kh-ta',
      'co-tc',
      'wh-qp',
      'tb-vc',
      'td-yn'
    ]
  end

  def test_code
    assert_equal [7, 'co,de,ka,ta'], program(@raw_input)
  end
end
