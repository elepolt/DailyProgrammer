# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  computers = create_dict(array)
  ans1 = 0
  sets = Set.new
  computers.each do |name, comps|
    comps.each do |comp|
      tmp1 = comps - [comp]
      tmp2 = computers[comp] - [name]
      next if (tmp1 & tmp2).empty?

      (tmp1 & tmp2).each do |tmp3|
        sets.add([name, comp, tmp3].sort)
      end
    end
  end
  sets.each do |comps|
    ans1 += 1 if comps.select { |c| c[0] == 't' }.length.positive?
  end

  ans2 = []
  computers.each do |key, comps|
    keys = [key] + comps
    tmp_sets = []
    comps.each do |comp|
      tmp_keys = [comp] + computers[comp]
      tmp2 = keys & tmp_keys
      tmp_sets.append(tmp2.sort)
    end
    tmp_sets.tally.each do |tly, val|
      ans2 = tly if tly.length > ans2.length && tly.length - 1 == val
    end
  end

  [ans1, ans2.sort.join(',')]
end

def create_dict(array)
  computers = {}
  array.each do |pair|
    comps = pair.split('-')
    computers[comps[0]] = [] unless computers.keys.include?(comps[0])
    computers[comps[0]].append(comps[1])
    computers[comps[1]] = [] unless computers.keys.include?(comps[1])
    computers[comps[1]].append(comps[0])
  end
  computers
end

puts "Part 1/2: #{program(input_array)}"
