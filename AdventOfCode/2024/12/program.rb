# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  board = Board.new(array, false)
  regions = get_regions(board)
  ans1 = 0
  ans2 = 0
  regions.each do |region|
    fencing = 0
    region.each do |coord|
      fencing += count_fencing(board, coord)
    end
    ans1 += region.length * fencing
    ans2 += region.length * count_sides(region)
  end

  [ans1, ans2]
end

def get_regions(board)
  regions = []
  cells = board.cells.map{|coord, _cell| coord}.to_set
  while cells.length > 0
    region = traverse_region(board, cells.first, [])
    regions.append(region)
    region.each do |coord|
      cells.delete(coord)
    end
  end
  regions
end

def traverse_region(board, coord, visited)
  visited += [coord]
  neighbors = board.neighbors(coord, 0, false)
  neighbors.each do |neighbor|
    next if visited.include?(neighbor)

    visited += traverse_region(board, neighbor, visited) if board.cells[neighbor] == board.cells[coord]
  end
  visited.uniq
end

def count_fencing(board, coord)
  fencing = 0
  fencing += 1 if coord[0] == 0 || coord[0] == board.height
  fencing += 1 if coord[1] == 0 || coord[1] == board.width
  # pp coord
  neighbors = board.neighbors(coord, 0, false)
  neighbors.each do |neighbor|
    fencing += 1 unless board.cells[neighbor] == board.cells[coord]
  end
  fencing
end

def count_sides(region)
  return 4 if region.length <= 2

  up = [-1, 0]
  left = [0, -1]
  upleft = [-1, -1]
  right = [0, 1]
  upright = [-1, 1]
  down = [1, 0]
  downleft = [1, -1]
  downright = [1, 1]

  # rows.each do |idx|
  #   row = region.select { |r| r[0] == idx }.sort
  sides = 0
  side_coords = []
  region.each do |cell|
    # If there's nothing to my left, and nothing above me, we have a side
    if !region.include?(coord_math(cell, left))
      # If there's nothing above me or below me we have a side
      if !region.include?(coord_math(cell, up))
        pp ['up, left', cell]
        side_coords.append(cell)
        sides += 1
        # next
      elsif region.include?(coord_math(cell, upleft))
        pp ['upleft', cell]
        side_coords.append(cell)
        sides += 1
      end
    end
    # If there's nothing to my left, and nothing above me, we have a side
    if !region.include?(coord_math(cell, right))
      # If there's nothing above me or below me we have a side
      if !region.include?(coord_math(cell, up))
        pp ['up, right', cell]
        side_coords.append(cell)
        sides += 1
        # next
      elsif region.include?(coord_math(cell, upright))
        pp ['upright', cell]
        side_coords.append(cell)
        sides += 1
      end
    end
    # If there's nothing to my left, and nothing above me, we have a side
    if !region.include?(coord_math(cell, down))
      # If there's nothing above me or below me we have a side
      if !region.include?(coord_math(cell, left))
        pp ['down, left', cell]
        side_coords.append(cell)
        sides += 1
        # next
      elsif region.include?(coord_math(cell, downleft))
        pp ['downleft', cell]
        side_coords.append(cell)
        sides += 1
      end
    end
    # If there's nothing to my left, and nothing above me, we have a side
    if !region.include?(coord_math(cell, down))
      # If there's nothing above me or below me we have a side
      if !region.include?(coord_math(cell, right))
        pp ['down, right', cell]
        side_coords.append(cell)
        sides += 1
        # next
      elsif region.include?(coord_math(cell, downright))
        pp ['downright', cell]
        side_coords.append(cell)
        sides += 1
      end
    end
    pp side_coords
    # sides += 1  && ( !region.include?(coord_math(cell, upleft)))
    # sides += 1 if !region.include?(coord_math(cell, right)) && !region.include?(coord_math(cell, up))
    # sides += 1 if !region.include?(coord_math(cell, left)) && !region.include?(coord_math(cell, down))
    # sides += 1 if !region.include?(coord_math(cell, right)) && !region.include?(coord_math(cell, down))
  end
  sides
end

def coord_math(a, b)
  dx = a[0] + b[0]
  dy = a[1] + b[1]
  [dx, dy]
end