# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'RRRRIICCFF',
      'RRRRIICCCF',
      'VVRRRCCFFF',
      'VVRCCCJFFF',
      'VVVVCJJCFE',
      'VVIVCCJJEE',
      'VVIIICJJEE',
      'MIIIIIJJEE',
      'MIIISIJEEE',
      'MMMISSJEEE'
    ]
  end

  def test_code
    board = Board.new(@raw_input, false)
    # assert_equal 2, count_fencing(board, [0, 0])
    # assert_equal 2, count_fencing(board, [2, 0])
    # assert_equal 3, count_fencing(board, [5, 3])
    regions = get_regions(board)
    assert_equal 12, traverse_region(board, [0,0], []).length
    assert_equal 12, regions[0].length
    # get_corners(board, regions[0])
    # assert_equal 11, .length

    # assert_equal regions['R'], { 'count': 12, 'fencing': 18 }

    # assert_equal regions['I'], { 'count': 4, 'fencing': 8 }, 'Messed up on I'
    # assert_equal regions['C'], { 'count': 14, 'fencing': 28 }, 'Messed up on C'
    # assert_equal regions['F'], { 'count': 10, 'fencing': 18 }, 'Messed up on F'
    # assert_equal regions['V'], { 'count': 13, 'fencing': 20 }, 'Messed up on V'
    # assert_equal regions['J'], { 'count': 11, 'fencing': 20 }, 'Messed up on J'
    # assert_equal regions['C'], { 'count': 1, 'fencing': 4 }, 'Messed up on C'
    # assert_equal regions['E'], { 'count': 13, 'fencing': 18 }, 'Messed up on E'
    # assert_equal regions['I'], { 'count': 14, 'fencing': 22 }, 'Messed up on I'
    # assert_equal regions['M'], { 'count': 5, 'fencing': 12 }, 'Messed up on M'
    # assert_equal regions['S'], { 'count': 3, 'fencing': 8 }, 'Messed up on S'
    # assert_equal 10, count_sides(board, regions[0])
    # assert_equal 10, count_sides(board, regions[4])

    # assert_equal 5, count_sides(regions[0]), "Goofed, 0: #{regions[0]}"
    # assert_equal 4, count_sides(regions[1]), "Goofed, 1: #{regions[1]}"
    assert_equal 22, count_sides(regions[2]), "Goofed, 2: #{regions[2]}"
    # assert_equal 12, count_sides(regions[3]), "Goofed, 3: #{regions[3]}"
    # assert_equal 10, count_sides(regions[4]), "Goofed, 4: #{regions[4]}"
    # assert_equal 12, count_sides(regions[5]), "Goofed, 5: #{regions[5]}"
    # assert_equal 4, count_sides(regions[6]), "Goofed, 6: #{regions[6]}"
    # assert_equal 8, count_sides(regions[7]), "Goofed, 7: #{regions[7]}"
    # assert_equal 16, count_sides(regions[8]), "Goofed, 8: #{regions[8]}"
    # assert_equal 6, count_sides(regions[9]), "Goofed, 9: #{regions[9]}"
    # assert_equal 6, count_sides(regions[10]), "Goofed, 10: #{regions[10]}"

    # assert_equal [1930, 1206], program(@raw_input)
  end
end
