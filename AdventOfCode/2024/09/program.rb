# frozen_string_literal: true

require_relative '../../base'

line = read_lines('input.txt')[0]
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(line)
  ans1 = part1(map_string(line))
  filemap = map_string(line)
  ans2 = part2(filemap.dup)

  [ans1, ans2]
end

def part1(filemap)
  keys = filemap.keys.to_a.select(&:odd?)
  keys.each do |key|
    fill_space(filemap, key)
  end
  ans1 = 0
  filemap.values.flatten.each_with_index do |value, idx|
    ans1 += idx * value
  end
  ans1
end

def part2(filemap)
  values = filemap.values.flatten
  max_value = (filemap.values.flatten - ['.']).max
  available = find_space(values)
  (1..max_value).to_a.reverse.each do |key|
    move_space(values, key, available)
  end
  ans2 = 0
  values.each_with_index do |value, idx|
    next if value == '.'

    ans2 += idx * value
  end
  ans2
end

def map_string(string)
  filemap = {}
  mapping_index = 0
  string.split('').map(&:to_i).each_with_index do |num, idx|
    if idx.odd?
      filemap[idx] = ['.'] * num
      next
    end
    filemap[idx] = [mapping_index] * num
    mapping_index += 1
  end
  filemap
end

def fill_space(filemap, space)
  return unless filemap.keys.include?(space)

  moved = 0
  fill_count = filemap[space].length
  filemap[space] = []
  while filemap[space].count < fill_count
    last_key = filemap.keys.last
    break if last_key == space

    value = filemap[last_key].pop
    filemap.delete(last_key) if filemap[last_key].empty?
    next if value == '.' || value.nil?

    moved += 1
    filemap[space].append(value)
  end
end

def move_space(values, filekey, available)
  space_needed = values.count(filekey)
  filekey_index = values.find_index(filekey)
  return if space_needed.zero?

  index = available.find_index { |av| av.count >= space_needed }
  return if index.nil?

  space = available[index]
  space_needed.times do |idy|
    values[space.shift] = filekey
    values[filekey_index + idy] = '.'
  end

  available.delete_at(index) if space.empty?
end

def find_space(values)
  # Build a stack of available space indices
  spaces = []
  tmp_space = []
  values.each_with_index do |val, idx|
    next if val != '.' && tmp_space.empty?

    if val != '.' && !tmp_space.empty?
      spaces.append(tmp_space)
      tmp_space = []
      next
    end

    tmp_space.append(idx)
  end
  spaces
end

# 946113020036 too low
puts "Part 1/2: #{program(line)}"
