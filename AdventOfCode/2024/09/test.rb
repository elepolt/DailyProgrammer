# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = '2333133121414131402'
  end

  def test_code
    filemap = map_string('12345')
    # binding.irb
    assert_equal [0], filemap[0]
    assert_equal %w[. .], filemap[1]
    assert_equal [1, 1, 1], filemap[2]
    assert_equal %w[. . . .], filemap[3]
    assert_equal [2, 2, 2, 2, 2], filemap[4]

    # fill_space(filemap, 1)
    # assert_equal [2, 2], filemap[1]
    # fill_space(filemap, 3)
    # assert_equal [2, 2, 2], filemap[3]
    # assert_equal 1928, program('12345')
    # filemap = map_string(@raw_input)
    # assert_equal [0], filemap[0]
    # assert_equal %w[. .], filemap[1]
    # assert_equal [1, 1, 1], filemap[2]
    # assert_equal %w[. . . .], filemap[3]
    filemap = map_string(@raw_input)
    values = filemap.values.flatten
    spaces = find_space(values)
    assert_equal [2, 3, 4], spaces[0]
    # pp values
    # assert_equal 2858, part2(filemap)

    assert_equal [1928, 2858], program(@raw_input)
    move_space(values, 9, spaces)
    move_space(values, 7, spaces)
    move_space(values, 4, spaces)
    move_space(values, 2, spaces)
    pp values
  end
end
