# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '#####',
      '.####',
      '.####',
      '.####',
      '.#.#.',
      '.#...',
      '.....',
      '',
      '#####',
      '##.##',
      '.#.##',
      '...##',
      '...#.',
      '...#.',
      '.....',
      '',
      '.....',
      '#....',
      '#....',
      '#...#',
      '#.#.#',
      '#.###',
      '#####',
      '',
      '.....',
      '.....',
      '#.#..',
      '###..',
      '###.#',
      '###.#',
      '#####',
      '',
      '.....',
      '.....',
      '.....',
      '#....',
      '#.#..',
      '#.#.#',
      '#####'
    ]
  end

  def test_code
    boards = build_boards(@raw_input)
    # pp boards.count
    assert_equal false, compare_boards(boards[0], boards[2])
    assert_equal [3, nil], program(@raw_input)
  end
end
