# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  boards = build_boards(array)
  ans1 = 0
  boards.combination(2).each do |board12|
    ans1 += 1 if compare_boards(board12[0], board12[1])
  end
  [ans1, nil]
end

def compare_boards(board1, board2)
  overlaps = board1.cells.select { |_c, v| v == '#' }.keys & board2.cells.select { |_c, v| v == '#' }.keys
  overlaps.empty?
end

def build_boards(array)
  boards = []
  tmp_board = []
  array.each do |line|
    if line == ''
      boards.append(Board.new(tmp_board, false))
      tmp_board = []
      next
    end

    tmp_board.append(line)
  end
  boards.append(Board.new(tmp_board, false))

  boards
end
puts "Part 1/2: #{program(input_array)}"
