# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    # ax + by = c
    # dx + ey = f
    # 94x + 22y = 8400
    # 34x + 67y = 5400
    # (c*e - b*f)/(a*e - b*d)
    @raw_input = [
      'Button A: X+94, Y+34',
      'Button B: X+22, Y+67',
      'Prize: X=8400, Y=5400',
      '',
      'Button A: X+26, Y+66',
      'Button B: X+67, Y+21',
      'Prize: X=12748, Y=12176',
      '',
      'Button A: X+17, Y+86',
      'Button B: X+84, Y+37',
      'Prize: X=7870, Y=6450',
      '',
      'Button A: X+69, Y+23',
      'Button B: X+27, Y+71',
      'Prize: X=18641, Y=10279'
    ]
    @test2 = [
      'Button A: X+94, Y+34',
      'Button B: X+22, Y+67',
      'Prize: X=10000000008400, Y=10000000005400',
      '',
      'Button A: X+26, Y+66',
      'Button B: X+67, Y+21',
      'Prize: X=10000000012748, Y=10000000012176',
      '',
      'Button A: X+17, Y+86',
      'Button B: X+84, Y+37',
      'Prize: X=10000000007870, Y=10000000006450',
      '',
      'Button A: X+69, Y+23',
      'Button B: X+27, Y+71',
      'Prize: X=10000000018641, Y=10000000010279'
    ]
  end

  def test_code
    configurations = parse(@raw_input)
    # pp configurations
    # assert_equal 
    # assert_equal 280, play_game2(configurations[0])
    assert_equal false, play_game2(configurations[1])
    # assert_equal false, play_game2(configurations[2])
    # assert_equal 200, play_game2(configurations[3])
    # configurations = parse(@test2)
    # pp play_game2(configurations[0])
    # assert_equal [480, nil], program(@raw_input)
  end
end
