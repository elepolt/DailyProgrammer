# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  configurations = parse(array)
  ans1 = 0
  configurations.each do |configuration|
    solutions = play_game(configuration)
    next unless solutions.count.positive?

    pp solutions if solutions.count > 1
    solution = solutions.first
    ans1 += (solution[0] * 3 + solution[1])
  end
  ans2 = 0
  configurations.each do |configuration|
    configuration['prize'][:x] += 10_000_000_000_000
    configuration['prize'][:y] += 10_000_000_000_000
    tmp = play_game2(configuration)
    ans2 += tmp if tmp
  end
  [ans1, ans2]
end

def play_game(configuration)
  prize = configuration['prize']
  # binding.irb
  a_button = configuration['a']
  b_button = configuration['b']
  b_presses = []
  solutions = {}
  
  (prize[:x]/a_button[:x] + 1).times do |presses|
    coord = [a_button[:x] * presses, a_button[:y] * presses]

    b_presses = 0
    solution_found = false
    until solution_found
      next_coord = [coord[0] + b_button[:x] * b_presses, coord[1] + b_button[:y] * b_presses]

      if next_coord[0] == prize[:x] && next_coord[1] == prize[:y]
        solutions[presses] = [] unless solutions[presses]
        solutions[presses] = b_presses
        break
      end
      solution_found = next_coord[0] > prize[:x] || next_coord[1] > prize[:y]

      b_presses += 1
    end
  end
  solutions
end

# Linear Algebra!
def play_game2(configuration)
  prize = configuration['prize']
  a_button = configuration['a']
  b_button = configuration['b']
  # ax + by = c
  # dx + ey = f
  a = a_button[:x]
  b = b_button[:x]
  c = prize[:x]
  d = a_button[:y]
  e = b_button[:y]
  f = prize[:y]

  a_presses = (c*e - b*f)/(a*e - b*d)
  # 94x + 22y = 5400
  # (5400 - 94 * a_presses) / b
  b_presses = (c - a*a_presses) / b

  return false unless a_presses * a_button[:x] + b_presses * b_button[:x] == prize[:x]
  return false unless a_presses * a_button[:y] + b_presses * b_button[:y] == prize[:y]

  a_presses * 3 + b_presses
end
# def press_button(button)
#   coord = [a_button[:x] * presses, a_button[:y] * presses]
# end

def parse(array)
  configurations = []
  array.each_slice(4) do |strings|
    configuration = {}
    a_buttons = strings[0].split(': ')[1].split(', ').map{|conf| conf.split('+')}.flatten
    configuration['a'] = { 'x': a_buttons[1].to_i, 'y': a_buttons[3].to_i }
    b_buttons = strings[1].split(': ')[1].split(', ').map{|conf| conf.split('+')}.flatten
    configuration['b'] = { 'x': b_buttons[1].to_i, 'y': b_buttons[3].to_i }
    prize = strings[2].split('=')
    configuration['prize'] = { 'x': prize[1].to_i, 'y': prize[2].to_i }
    configurations.append(configuration)
  end
  configurations
end

puts "Part 1/2: #{program(input_array)}"
