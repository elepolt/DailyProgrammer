require_relative '../../base.rb'

input_array = read_lines('input.txt').map{|line| line.split('')}
game_array = []
input_array.each do |l|
  game_array.push(l)
end

def run_code(array, part_two = false)
  count = 0
  distance = part_two ? 8 : 0
  is_different = true
  while is_different
    tmp_array = array.clone # we need to check later to make sure there were no changes, so clone one here
    array = next_generation(array, distance)
    count += 1 # Not used but I like it
    is_different = array != tmp_array
  end
  array.flatten.count('#')
end

def next_generation(grid, distance = 0)
  next_grid = []
  grid.each_with_index do |x, idx|
    # Add the new row
    next_grid[idx] = []
    # Each with index here because the 'y' will be the actual cell (alive/dead)
    grid.each_with_index do |y, idy|
      # Don't forget to use the index, and not the actual value of the cell
      neighbors = traverse(grid, [idx, idy], distance)
      current_cell = grid[idx][idy]
      next_grid[idx][idy] = calculate_life(current_cell, neighbors, distance > 0)
    end
  end
  next_grid
end

def calculate_life(current_cell, neighbors, part_two = false)
  return current_cell if current_cell == '.'
  # This hash got messy, might go back to a simple array...
  neighbors['L'] = 0 unless neighbors.keys.include?('L')
  empty_seats = neighbors['L']
  neighbors['#'] = 0 unless neighbors.keys.include?('#')
  filled_seats = neighbors['#']
  neighbors['.'] = 0 unless neighbors.keys.include?('.')
  floor_tiles = neighbors['.']


  # Default the cell to be filled
  next_generation_cell = current_cell

  # Fill a seat if the seat is empty and all surrounding seats
  next_generation_cell = '#' if current_cell == 'L' && filled_seats == 0

  # A filled seat becomes empty if there are 4 or more filled seats surrounding it
  seat_limit = part_two ? 4 : 5
  next_generation_cell = 'L' if current_cell == '#' && filled_seats >= seat_limit
  next_generation_cell
end

def traverse(grid, starting_coords, distance = 0)
  neighbors = Hash.new(0) # Creates a new hash with all default values being zero
  # This neat nested loop traverses the surrounding box of a given index
  [-1, 0, 1].each do |idx|
    [-1, 0, 1].each do |idy|
      # skip if it's the starting_coords
      next if idx == 0 && idy == 0
      x = starting_coords[0] + idx
      y = starting_coords[1] + idy
      # Skip if we're at an edge
      next if x < 0 || x >= grid.length
      next if y < 0 || y >= grid[0].length
      cell = grid[x][y]
      neighbors[cell] += 1
      # AoC D11 P2; This is where Game of Life ultimately ends
      # AoC is asking us to go further than the surrounding blocks to find the first
      # seat, up to a distance away. Step one though is that if we've found a seat,
      # we should move on to the next iteration
      next if cell != '.'
      distance.times do
        # Continue adding our idx/y value from above, which will move
        # us neatly in all directions
        x += idx
        y += idy
        # Break when we get to an edge
        break if x < 0 || x >= grid.length
        break if y < 0 || y >= grid[0].length
        cell = grid[x][y]
        neighbors[cell] += 1
        # AoC Requirement to continue until we aren't on a floor
        break if cell != '.'
      end
    end
  end
  neighbors
end

# puts "Part 1: #{run_code(game_array)}"
# 2214 is my answer, but my code as is is printing 2213
puts "Part 2: #{run_code(input_array, true)}"