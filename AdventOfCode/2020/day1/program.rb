require_relative '../../base.rb'

input_array = parse_intengers('input.txt')

# Input the array and how many numbers we want to add together
def find_sum_2(array, summation_count)
  # Brute force again using the given number
  combinations = array.combination(summation_count).to_a
  # utilize inject to get the numbers that add up, and then to multiply together
  correct_sum = combinations.select{|x| x.inject(:+) == 2020}.first
  correct_sum.inject(:*)
end

puts find_sum_2(input_array, 2)
puts find_sum_2(input_array, 3)