require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  def setup
    @raw_input = [1721, 979, 366, 299, 675, 1456]
  end
  # 

  def test_sum
    assert_equal 514579, find_sum(@raw_input)
  end

  def test_sum_2
    assert_equal 241861950, find_sum_2(@raw_input, 3)
  end

  def test_sum_original
    assert_equal 514579, find_sum_2(@raw_input, 2)
  end
end
