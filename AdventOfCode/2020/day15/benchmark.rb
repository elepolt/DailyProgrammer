require 'benchmark'

# So I have found out so much about optimizations today....
# In order of fastest to slowest:
# dict[num] -> dict.key?(num) -> dict.keys.include?(num)
# Really there is no point in ever doing dict.keys.include?(num) since 
# in reality it's doing the exact same thing as dict.key?(num), but
# probably just converting everything to an array first, which is costly

# So my simple test here is to fill a dictionary with values
# {
#   0 => 0
#   1 => 1
#   2 => 2
#   ....
#   60000 => 60000
# }
# Then loop through them all, checking that they exist, and += 1 to a variable
map = Hash.new { |h, v| h[v] = [] }
puts Benchmark.measure {
  (0...60000).each do |turns|
    map[turns] << turns
  end
}

i = 0
# This takes over 10 seconds!
puts Benchmark.measure {
  (0...60000).each do |turns|
    i += 1 if map.keys.include?(turns)
  end
}

# All of the ones beneath this are all under .02
puts Benchmark.measure {
  (0...60000).each do |turns|
    i += 1 if map.key?(turns)
  end
}
puts Benchmark.measure {
  (0...60000).each do |turns|
    i += 1 if map[turns].length > 0
  end
}
puts Benchmark.measure {
  (0...60000).each do |turns|
    i += 1 if map[turns]
  end
}
