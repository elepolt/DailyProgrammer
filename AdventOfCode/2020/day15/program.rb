require_relative '../../base.rb'
require 'benchmark'

input_array = split_string(read_lines('input.txt')[0], ',').map(&:to_i)

def run_memory(array, turn_count)
  turns = []
  counts = Hash.new { |h, v| h[v] = [] }
  last_num = -1
  idx = 0
  # Ok, go through the array because all of the numbers there are 'starting numbers'
  # so they get set into our dictionary as counted
  array.each do |num|
    idx += 1
    counts[num] << idx
    turns.push(num)
    last_num = num
  end
  # We have an odd starting concept... 
  idx += 1 
  last_num = 0
  turns.push(last_num)
  counts[last_num] << idx

  while idx < turn_count
    idx += 1
    last_num = last_number(last_num, idx, counts)
    counts[last_num] << idx
  end

  [last_num, nil]
end

# If the number has been counted, we need to find the turn
# in which it was last said and subtract that from our current turn
# turns: [0,3,6] - turn: 4 - num: 6 should return 4-1
# check where the last time it was said
# otherwise we are returning 0, because it has only been seen once
def last_number(num, turn, dict)
  last_num = 0
  if dict[num].length >= 2
    last_two = dict[num].last(2)
    last_num = last_two[1] - last_two[0]
  end
  last_num
end

puts "Part [1]: #{run_memory(input_array, 2020)}"
puts "Part [2]: #{run_memory(input_array, 30000000)}"
