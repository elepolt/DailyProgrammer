require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [0, 3, 6]
  end

  def test_memory
    assert_equal [436, nil], run_memory(@raw_input, 2020)
    assert_equal [0, nil], run_memory(@raw_input, 4)
    assert_equal [3, nil], run_memory(@raw_input, 5)
    assert_equal [3, nil], run_memory(@raw_input, 6)
    assert_equal [1, nil], run_memory(@raw_input, 7)
    assert_equal [0, nil], run_memory(@raw_input, 8)
    assert_equal [4, nil], run_memory(@raw_input, 9)
    assert_equal [0, nil], run_memory(@raw_input, 10)

    # assert_equal [175594, nil], run_memory([0,3,6], 30000000)
    # assert_equal [2578, nil], run_memory([1,3,2], 30000000)
    # assert_equal [3544142, nil], run_memory([2,1,3], 30000000)
    # assert_equal [261214, nil], run_memory([1,2,3], 30000000)
    # assert_equal [6895259, nil], run_memory([2,3,1], 30000000)
    # assert_equal [18, nil], run_memory([3,2,1], 30000000)
    # assert_equal [362, nil], run_memory([3,1,2], 30000000)

  end
end
