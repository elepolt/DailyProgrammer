require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'F10',
      'N3',
      'F7',
      'R90',
      'F11'
    ]
  end

  # def test_all
  #   assert_equal 25, sail(@raw_input, false)
  # end

  # def test_left_turn
  #   input = @raw_input.push('L180').push('F11')
  #   assert_equal 20, sail(input, false)
  # end

  def test_waypoints
    assert_equal 286, sail(@raw_input, true)
  end
end
