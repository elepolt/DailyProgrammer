require_relative '../../base.rb'

input_array = read_lines('input.txt')

def sail(array, use_waypoints)
  # Start our ship facing east
  directions = ['N', 'E', 'S', 'W']
  current_dir = 1
  # Init our hash of distancces
  direction_dict = Hash['N' => 0, 'W' => 0, 'S' => 0, 'E' => 0]
  # Part two init our waypoint
  waypoint = [1, 10, 0, 0]
  array.each do |command|
    # Parse our command to get direcion and number
    direction = command[0]
    distance = command[1..-1].to_i
    # Handle left/right turn
    if ['L','R'].include?(direction)
      # Distance here is in degrees of turning
      turn_amount = distance / 90
      # If it's left we need to go negative
      turn_amount *= -1 if direction == 'L'
      if use_waypoints
        # However when we rotate we need to negate that negative
        # [1,10,0,0].rotate(1) == [10,0,0,1]
        waypoint.rotate!(-turn_amount)
      else
        current_dir += turn_amount
        current_dir = current_dir % 4
      end
    elsif direction == 'F'
      # This block could probably be cleaned up a bit,
      # we simply loop through our waypoint and multiply our distance
      if use_waypoints
        waypoint.each_with_index do |waypoint_dist, idx|
          direction_dict[directions[idx]] += waypoint_dist * distance
        end
      else
        # Else we just add the distance to our current direction
        direction_dict[directions[current_dir]] += distance
      end
    else
      if use_waypoints
        # Add distance to our waypoint for part2
        dir_index = directions.find_index(direction)
        waypoint[dir_index] += distance
      else
        direction_dict[direction] += distance
      end
    end
  end
  ew = (direction_dict['E'] - direction_dict['W']).abs
  ns = (direction_dict['N'] - direction_dict['S']).abs
  ew + ns
end

# 1082
# puts "Part 1: #{sail(input_array, false)}"
puts "Part 2: #{sail(input_array, true)}"
