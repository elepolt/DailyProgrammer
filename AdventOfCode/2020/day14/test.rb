require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'mask = 000000000000000000000000000001XXXX0X',
      'mem[8] = 11',
      'mem[7] = 101',
      'mem[8] = 0'
    ]
  end

  def test_bitmask
    assert_equal [165, 3232], run_code(@raw_input)
  end

  def test_bitmask_two
    input = @raw_input + [
      'mem[9] = 11',
      'mem[10] = 101',
      'mem[9] = 0'
    ]
    assert_equal [165*2, 3232], run_code(input.flatten)
  end

  def test_bitmask_three
    input = @raw_input + [
      'mem[9] = 11',
      'mem[10] = 101',
      'mem[9] = 0',
      'mem[90] = 11',
      'mem[100] = 101',
      'mem[90] = 0'
    ]
    assert_equal [165*3, 3232], run_code(input.flatten)
  end

  def test_mask_value
    assert_equal 73, mask_value('XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X', 11, 'X').to_i(2)
    assert_equal 101, mask_value('XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X', 101, 'X').to_i(2)
    assert_equal '000000000000000000000000000000X1101X', mask_value('000000000000000000000000000000X1001X', 42, '0')
    assert_equal '00000000000000000000000000000001X0XX', mask_value('00000000000000000000000000000000X0XX', 26, '0')
  end

  def test_part_two
    input = [
      'mask = 000000000000000000000000000000X1001X',
      'mem[42] = 100',
      'mask = 00000000000000000000000000000000X0XX',
      'mem[26] = 1',
    ]
    assert_equal [51, 208], run_code(input.flatten)
  end

  def test_all_combos
    assert_equal [1], all_combos([0,1]).sort
    assert_equal [1,2,3], all_combos([0,1,2]).sort
    assert_equal [1, 2, 3, 8, 9, 10, 11], all_combos([0,1,2,8]).sort
  end
end
