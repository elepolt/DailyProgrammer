require_relative '../../base.rb'

input_array = read_lines('input.txt')

def run_code(array)
  mask = ''
  memories = Hash[]
  memories_two = Hash[]
  array.each do |mask_string|
    a,b = mask_string.split(' = ')
    if a == 'mask'
      mask = b
    else
      mem_address = a.match('\d+').to_s.to_i
      value = b.to_i
      # Part 1 is simply setting the given address to the value from the mask
      memories[mem_address] = mask_value(mask, value, 'X').to_i(2)

      # Part two gets a similar masked situation, but leaves 0s alone instead of Xs
      part_two_sums = [0] # See below, but we need 0 to be in the array
      part_two_val = mask_value(mask, mem_address, '0')
      # Then we get a little clever. My goal is to get an array of all
      # the values that could change, ie: X110X should return [1,16]
      part_two_val.split('').reverse.each_with_index do |x, idx|
        part_two_sums.push(2**idx) if x == 'X'
      end
      # Now find the lowest value possible by changing Xs to 0s and converting to decimal
      lowest_value = part_two_val.gsub('X', '0').to_i(2)
      memories_two[lowest_value] = value
      # Now we loop through every possible math condition and add the values to the lowest possible
      # value, keeping track of said values in the part_two dictionary
      all_combos(part_two_sums).each do |sum|
        memories_two[lowest_value+sum] = value
      end
    end
  end
  [memories.values.sum, memories_two.values.sum]
end

def mask_value(mask, value, skip_char)
  # I am like 99% sure there's a better way to do this
  # But this was literal logic. Get our value to binary
  bin_val = value.to_s(2)
  # Add zeroes to the beginning of it to match the length of our mask
  zeroes = mask.length - bin_val.length
  # Then split it into an array
  bin_val = ('0'*zeroes).concat(bin_val).split('')
  mask.split('').each_with_index do |mask_val, idx|
    next if mask_val == skip_char
    # and simply overwrite the binary value at the index
    bin_val[idx] = mask_val
  end
  bin_val.join('')
end

# This is simply getting every possible combination of sums from an array
# See unit test for examples
def all_combos(array)
  combos = (2..array.length).flat_map{|size| array.combination(size).to_a }
  sums = []
  combos.each do |combo|
    sums.push(combo.sum)
  end
  sums.uniq
end

puts "Part [1,2]: #{run_code(input_array)}"