require_relative '../../base.rb'

input_array = read_lines('input.txt')

# Input the array and how many numbers we want to add together
def valid_count(array, check_positions)
  count = 0
  array.each do |line|
    if check_positions
      count += 1 if valid_password_positions(line)
    else
      count += 1 if valid_password(line)
    end
  end
  count
end

def valid_password(input_string)
  # All the splits that I need to get the password requirements, and the password itself
  rules, password = split_string(input_string, ':')
  password.strip!
  numbers, letter = split_string(rules, ' ')
  min, max = split_string(numbers, '-').map(&:to_i)

  min <= password.count(letter) && password.count(letter) <= max
end

def valid_password_positions(input_string)
  # All the splits that I need to get the password requirements, and the password itself
  rules, password = split_string(input_string, ':')
  password.strip!
  numbers, letter = split_string(rules, ' ')
  pos_1, pos_2 = split_string(numbers, '-').map(&:to_i)

  # Get the two positions in question and make sure only one of them has the letter
  positions = [password[pos_1-1], password[pos_2-1]]
  positions.count(letter) == 1
end

puts valid_count(input_array, false)
puts valid_count(input_array, true) #168 is wrong