require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '1-3 a: abcde',
      '1-3 b: cdefg',
      '2-9 c: ccccccccc'
    ]
  end

  def test_first
    assert_equal true, valid_password(@raw_input[0])
  end
  def test_second
    assert_equal false, valid_password(@raw_input[1])
  end
  def test_third
    assert_equal true, valid_password(@raw_input[2])
  end

  def test_count
    assert_equal 2, valid_count(@raw_input, false)
  end

  def test_first_pos
    assert_equal true, valid_password_positions(@raw_input[0])
  end
  def test_second_pos
    assert_equal false, valid_password_positions(@raw_input[1])
  end
  def test_third_pos
    assert_equal false, valid_password_positions(@raw_input[2])
  end
  def test_exactly_one
    assert_equal true, valid_password_positions('1-3 a: cbade')
  end

  def test_count_positions
    assert_equal 1, valid_count(@raw_input, true)
  end
end
