require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      35,
      20,
      15,
      25,
      47,
      40,
      62,
      55,
      65,
      95,
      102,
      117,
      150,
      182,
      127,
      219,
      299,
      277,
      309,
      576,
    ]
  end

  def test_code
    assert_equal 127, run_code(@raw_input, 5)
  end

  def test_code_two
    assert_equal 62, find_code(@raw_input, 127)
  end
end
