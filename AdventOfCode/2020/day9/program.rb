require_relative '../../base.rb'

input_array = parse_intengers(read_lines('input.txt'))

def run_code(array, preamble_length)
  broken_numbers = [] # Making an assumption we'll need this list later
  # We start our array at whatever our preamble length is
  (preamble_length..array.length).each do |idx|
    # idx will start at whatever our preamble is and increase from there
    # So first steps is to get the previous preamble lengthed numbers
    preamble = array[(idx - preamble_length)..idx - 1]
    # Then get all the combinations and sums
    preamble_sums = preamble.combination(2).to_a.map{|c| c.sum}
    # And finally push into the broken numbers when it occurs
    broken_numbers.push(array[idx]) unless preamble_sums.include?(array[idx])
  end
  # This challenge only gave us a single broken number
  broken_numbers.first
end

# The goal here is to find a continous group of numbers that adds up to 
# our broken number above, then the 'code' is the min+max of those numbers
def find_code(array, broken_number)
  code = 0
  array.length.times do |idx|
    # There is only one code, so once we have it break
    break if code > 0
    # Start our next loop at +1 from our index
    (idx + 1..array.length).each do |idj|
      # Grab the array from idx to idj, which will increase
      # each idj loop, ie: [0..1], [0..2], [1..2], [1..3], etc
      temp_array = array[idx..idj]
      sum = temp_array.sum
      # If they sum higher break and move on to our next idx
      break if sum > broken_number
      # When we find it, set the code and break
      if sum == broken_number
        code = temp_array.min + temp_array.max
        break
      end
    end
  end
  code
end

puts "Part 1: #{run_code(input_array, 25)}"
puts "Part 2: #{find_code(input_array, run_code(input_array, 25))}"