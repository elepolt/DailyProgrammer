require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'light red bags contain 1 bright white bag, 2 muted yellow bags.',
      'dark orange bags contain 3 bright white bags, 4 muted yellow bags.',
      'bright white bags contain 1 shiny gold bag.',
      'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.',
      'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
      'dark olive bags contain 3 faded blue bags, 4 dotted black bags.',
      'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.',
      'faded blue bags contain no other bags.',
      'dotted black bags contain no other bags.',
    ]
    @bag_dictionary = {
      "light red"=>{"bright white"=>1, "muted yellow"=>2},
      "dark orange"=>{"bright white"=>3, "muted yellow"=>4},
      "bright white"=>{"shiny gold"=>1},
      "muted yellow"=>{"shiny gold"=>2, "faded blue"=>9},
      "shiny gold"=>{"dark olive"=>1, "vibrant plum"=>2},
      "dark olive"=>{"faded blue"=>3, "dotted black"=>4},
      "vibrant plum"=>{"faded blue"=>5, "dotted black"=>6}
    }
  end

  def test_count_bags
    assert_equal [4, 32], count_bags(@raw_input)
  end

  def test_recursion
    assert_equal 1, bag_check(@bag_dictionary, 'bright white')
  end

  def test_recursion_two
    assert_equal 3, bag_check(@bag_dictionary, 'light red')
  end

  def test_recursion_three
    assert_equal 0, bag_check(@bag_dictionary, 'faded blue')
  end

  def test_total_bags
    assert_equal 32, total_bags(@bag_dictionary, 'shiny gold', 1)
  end

  def test_total_bags_two
    assert_equal 11, total_bags(@bag_dictionary, 'vibrant plum', 1)
  end

  def test_total_bags_two
    assert_equal 22, total_bags(@bag_dictionary, 'vibrant plum', 2)
  end

  def test_total_bags_three
    new_dict = {
      "shiny gold"=>{"dark red"=>2},
      "dark red"=>{"dark orange"=>2},
      "dark orange"=>{"dark yellow"=>2},
      "dark yellow"=>{"dark green"=>2},
      "dark green"=>{"dark blue"=>2},
      "dark blue"=>{"dark violet"=>2},
      "dark violet"=>{}}
    assert_equal 126, total_bags(new_dict, 'shiny gold', 1)
  end

end
