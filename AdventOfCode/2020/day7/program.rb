require_relative '../../base.rb'

input_array = read_lines('input.txt')

def count_bags(array)
  part_one_count = 0
  part_two_count = 0
  bag_dictionary = Hash[]
  array.each do |bag_rules|
    bag_color, bag_contains  = bag_rules.split(' bags contain ')
    bag_dictionary[bag_color] = Hash[]

    # Leave the dictionary value empty if it cannot hold other bags
    next if bag_contains == 'no other bags.'

    # 1 shiny coral bag, 4 dotted purple bags.
    # becomes ['1 shiny coral bag', '4 dotted purple bags.']
    inside_bags = bag_contains.split(', ')
    inside_bags.each do |inside_bag|
      # 1 shiny coral bag
      # becomes ['1', 'shiny coral bag']
      number_color = inside_bag.split(' ')
      # Shift the array once for the number leaving ['shiny', 'coral', 'bag']
      number = number_color.shift
      # Pop once to remove the trailing 'bag.' or 'bags.' leaving ['shiny', 'coral']
      number_color.pop
      # Rejoin the array with a space leaving 'shiny coral'
      color = number_color.join(' ')
      # Then add it to the dictionary with number setting to int
      bag_dictionary[bag_color][color] = number.to_i
    end
  end
  # So now at this point we have a dictionary of what each bag can hold
  bag_dictionary.each do |bag_color, bag_contains|
    # And our goal is to figure out how many bags end up letting us have a shiny gold bag
    # but by default bag_check is passing back *how many* shiny bags can be held
    part_one_count += 1 if bag_check(bag_dictionary, bag_color) > 0
  end

  # Then part two recurses through to see how many total bags a single shiny gold bag can hold
  part_two_count = total_bags(bag_dictionary, 'shiny gold', 1)

  [part_one_count, part_two_count]
end

# Recursion!
def bag_check(bag_dictionary, bag_color)
  shiny_count = 0
  # If the key doesn't exist in the dictionary we won't continue
  return 0 unless bag_dictionary.keys.include?(bag_color)
  # If the bag includes shiny gold return the value
  return bag_dictionary[bag_color]['shiny gold'].to_i if bag_dictionary[bag_color].keys.include?('shiny gold')
  # Else loop through and recurse
  bag_dictionary[bag_color].keys.each do |new_bag_color|
    shiny_count += bag_check(bag_dictionary, new_bag_color)
  end
  shiny_count
end

# Recursion 2!
# This returns how many bags any color bag can ultimately hold
# IE: if we have two violet bags, and each violet bag contains four bags, that's 8
def total_bags(bag_dictionary, bag_color, bag_count)
  # If the key doesn't exist in the dictionary we won't continue
  return 0 unless bag_dictionary.keys.include?(bag_color)
  # Start with the total values of the bag, multiplied with how many bags were given,
  total_count = bag_dictionary[bag_color].values.sum * bag_count
  bag_dictionary[bag_color].each do |new_bag_color, new_bag_count|
    # Now recurse and make sure we continue multiplying how many bags are really there
    # IE, if our two violet bags contain four bags, whatever the values of the four bags
    # needs to also be multiplied by how many violet bags we have
    total_count += total_bags(bag_dictionary, new_bag_color, bag_count * new_bag_count)
  end
  total_count
end

puts count_bags(input_array)
