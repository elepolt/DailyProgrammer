require_relative '../../base.rb'
require 'pp'

input_array = read_lines('input.txt')
required_fields = [
  'byr', # (Birth Year)
  'iyr', # (Issue Year)
  'eyr', # (Expiration Year)
  'hgt', # (Height)
  'hcl', # (Hair Color)
  'ecl', # (Eye Color)
  'pid', # (Passport ID)
]
optional_fields = [
  'cid'  # (Country ID)
]

# Given our nicely formatted data, run through and count all the valid passports
# optional_fields is currently unused, not sure why that was even included in the challenge
def valid_count(array, required_fields, optional_fields)
  all_data_count = 0
  valid_data_count = 0
  array.each do |passport_data|
    # Split our passport data into key/values
    data = passport_data.split(' ').map {|p| p.split(':')[0]}
    values = passport_data.split(' ').map {|p| p.split(':')[1]}
    # And map them together into a hash
    passport_hash = Hash[data.zip(values)]
    # From the required fields, 'subtract' the data
    # If it's empty it means the passport contained all the required fields
    contains_required_fields = (required_fields - data).empty?
    if contains_required_fields
      all_data_count += 1 # Part 1
      valid_data_count += 1 if validate_passport(passport_hash) # Part 2
    end
  end
  [all_data_count, valid_data_count]
end

# Goal of this method is to get the broken lines of data into a single line
def split_passports(array)
  # Issue regarding the last passport on whether or not there's a trailing \n
  # So handle that by forcing a new line if one doesn't exist to round out the data
  array.push('') if array.last != ''

  # Then just loop through, stopping at each empty line and joining
  # the previous lines into a single line of passport data
  # This:
  # 'hcl:#cfa07d eyr:2025 pid:166559648',
  # 'iyr:2011 ecl:brn hgt:59in'
  # Becomes 'hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in'
  passport_arrays = []
  array.count('').times do |i|
    # Find the next empty line
    next_index = array.find_index('')
    # non-zeroed index so subtract 1 here
    passport_arrays.push(array[0..next_index-1].join(' '))
    array.shift(next_index+1)
  end
  passport_arrays
end

# We only reach here if we have all the keys in question,
# But I cannot validate if they are in the proper format
# This is a disaster
def validate_passport(passport_hash)
  byr_is_valid = number_range_validation(passport_hash['byr'].to_i, 4, 1920, 2002)
  iyr_is_valid = number_range_validation(passport_hash['iyr'].to_i, 4, 2010, 2020)
  eyr_is_valid = number_range_validation(passport_hash['eyr'].to_i, 4, 2020, 2030)

  hgt = passport_hash['hgt']
  cm_in = hgt[hgt.length-2..hgt.length]
  # hehe, I'm sure this is terrible practice but ruby's to_i will strip characters
  # IE: 124cm.to_i == 124
  height = hgt.to_i
  hgt_is_valid = false
  if cm_in == 'cm'
    hgt_is_valid = number_range_validation(height, nil, 150, 193)
  elsif cm_in == 'in'
    hgt_is_valid = number_range_validation(height, nil, 59 , 76)
  end

  # Not sure if it's worth trying to refactor this into its own function
  hcl = passport_hash['hcl']
  hcl_is_valid = hcl.length == 7 && !hcl.match('#\w{6}').nil?

  pid = passport_hash['pid']
  pid_is_valid = pid.length == 9 && !pid.match('\d{9}').nil?

  ecl = passport_hash['ecl']
  ecl_is_valid = %w[amb blu brn gry grn hzl oth].include?(ecl)
  
  # byr (Birth Year) - four digits; at least 1920 and at most 2002.
  # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
  # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
  # hgt (Height) - a number followed by either cm or in:
  #     If cm, the number must be at least 150 and at most 193.
  #     If in, the number must be at least 59 and at most 76.
  # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
  # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
  # pid (Passport ID) - a nine-digit number, including leading zeroes.
  # cid (Country ID) - ignored, missing or not.
  [byr_is_valid, hcl_is_valid, iyr_is_valid, ecl_is_valid, eyr_is_valid, pid_is_valid, hgt_is_valid].uniq == [true]
end

# For numbers that need to be checked for length, and size range
# If no length is passed in it will just check the min max
# IE: four digits; at least 1920 and at most 2002.
# This uses inclusivity, so keep that in mind
# Really.... we could probably over engineer this to take a dictionary of values and operators
def number_range_validation(number, length, min, max)
  is_valid = true
  if length.nil?
    is_valid = min <= number.to_i && number.to_i <= max
  else
    is_valid = number.to_s.length == length && min <= number.to_i && number.to_i <= max
  end
  is_valid
end

passport_data_counts = valid_count(split_passports(input_array), required_fields, optional_fields)
puts "Passport with all data count: #{passport_data_counts[0]}"
puts "Valid passport count: #{passport_data_counts[1]}"