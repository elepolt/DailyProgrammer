require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd',
      'byr:1937 iyr:2017 cid:147 hgt:183cm',
      '',
      'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884',
      'hcl:#cfa07d byr:1929',
      '',
      'hcl:#ae17e1 iyr:2013',
      'eyr:2024',
      'ecl:brn pid:760753108 byr:1931',
      'hgt:179cm',
      '',
      'hcl:#cfa07d eyr:2025 pid:166559648',
      'iyr:2011 ecl:brn hgt:59in'
    ]
    @required_fields = [
      'byr', # (Birth Year)
      'iyr', # (Issue Year)
      'eyr', # (Expiration Year)
      'hgt', # (Height)
      'hcl', # (Hair Color)
      'ecl', # (Eye Color)
      'pid', # (Passport ID)
    ].sort!
    @optional_fields = [
      'cid'  # (Country ID)
    ]
  end

  def test_first
    assert_equal [2, 2], valid_count(split_passports(@raw_input), @required_fields, @optional_fields)
  end

  def test_validate
    passport_hash = {
    'pid' => '087499704',
    'hgt' => '74in',
    'ecl' => 'grn',
    'iyr' => '2012',
    'eyr' => '2030',
    'byr' => '1980',
    'hcl' => '#623a2f'
    }
    
    assert_equal true, validate_passport(passport_hash)
  end
  
  def test_validate_true
    passport_hash = {
      'eyr' => '2029',
      'ecl' => 'blu',
      'cid' => '129',
      'byr' => '1989',
      'iyr' => '2014',
      'pid' => '896056539',
      'hcl' => '#a97842',
      'hgt' => '165cm',
    }
    
    assert_equal true, validate_passport(passport_hash)
  end
  
  def test_validate_true_two
    passport_hash = {
      'hcl' => '#888785',
      'hgt' => '164cm',
      'byr' => '2001',
      'iyr' => '2015',
      'cid' => '88',
      'pid' => '545766238',
      'ecl' => 'hzl',
      'eyr' => '2022'
    }
    
    assert_equal true, validate_passport(passport_hash)
  end
  
  def test_validate_true_three
    passport_hash = {
      'iyr' => '2010',
      'hgt' => '158cm',
      'hcl' => '#b6652a',
      'ecl' => 'blu',
      'byr' => '1944',
      'eyr' => '2021',
      'pid' => '093154719'
    }
    
    assert_equal true, validate_passport(passport_hash)
  end
  
  def test_validate_false
    passport_hash = {
      'hcl' => 'dab227',
      'iyr' => '2012',
      'ecl' => 'brn',
      'hgt' => '182cm',
      'pid' => '021572410',
      'eyr' => '2020',
      'byr' => '1992',
      'cid' => '277',
    }
    
    assert_equal false, validate_passport(passport_hash)
  end
  
  def test_validate_false_two
    passport_hash = {
      'eyr' => '1972',
      'cid' => '100',
      'hcl' => '#18171d',
      'ecl' => 'amb',
      'hgt' => '170',
      'pid' => '186cm',
      'iyr' => '2018',
      'byr' => '1926',
    }
    
    assert_equal false, validate_passport(passport_hash)
  end
  
  def test_validate_false_three
    passport_hash = {
      'iyr' => '2019',
      'hcl' => '#602927',
      'eyr' => '1967',
      'hgt' => '170cm',
      'ecl' => 'grn',
      'pid' => '012533040',
      'byr' => '1946',
    }
    
    assert_equal false, validate_passport(passport_hash)
  end
  
  def test_validate_false_four
    passport_hash = {
      'hgt' => '59cm',
      'ecl' => 'zzz',
      'eyr' => '2038',
      'hcl' => '74454a',
      'iyr' => '2023',
      'pid' => '3556412378',
      'byr' => '2007',
    }
    
    assert_equal false, validate_passport(passport_hash)
  end

  def test_passportsplit
    assert_equal 4, split_passports(@raw_input).length
  end

  def test_passportsplit_values
    split_values = [
      'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm',
      'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929',
      'hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm',
      'hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in',
    ]
    assert_equal split_values, split_passports(@raw_input)
  end

  def test_number_range_validation_works
    assert_equal true, number_range_validation(2001, 4, 1990, 2020)
  end

  def test_number_range_validation_false
    assert_equal false, number_range_validation(1989, 4, 1990, 2020)
  end
end
