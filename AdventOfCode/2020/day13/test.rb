require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '939',
      '7,13,x,x,59,x,31,19'
    ]
  end

  def test_all
    assert_equal [295, nil], catch_the_bus(@raw_input)
  end

  def test_lowest_common_multiple
    assert_equal 14, lowest_common_multiple([17,'x',13,19])
  end
  # def test_time_stamps17
  #   input = '17,x,13,19'
  #   assert_equal 3417, brute_force(input)
  #   # assert_equal 3417, time_stamps(input)
  # end

  # def test_time_stamps67
  #   input = '67,7,59,61'
  #   # assert_equal 754018, time_stamps(input)
  #   assert_equal 754018, brute_force(input)
  # end

  # def test_time_stamps_3
  #   input = '67,x,7,59,61'
  #   assert_equal 779210, time_stamps(input)
  # end

  # def test_time_stamps_4
  #   input = '67,7,x,59,61'
  #   assert_equal 1261476, time_stamps(input)
  # end

  # def test_time_stamps1789
  #   input = '1789,37,47,1889'
  #   assert_equal 1202161486, time_stamps(input)
  # end
end
