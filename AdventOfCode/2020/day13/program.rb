require_relative '../../base.rb'

input_array = read_lines('input.txt')
def catch_the_bus(array)
  bus_id = 0
  closest_time = -1
  departure_time = array[0].to_i 
  all_busses = array[1].split(',')
  all_busses.each do |bus|
    bus = bus.to_i
    next if bus.zero? # Neat, 'x'.to_i returns zero /shrug
    bus_depatures = departure_time / bus
    last_departure_time = bus_depatures * bus
    last_departure_time += bus if departure_time % bus > 0
    time_comparison = last_departure_time - departure_time
    if closest_time == -1 || time_comparison < closest_time
      closest_time = time_comparison
      bus_id = bus
    end
  end
  [bus_id * closest_time, nil]
end

def time_stamps(input_string)
  nums = input_string.split(',').map(&:to_i)
  nums.delete(0)
  # lcm = lowest_common_multiple(nums)
  # time = 1
  new_nums = []
  nums.each_with_index do |num, idx|
    new_nums.push(num+idx)
  end
  lcm = lowest_common_multiple(new_nums)
  lcm
end

def brute_force(array)
  idx = 0
  nums = array.split(',').map(&:to_i)
  while true
    idx += nums.first
    uniq_array = []
    nums.each_with_index do |num, idy|
      next if num.zero?
      uniq_array.push((idx + idy) % num == 0)
    end
    break if uniq_array.uniq == [true]
  end
  idx
end

def lowest_common_multiple(nums)
  lcm = 0
  while true
    lcm += 1
    lcm_array = []
    nums.each_with_index do |num, idx|
      next if num == 'x'
      # byebug
      lcm_array.push((lcm + idx) % num == 0)
    end
    break if lcm_array.uniq == [true]
  end
  lcm
end

# puts "Part 1: #{catch_the_bus(input_array)}"
# puts "Part 2: #{time_stamps(input_array[1])}"
# puts "Part 2: #{brute_force(input_array[1])}"
