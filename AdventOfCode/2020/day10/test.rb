require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      28,
      33,
      18,
      42,
      31,
      14,
      46,
      20,
      48,
      47,
      24,
      23,
      49,
      45,
      19,
      38,
      39,
      11,
      1,
      32,
      25,
      35,
      8,
      17,
      7,
      9,
      4,
      2,
      34,
      10,
      3
    ]
  end

  def test_code
    assert_equal 220, run_code(@raw_input)
  end

  def test_code_two
    input = [
      16,
      10,
      15,
      5,
      1,
      11,
      7,
      19,
      6,
      12,
      4
    ]
    assert_equal 35, run_code(input)
  end

  def test_code_part_2
    assert_equal 19208, run_code_two(@raw_input)
  end

  def test_code_two_part_2
    input = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]
    assert_equal 8, run_code_two(input)
  end

  def test_dynamic
    input = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]
    input.push(0, input.max+3)
    input.sort!
    assert_equal 8, dynamic_programming(input, 0, Hash[])
    
    # @raw_input.push(0, @raw_input.max+3)
    # @raw_input.sort!
    # assert_equal 19208, dynamic_programming(@raw_input, 0, Hash[])
  end
end
