require_relative '../../base.rb'

input_array = parse_intengers(read_lines('input.txt'))

def run_code(array)
  # Starts at 1 due to our original and final stipulations
  one_counts = 1
  three_counts = 1
  array.sort!
  (0..array.length - 2).each do |idx|
    (1..array.length - 1).each do |jdx|
      # puts jdx
      sum = array[jdx] - array[idx]
      if sum == 1
        one_counts += 1
        break
      elsif sum == 3
        three_counts += 1
        break
      end
    end
  end
  one_counts * three_counts
end

# figure out how many viable combinations we have
def run_code_two(array)
  combinations = []
  array.push(0) # Jolts can start between 1-3, and need to be accounted for
  array.push(array.max + 3) # Add the final +3 at the end for looping purposes
  array.sort!
  tmp_i = 0
  # OK! This ugly loop below is an attempt to break up our input into groups
  # based on the 'jump'. So when there's a jump of three we take the previous
  # numbers and set them into an array
  # turning  [0, 1, 2, 3, 4, 7, 8, 9, 10, 11] into
  # [0, 1, 2, 3, 4]
  # [7, 8, 9, 10, 11]
  (0..array.length - 2).each do |idx|
    if array[idx+1] - array[idx] == 3
      combinations.push(array[tmp_i..idx])
      tmp_i = idx + 1
    end
  end

  # Then down here we do math! From my by-hand math I found
  # that we have arrays of length 1-5, but only lengths 3+ 
  # concern us since that's where we can afford to drop numbers and still
  # succeed. I'm positive that this can be actually calculated using our
  # 3 jump input instead of hard coding values, but I can't quite wrap
  # my head around it right now
  # EDIT: Well how about that.... Tribonacci sequence is a thing.
  # 0, 0, 1, 1, 2, 4, 7
  # This solution only works due to my input not having any skips, ie [1,2,4,5]
  count = 1
  combinations.each_with_index do |comb, idx|
    count *= 7 if comb.length == 5
    count *= 4 if comb.length == 4
    count *= 2 if comb.length == 3
  end
  count
end

# First attempt at dynamic programming and mostly copied, but this is kick ass
def dynamic_programming(array, idx, answer_hash)
  # Naturally hop out if we're at the end of the array.
  return 1 if idx == array.length - 1
  # So this answer_hash holds the 'answer' for indexes as we find them
  # When we find ourselves looking for an answer moving forward
  # we can first check to see if we have the answer before we try to calculate it
  return answer_hash[idx] if answer_hash.keys.include?(idx)

  # Here begins our 'brute force' method
  count = 0
  # for each step in our array, starting from our given index
  (idx+1..array.length-1).each do |jdx|
    # Recurse if our condition is met.
    # So for this challenge we are looking for any situation in which
    # jdx - idx is less than or equal to three.
    # examine [0, 1, 4, 5, 6, 7, 10, 11, 12, 15]
    # from 12, there is only 1 way to get to 15, so our answer for 12 = 1
    # from 11, there is only 1 way to get to 12, which we know there's only one way to get to 15
    #          so the answer for 11 is 1
    # from 10 we can step to 11, or we can step to 12
    #         from above we know that 12 is 1, and 11 is 1, which means that 10 has two answers
    # from 7 we can step to 10, which is 2
    # from 6 we can step to 7, which is 2
    # from 5 we can step to 6, which is 2, or 7 which is 2, so our answer for 5 is 4
    # from 4 we can step to 5, 6, or 7, which have answers 2, 2, 4 respectively
    #        which means the answer for 4 is 8
    # 1 can only reach 4, 0 can only reach 1, which keeps our final answer at 8

    count += dynamic_programming(array, jdx, answer_hash) if array[jdx] - array[idx] <= 3
  end

  answer_hash[idx] = count
  count
end

puts "Part 1: #{run_code(input_array)}"
puts "Part 2: #{run_code_two(input_array)}"
puts "Part 2: #{dynamic_programming(input_array, 0, Hash[])}"
