require_relative '../../base.rb'

input_array = read_lines('input.txt')

def run_code(field_hash)
  # Part 1, given the 
  invalid_tickets = []
  valid_tickets = []
  ranges = []
  # Step one is to get all of our ranges
  field_hash.each do |key, value|
    next if ['your ticket', 'nearby tickets'].include?(key)
    ranges += value
  end
  valid_tickets = []
  # Now loop through and separate valid tickets from invalid tickets
  field_hash['nearby tickets'].each do |nearby_tickets|
    ticket_vals = nearby_tickets.split(',').map(&:to_i)
    invalid_ticket = validate_ticket(ranges, ticket_vals)
    invalid_tickets += invalid_ticket
    # Ticket is valid if we
    valid_tickets.push(ticket_vals) if invalid_ticket == []
  end
  # Part 1 just wants the sum of the invalid values
  # And part 2 uses all the valid tickets only
  [invalid_tickets.flatten.sum, valid_tickets]
end

# Goal of part two is to figure out which values map to which rules
def part_two(valid_tickets, field_hash)
  indices = Hash[]
  # First get our ticket values into a usable form
  my_ticket = parse_intengers(split_string(field_hash['your ticket'], ','))
  # Then remove these keys because we don't need them
  field_hash.delete('your ticket')
  field_hash.delete('nearby tickets')
  # Set an array of our available indices 0,1,2 etc
  available_indices = (0..field_hash.keys.length - 1).to_a
  # This loop will ultimately drop our available indices down to zero
  # So just keep looping until that is finished. Guaranteed there's a better way to do this...
  while available_indices.length > 0
    field_hash.each do |key, value|
      tmp_ticket_indices = available_indices
      # For each valid ticket loop through
      valid_tickets.each do |ticket_vals|
        # And get the index that is invalid, IE given range [[0,4], [7,10]]
        # ticket_vals 2,6,8 would return [1]
        # Then drop that index from our tmp indicies
        tmp_ticket_indices -= validate_ticket(value, ticket_vals, true)
      end
      # If/when the tmp indices have a single value left, that is our index for the rule
      # So mark it in our indices hash and drop it from availables
      # It's probably here that we could optimize our performance so that we don't have
      # To loop back over indices that we've already found
      if tmp_ticket_indices.length == 1
        indices[key] = tmp_ticket_indices.first
        available_indices -= tmp_ticket_indices
      end
    end
  end

  # Once that's done we want to multiply all of our 'departure' values together
  ans = 1
  indices.each do |k,v|
    ans *= my_ticket[v] if k.start_with?('departure')
  end

  ans
end

def validate_ticket(ranges, ticket_vals, use_index = false)
  invalid_tickets = []
  ticket_vals.each_with_index do |ticket, idx|
    is_valid = false
    # byebug
    ranges.each do |mm|
      is_valid = true if mm[0] <= ticket && ticket <= mm[1]
    end
    push_value = use_index ? idx : ticket
    invalid_tickets.push(push_value) unless is_valid
  end
  invalid_tickets
end

def parse_input(array)
  response = Hash.new()
  bb_true = false
  your_ticket = false
  nearby_tickets = false
  array.each do |x|
    next if x == ''
    
    if x == 'your ticket:'
      your_ticket = true
      next
    end
    if x == 'nearby tickets:'
      response['nearby tickets'] = []
      nearby_tickets = true
      next
    end
    if your_ticket
      response['your ticket'] = x
      your_ticket = false
      next
    end
    if nearby_tickets
      response['nearby tickets'].push(x)
      next
    end
    a,value_2 = x.split(': ')
    m,m2 = value_2.split(' or ')
    response[a] = [m.split('-').map(&:to_i), m2.split('-').map(&:to_i)]
  end

  response
end

field_hash = parse_input(input_array)
part_one, valid_tickets = run_code(field_hash)
puts "Part [1]: #{part_one}"
puts "Part [2]: #{part_two(valid_tickets, field_hash)}"
