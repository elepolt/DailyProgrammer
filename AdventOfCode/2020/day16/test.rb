require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'class: 1-3 or 5-7',
      'row: 6-11 or 33-44',
      'seat: 13-40 or 45-50',
      '',
      'your ticket:',
      '7,1,14',
      '',
      'nearby tickets:',
      '7,3,47',
      '40,4,50',
      '55,2,20',
      '38,6,12',
    ]
  end

  def test_parsing
    response = Hash[
      'class' => [[1, 3], [5, 7]],
      'row' => [[6, 11], [33, 44]],
      'seat' => [[13, 40], [45, 50]],
      'your ticket' => '7,1,14',
      'nearby tickets' => [
        '7,3,47',
        '40,4,50',
        '55,2,20',
        '38,6,12'
      ]
    ]
    assert_equal response, parse_input(@raw_input)
  end

  def test_validate_ticket
    assert_equal [4], validate_ticket([[1, 3], [5, 7], [6, 11], [33, 44], [13, 40], [45, 50]], [40,4,50])
  end

  def test_part_two
    field_hash = Hash[
      'class' => [[0, 1], [4, 19]],
      'row' => [[0, 5], [8, 19]],
      'seat' => [[0, 13], [16, 19]],
      'your ticket' => '1,2,3'
    ]
    valid_tickets = [
      [3,9,18],
      [15,1,5],
      [5,14,9],
    ]
    # The answer here is 1 because our rules use the word 'departure' and our example data doesn't have that
    assert_equal 1, part_two(valid_tickets, field_hash)
    assert_equal [0], validate_ticket([[0,1], [4,19]], [3,9,18], true)
    assert_equal [], validate_ticket([[0,1], [4,19]], [15,1,5], true)
    assert_equal [], validate_ticket([[0,1], [4,19]], [5,14,9], true)
    assert_equal [0], validate_ticket([[0,1], [4,19]], [3,9,18], true)
    assert_equal [0], validate_ticket([[0,13], [16,19]], [15,1,5], true)
    assert_equal [1], validate_ticket([[0,13], [16,19]], [5,14,9], true)
  end

  def test_code
    input = parse_input(@raw_input)
    assert_equal [71, [[7, 3, 47]]], run_code(input)
  end
end
