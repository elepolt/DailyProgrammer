require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'FBFBBFFRLR',
      'BFFFBBFRRR',
      'FFFBBBFRRR',
      'BBFFBBFRLL'
    ]
  end

  def test_highest
    # This 120 is a fake seat_id and is apt to change if this input changes
    assert_equal [820, 568], highest_pass(@raw_input)
  end

  def test_first
    assert_equal [44, 5, 357], find_seat(@raw_input[0])
  end

  def test_second
    assert_equal [70, 7, 567], find_seat(@raw_input[1])
  end

  def test_third
    assert_equal [14, 7, 119], find_seat(@raw_input[2])
  end

  def test_fourth
    assert_equal [102, 4, 820], find_seat(@raw_input[3])
  end

  def test_get_half
    assert_equal 64, get_half(127)
  end

  def test_get_half_two
    assert_equal 32, get_half(63)
  end

  def test_seat_binary
    assert_equal 357, find_seat_binary(@raw_input[0])
  end
end
