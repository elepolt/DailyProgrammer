require_relative '../../base.rb'

input_array = read_lines('input.txt')

def highest_pass(array)
  your_seat_id = 0
  seat_ids = []
  # Loop through all the seats and find the highest ID
  array.each do |seat_string|
    row, column, seat_id = find_seat(seat_string)
    seat_ids.push(seat_id)
  end
  # Sort them so that we can find missing IDs
  seat_ids.sort!
  seat_ids[0..seat_ids.length - 2].each_with_index do |seat_id, idx|
    # This currently only works because our input data is good.
    # We're just checking for missing seat_ids, ie [800, 802] should return 801
    your_seat_id = seat_id + 1 if seat_ids[idx + 1] - seat_ids[idx] > 1
  end
  highest = seat_ids.max
  [highest, your_seat_id]
end

# This piece takes the likes of FBFBBFFRLR and spits out [44, 5, 357]
# Seats are set 0-127. When there's an L or F you take the front half, meaning
# 0-63 or 0-3, R and B are the opposite.
def find_seat(seat_string)
  front_back_min = 0
  front_back_max = 127
  front_back_total = front_back_min + front_back_max
  left_right_min = 0
  left_right_max = 7
  left_right_total = left_right_min + left_right_max
  seat_string.split('').each do |letter|
    front_back_max = get_half(front_back_total) if letter == 'F'
    front_back_min = get_half(front_back_total) if letter == 'B'
    left_right_max = get_half(left_right_total) if letter == 'L'
    left_right_min = get_half(left_right_total) if letter == 'R'

    front_back_total = front_back_min + front_back_max
    left_right_total = left_right_min + left_right_max
  end

  # By the end of the string min/max should be equal, so just use that as the row/column
  [front_back_min, left_right_min, (front_back_min * 8) + left_right_min]
end

# Neat solution for this specific challenge as the seats work as a binary number
# So simply convert the letters to 0/1, and return the integer form
def find_seat_binary(seat_string)
  seat_string.gsub!('F', '0')
  seat_string.gsub!('B', '1')
  seat_string.gsub!('L', '0')
  seat_string.gsub!('R', '1')
  seat_string.to_i(2)
end

# We round up here because it's zero indexed, so 33.5 needs to become 34
def get_half(total_amount)
  (total_amount / 2.to_f).ceil
end

answer = highest_pass(input_array)
puts "The highest seat ID is #{answer[0]}"
puts "Your seat ID is #{answer[1]}"
