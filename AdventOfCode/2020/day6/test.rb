require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'abc',
      '',
      'a',
      'b',
      'c',
      '',
      'ab',
      'ac',
      '',
      'a',
      'a',
      'a',
      'a',
      '',
      'b',
    ]
  end

  def test_combine
    # This 120 is a fake seat_id and is apt to change if this input changes
    assert_equal [
      'abc',
      'a b c',
      'ab ac',
      'a a a a',
      'b'
    ], combine_answers(@raw_input)
  end

  def test_count
    assert_equal [11, 6], count_answers(combine_answers(@raw_input))
  end
end
