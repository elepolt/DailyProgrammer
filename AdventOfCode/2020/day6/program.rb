require_relative '../../base.rb'

input_array = read_lines('input.txt')

def count_answers(array)
  total_count = 0
  per_answer_count = 0
  array.each do |answer_string|
    # Gets everyones answer into a single string
    total_answers = answer_string.gsub(' ', '').split('')
    # Part 1 requires us to simply count how many unique 'yes' answers per string
    # So give `ab b c` we need to ultimately get to [a, b, c] to get our answer
    total_count += total_answers.uniq.length

    # For part 2 we need to see which questions everyone in the group answered yes to
    # So given the number of people,
    number_of_people = answer_string.split(' ').length
    # Loop through all the unique answers and check to see which answers
    # Show up the same number of times as the number of people
    total_answers.uniq.each do |possible_answer|
      per_answer_count += 1 if total_answers.count(possible_answer) == number_of_people
    end
  end
  [total_count, per_answer_count]
end

# Goal of this method is to get the broken lines of data into a single line
# We may want to look into refactoring this into base.rb and upping the verbosity
def combine_answers(array)
  # Issue regarding the last line on whether or not there's a trailing \n
  # So handle that by forcing a new line if one doesn't exist to round out the data
  array.push('') if array.last != ''

  # Then just loop through, stopping at each empty line and joining
  # the previous lines into a single line of passport data
  # This:
  # 'hcl:#cfa07d eyr:2025 pid:166559648',
  # 'iyr:2011 ecl:brn hgt:59in'
  # Becomes 'hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in'
  answer_arrays = []
  array.count('').times do |i|
    # Find the next empty line
    next_index = array.find_index('')
    # non-zeroed index so subtract 1 here
    answer_arrays.push(array[0..next_index-1].join(' '))
    array.shift(next_index+1)
  end
  answer_arrays
end

puts count_answers(combine_answers(input_array))