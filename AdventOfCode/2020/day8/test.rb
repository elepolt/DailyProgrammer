require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'nop +0',
      'acc +1',
      'jmp +4',
      'acc +3',
      'jmp -3',
      'acc -99',
      'acc +1',
      'jmp -4',
      'acc +6',
    ]
  end

  def test_code
    assert_equal [5, false], run_code(@raw_input)
  end

  def test_code_two
    assert_equal 8, part_two(@raw_input)
  end

  def test_part_one_finite
    raw_input = [
      'nop +0',
      'acc +1',
      'jmp +4',
      'acc +3',
      'jmp -3',
      'acc -99',
      'acc +1',
      'nop -4',
      'acc +6',
    ]
    assert_equal [8, true], run_code(raw_input)
  end
end
