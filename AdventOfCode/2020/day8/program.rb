require_relative '../../base.rb'

input_array = read_lines('input.txt')

def run_code(array)
  accumulator = 0
  continue_code = true
  idx = 0
  instruction_count = []
  while continue_code
    instruction, count = array[idx].split(' ')
    # intcode rules
    # if the instruction is nop, add one to the index move on
    # if it's acc,  increase accumulator by however many, and increase the idx by 1
    # if it's jmp, change the index however many
    index_change = ['nop', 'acc'].include?(instruction) ? 1 : count.to_i
    idx += index_change
    accumulator += count.to_i if instruction == 'acc'

    # The instructions should only be run once, so if we hit an index that we've already run
    # we've hit an infinite loop
    # It should also end if we've hit the end of the array
    continue_code = !(instruction_count.include?(idx) || (idx >= array.length))
    instruction_count.push(idx)
  end
  # Also return whether or not we've hit the end of the array to know if it's an infinite loop or not
  [accumulator, idx >= array.length]
end

# Let's brute force this bitch
def part_two(array)
  accumulator = 0
  # Find all the indices of jmp and nop
  jmp_nop_indices = array.each_index.select{|i| ['nop', 'jmp'].include?(array[i].split(' ')[0])}
  # Our goal here is to loop through and replace nop with jmp and vice versa one at a time
  # until we find our working solution
  jmp_nop_indices.each do |idx|
    # clone the array first so that we don't overwrite anything
    new_array = array.clone
    # Get the original instruction and count
    instruction, count = new_array[idx].split(' ')
    # update the instruction locally
    instruction = instruction == 'jmp' ? 'nop' : 'jmp'
    new_array[idx] = [instruction, count].join(' ')
    # Check the output and set/break if the new array has run all the way through
    tmp_accumulator, is_finite = run_code(new_array)
    accumulator = tmp_accumulator if is_finite
    break if is_finite
  end
  accumulator
end

puts "Part 1: #{run_code(input_array)}"
puts "Part 2: #{part_two(input_array)}"