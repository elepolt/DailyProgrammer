require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '..##.......',
      '#...#...#..',
      '.#....#..#.',
      '..#.#...#.#',
      '.#...##..#.',
      '..#.##.....',
      '.#.#.#....#',
      '.#........#',
      '#.##...#...',
      '#...##....#',
      '.#..#...#.#',
    ]
  end

  def test_first
    assert_equal 2, count_trees(@raw_input, 1, 1)
  end

  def test_sec
    assert_equal 7, count_trees(@raw_input, 3, 1)
  end

  def test_third
    assert_equal 3, count_trees(@raw_input, 5, 1)
  end

  def test_fourth
    assert_equal 4, count_trees(@raw_input, 7, 1)
  end

  def test_fifth
    assert_equal 2, count_trees(@raw_input, 1, 2)
  end

  def test_my_input
    input = [
      '...............#.#.............',
      '##..#....................#...##',
      '......#..#.#.....#..#.#.##.....',
      '.........#...#..............#.#',
      '............#.......##.........',
      '...#.....#.....#...#.....#..#..',
      '..............#..##.#..#......#',
      '.##.....#.....#......##.#......',
      '.#..........###....#...##....#.',
      '.....#....#.#.......#......##..',
      '.#....#......#.......#........#',
    ]
    assert_equal 9, count_trees(input, 3, 1)
  end

  def test_x_pos
    input = [
      '..##.',
      '#...#',
      '.#...',
      '..#.#',
      '.#...',
      '..#.#',
    ]
    assert_equal 2, count_trees(input, 3, 1)
    assert_equal [1, 1], get_next_position(input, 0, 3, 3, 1)
  end

  def test_slopes
    slopes = [
      [1, 1],
      [3, 1],
      [5, 1],
      [7, 1],
      [1, 2],
    ]
    assert_equal 336, compare_slopes(@raw_input, slopes)
  end
end
