require_relative '../../base.rb'

input_array = read_lines('input.txt')

# Take the entire array, and the slope, IE 3,1 which means we'll check 3 over, and 1 down
def count_trees(array, horizontal_slope, vertical_slope)
  # First thing we need to do is get the height of our mountain
  vertical_count = array.length
  horizontal_count = array.first.length
  count = 0

  # I have an assumption that the second part is going to be something about 
  # moving to not hit trees, so I'm going to preemptively use a while here
  # to traverse our mountain
  current_y = 0
  current_x = 0
  while current_y < vertical_count - 1
    current_y, current_x = get_next_position(array, current_y, current_x, horizontal_slope, vertical_slope)
    next_position = array[current_y][current_x]
    count += 1 if next_position == '#'
  end
  count
end

def get_next_position(array, current_y, current_x, horizontal_slope, vertical_slope)
  horizontal_count = array.first.length
  # Utilize modulus to handle going beyond our horizontal limit
  current_x = (current_x + horizontal_slope) % horizontal_count
  current_y += vertical_slope
  [current_y, current_x]
end

def compare_slopes(array, slopes)
  total_count = 1
  slopes.each do |x,y|
    total_count *= count_trees(array, x, y)
  end
  total_count
end

puts "Part 1: #{count_trees(input_array, 3, 1)}"
slopes = [
  [1, 1],
  [3, 1],
  [5, 1],
  [7, 1],
  [1, 2],
]
puts "Part 2: #{compare_slopes(input_array, slopes)}"