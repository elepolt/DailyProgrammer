UP = [-1, 0].freeze
DOWN = [1, 0].freeze
LEFT = [0, -1].freeze
RIGHT = [0, 1].freeze
DIRECTIONS = [UP, DOWN, LEFT, RIGHT].freeze
UPRIGHT = [-1, 1].freeze
DOWNLEFT = [1, -1].freeze
UPLEFT = [-1, -1].freeze
DOWNRIGHT = [1, 1].freeze

class Board
  def initialize(str_rows, convert_to_ints)
    @convert_to_ints = convert_to_ints
    @board = build_board(str_rows)
    @height = @board.length - 1
    @width = @board[0].length - 1
    # @cells = build_cells
  end
  attr_accessor :height, :width

  def build_board(array)
    board = []
    array.each do |row|
      new_row = row.split('')
      new_row = new_row.map(&:to_i) if @convert_to_ints
      board.append(new_row)
    end
    board
  end

  def cells
    @cells ||= begin
      tmp_cells = {}
      @board.each_with_index do |row, idx|
        row.each_with_index do |cell, idy|
          tmp_cells[[idx, idy]] = cell
        end
      end
      tmp_cells
    end
  end

  def print
    cells.map{|c| c[0][0]}.uniq.sort.each do |idx|
      pp cells.select{|c| c[0] == idx}.map{|c| c[1]}
    end
  end

  def neighbors(starting_coords, distance = 0, include_diagonals = true)
    tmp_neighbors = []
    [-1, 0, 1].each do |idx|
      [-1, 0, 1].each do |idy|
        next if idx.zero? && idy.zero? # skip if it's the starting_coords
        next if (idx == idy || idx == -idy) && !include_diagonals # Skips diagonals

        x = starting_coords[0] + idx
        y = starting_coords[1] + idy
        # Skip if we're at an edge
        next if x.negative? || x >= @board.length
        next if y.negative? || y >= @board[0].length

        tmp_neighbors.push([x, y])
      end
    end
    tmp_neighbors
  end

  def out_of_range(coord)
    coord[0].negative? || coord[1].negative? || coord[0] >= @board[0].length || coord[1] >= @board[0].length
  end

  def find_cell(symbol)
    cells.each do |coord, cell|
      return coord if cell == symbol
    end
  end
end

def up
  [-1, 0]
end

def left
  [0, -1]
end

def upleft
  [-1, -1]
end

def right
  [0, 1]
end

def upright
  [-1, 1]
end

def down
  [1, 0]
end

def downleft
  [1, -1]
end

def downright
  [1, 1]
end
# Adds b to a and returns the coordinate
def coord_addition(a, b)
  dx = a[0] + b[0]
  dy = a[1] + b[1]
  [dx, dy]
end

# Subtracks b from a and returns the difference
def coord_difference(a, b)
  dx = b[0] - a[0]
  dy = b[1] - a[1]
  [dx, dy]
end

# def traverse(grid, starting_coords, distance = 0, include_diagonals = true)
#   neighbors = []
#   # This neat nested loop traverses the surrounding box of a given index
#   [-1, 0, 1].each do |idx|
#     [-1, 0, 1].each do |idy|
#       next if idx.zero? && idy.zero? # skip if it's the starting_coords
#       next if (idx == idy || idx == -idy) && !include_diagonals # Skips diagonals

#       x = starting_coords[0] + idx
#       y = starting_coords[1] + idy
#       # Skip if we're at an edge
#       next if x.negative? || x >= grid.length
#       next if y.negative? || y >= grid[0].length

#       neighbors.push([x, y])
#       # This is where Game of Life ultimately ends,

#       # If we have a distance given to us we just continue going in whichever direction we were going
#       distance.times do
#         # Continue adding our idx/y value from above, which will move
#         # us neatly in all directions
#         x += idx
#         y += idy
#         # Break when we get to an edge
#         break if x.negative? || x >= grid.length
#         break if y.negative? || y >= grid[0].length

#         neighbors.push([x, y])
#       end
#     end
#   end
#   neighbors
# end


# raw_input = [
#   '...',
#   '.-.',
#   '...'
# ]
# board = Board.new(raw_input) 
# puts board.neighbors([1, 1]).map{|n| board.cells[n]}
# puts board.cells[[1, 1]]
