require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  gamma_array = []
  epsilon_array = []
  oxygen_generator = scrubber_rating = 0

  (0..array[0].length - 1).each do |index|
    most_common, least_common = get_common_value_at_index(array, index)
    gamma_array.push(most_common)
    epsilon_array.push(least_common)
    next unless index.zero?

    # The first time we run through is when we want to utilize our recursion
    # This is because the following indices are dependent on the common/least common
    # bits from the new arrays
    oxygen_generator = convert_array_to_decimal(break_down_array(array, 0, most_common, true))
    scrubber_rating = convert_array_to_decimal(break_down_array(array, 0, least_common, false))
  end

  # At this point we have everything we need, so get the decimal form and send it back
  gamma = convert_array_to_decimal(gamma_array)
  epsilon = convert_array_to_decimal(epsilon_array)
  life_support_rating = oxygen_generator * scrubber_rating

  [gamma * epsilon, life_support_rating]
end

# Takes our arryay and index and returns the most common and least common bit
def get_common_value_at_index(array, index)
  zero_count = one_count = 0
  array.each do |report_number|
    zero_count += 1 if report_number[index] == '0'
    one_count += 1 if report_number[index] == '1'
  end
  zero_count > one_count ? %w[0 1] : %w[1 0]
end

# Recursionnnn!
def break_down_array(array, index, common_value, use_common)
  # Send it back once we've gotten our number
  return array if array.length == 1

  # Given our common value get all of the numbers that have that value at that index
  next_array = select_values_by_index(array, index, common_value)
  index += 1
  # Get the new common values
  most_common, least_common = get_common_value_at_index(next_array, index)
  # Check to see if we should be using the most or least common
  next_common_value = use_common ? most_common : least_common
  # And break it down again
  break_down_array(next_array, index, next_common_value, use_common)
end

def convert_array_to_decimal(array)
  array.join('').to_i(2)
end

# This should take an array of binary strings, an index, and a 0/1 value
# and return only the strings that have that value at that index
def select_values_by_index(array, index, value)
  array.select { |v| v[index] == value }
end

# puts "Parts 1 & 2: #{program(input_array)}"
