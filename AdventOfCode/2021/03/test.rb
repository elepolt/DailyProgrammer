require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '00100',
      '11110',
      '10110',
      '10111',
      '10101',
      '01111',
      '00111',
      '11100',
      '10000',
      '11001',
      '00010',
      '01010'
    ]
  end

  def test_code
    assert_equal [198, 230], program(@raw_input)
    assert_equal %w[1 0], get_common_value_at_index(@raw_input, 0)
    assert_equal ['11110', '10110', '10111', '10101', '11100', '10000', '11001'], select_values_by_index(@raw_input, 0, '1')
    assert_equal ['10111'], break_down_array(@raw_input, 0, '1', true)
  end
end
