require_relative '../../base'
require 'pqueue'

def program(grid, ending_coords = nil, part2 = false)
  if part2
    grid = build_grid2(grid)
  end

  ending_coords ||= [grid.length - 1, grid.last.length - 1]

  # Ok. Dijkstra. Here we go.
  starting_node = [0,0]
  unvisited_nodes = PQueue.new
  unvisited_nodes.push([0, starting_node])

  dijkstra_values = {}
  dijkstra_values[starting_node] = {
    'value': 0,
    'previous_vertex': [0,0]
  }

  until unvisited_nodes.empty?
    risk, current_node = unvisited_nodes.shift

    current_dijkstra_value = dijkstra_values[current_node][:value]

    neighbors = traverse(grid, current_node, 0, false)
    neighbors.each do |coord|

      # If this is new, initialize it and push it into our unvisited queue
      unless dijkstra_values.key?(coord)
        dijkstra_values[coord] = {
          'value': 99999
        }
      end

      # Then compare the current values to it
      grid_value = grid[coord[0]][coord[1]]
      value = grid_value + current_dijkstra_value

      if value < dijkstra_values[coord][:value]
        dijkstra_values[coord][:value] = value
        dijkstra_values[coord][:previous_vertex] = current_node
        unvisited_nodes.push([value, coord])
      end
    end
  end

  # If you want to see the path uncomment
  # coord = ending_coords
  # while coord != [0,0]
  #   pp coord
  #   coord = dijkstra_values[coord][:previous_vertex]
  # end

  [dijkstra_values[ending_coords][:value], nil]
end

grid = []
input_array = read_lines('input.txt')
input_array.each do |input_string|
  array_of_ints = split_string(input_string, '')
  grid.push(parse_intengers(array_of_ints))
end

# I'm so sorry. I cannot logic out the smart way to do this graph for part 2
def build_grid2(grid)
  next_grid = grid.dup
  (0..grid.length-1).each do |row|
    original_row = grid[row].dup
    (1..4).each do |i|
      next_row = original_row.map do |r|
        num = (r + i)
        num -= 9 if num > 9
        num
      end
      next_grid[row].push(next_row).flatten!
    end
  end

  # Now do it 4 more times to add each line to our 
  (1..4).each do |i|
    (0..grid.length-1).each do |row|
      original_row = grid[row].dup
      next_row = original_row.map do |r|
        num = (r + i)
        num -= 9 if num > 9
        num
      end
      next_grid.push(next_row)
    end
  end

  next_grid
end

# puts(program(grid))
# puts(program(grid, [49, 58]))
# puts(program(grid, nil, true))
