require 'benchmark'

map = {}
(0...10000).each do |turns|
  map[turns] = {
    'value': rand(550),
    'previous_vertex': [rand(100), rand(100)],
    'visited': [true, false].sample
  }
end

# puts Benchmark.measure {
#   5000.times do
#     coord, value = map.reject {|key, dv| dv[:visited] }.min_by {|k,v| v[:value] }
#   end
# }

puts Benchmark.measure {
  5000.times do
    min_distance = 99999
    coord = nil
    map.each do |key, value|
      next if value[:visited]
      next unless value[:value] < min_distance

      coord = key
      min_distance = value[:value]
    end
  end
}