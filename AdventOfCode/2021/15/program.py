#!/usr/bin/python3
import sys
import heapq
import itertools
from collections import defaultdict, Counter, deque
import pdb

sys.setrecursionlimit(int(1e6))

infile = sys.argv[1] if len(sys.argv)>1 else 'input.txt'

GRID = []
for line in open(infile):
    GRID.append([int(x) for x in line.strip()])
ROWS = len(GRID)
COLS = len(GRID[0])
DR = [-1,0,1,0]
DC = [0,1,0,-1]
qd = {}

def solve(n_tiles):
    DIJKSTRA = [[None for _ in range(n_tiles*COLS)] for _ in range(n_tiles*ROWS)]
    Q = [(0,0,0)]
    count = 0
    max_q = 0
    while Q:
        count = count + 1
        (dist,row,col) = heapq.heappop(Q)
        if row<0 or row>=n_tiles*ROWS or col<0 or col>=n_tiles*COLS:
            continue

        val = GRID[row%ROWS][col%COLS] + (row//ROWS) + (col//COLS)
        while val > 9:
            val -= 9
        rc_cost = dist + val

        if DIJKSTRA[row][col] is None or rc_cost < DIJKSTRA[row][col]:
            DIJKSTRA[row][col] = rc_cost
        else:
            continue
        if row==n_tiles*ROWS-1 and col==n_tiles*COLS-1:
            break

        for d in range(4):
            rr = row+DR[d]
            cc = col+DC[d]
            heapq.heappush(Q, (DIJKSTRA[row][col],rr,cc))
    return DIJKSTRA[n_tiles*ROWS-1][n_tiles*COLS-1] - GRID[0][0]

print(solve(1))
print(solve(5))
