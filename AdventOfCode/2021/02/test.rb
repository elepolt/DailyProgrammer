require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'forward 5',
      'down 5',
      'forward 8',
      'up 3',
      'down 8',
      'forward 2'
    ]
  end

  def test_code
    assert_equal [150, 900], run_code(@raw_input)
    # assert_equal 5, run_windows(@raw_input, 3)
  end
end
