require_relative '../../base'

input_array = read_lines('input.txt')

def run_code(array)
  horizontal_index = vertical_index = depth = 0
  array.each do |command|
    direction, units = command.split(' ')
    units = units.to_i
    if direction == 'forward'
      horizontal_index += units
      depth += units * vertical_index
    end
    vertical_index -= units if direction == 'up'
    vertical_index += units if direction == 'down'
  end
  [horizontal_index * vertical_index, horizontal_index * depth]
end

puts "Part 1: #{run_code(input_array)}"
# puts "Part 2: #{run_windows(input_array, 3)}"
