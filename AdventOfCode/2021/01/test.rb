require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      199, # (N/A - no previous measurement)
      200, # (increased)
      208, # (increased)
      210, # (increased)
      200, # (decreased)
      207, # (increased)
      240, # (increased)
      269, # (increased)
      260, # (decreased)
      263, # (increased)
    ]
  end

  def test_code
    assert_equal 7, run_code(@raw_input)
    assert_equal 5, run_windows(@raw_input, 3)
  end
end
