require_relative '../../base'

input_array = parse_intengers(read_lines('input.txt'))

def run_code(array)
  larger_count = 0
  # Set our initial measurement to the first item in the list
  previous_measurement = array[0]
  # Loop through the list, starting at the second value
  array[1..].each do |measurement|
    larger_count += 1 if measurement > previous_measurement
    previous_measurement = measurement
  end
  larger_count
end

def run_windows(array, window_length)
  larger_count = -1 # start at -1 due to the first measurement always being higher
  previous_measurement = 0
  (0..(array.length - window_length)).each do |idx|
    measurement = array.slice(idx, window_length).sum
    larger_count += 1 if measurement > previous_measurement
    previous_measurement = measurement
  end
  larger_count
end

puts "Part 1: #{run_code(input_array)}"
puts "Part 2: #{run_windows(input_array, 3)}"
