require_relative '../../base'

input_array = parse_intengers(split_string(read_lines('input.txt')[0], ','))

def program(array)
  # Set up all our dictionaries
  fuel_dictionary = array.tally
  # And default new values to 0
  results_dictionary_p1 = Hash.new(0)
  results_dictionary_p2 = Hash.new(0)

  min, max = array.minmax
  (min..max).each do |base_position|
    fuel_dictionary.each do |position, count|
      # Skip maths if the crabs won't need to move
      next if position == base_position

      steps = (position - base_position).abs
      results_dictionary_p1[base_position] += count * steps
      results_dictionary_p2[base_position] += count * triangular_number(steps)
    end
  end

  min_value = results_dictionary_p1.values.min
  min_value_p2 = results_dictionary_p2.values.min

  [min_value, min_value_p2]
end

def triangular_number(num)
  (num * (num + 1)) / 2
end

puts "Part 1/2: #{program(input_array)}"
