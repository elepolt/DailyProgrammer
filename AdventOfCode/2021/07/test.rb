require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [16,1,2,0,4,2,7,1,2,14]
  end

  def test_code
    assert_equal [37, 168], program(@raw_input)
  end
end
