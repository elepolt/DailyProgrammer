require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '[({(<(())[]>[[{[]{<()<>>',
      '[(()[<>])]({[<{<<[]>>(',
      '{([(<{}[<>[]}>{[]{[(<()>',
      '(((({<>}<{<{<>}{[]{[]{}',
      '[[<[([]))<([[{}[[()]]]',
      '[{[{({}]{}}([{[{{{}}([]',
      '{<[[]]>}<{[{[{[]{()[[[]',
      '[<(<(<(<{}))><([]([]()',
      '<{([([[(<>()){}]>(<<{{',
      '<{([{{}}[<[[[<>{}]]]>[]]'
    ]
  end

  def test_code
    assert_equal [26397, 288957], program(@raw_input)
    assert_equal '}}]])})]', autocomplete(["[", "(", "{", "(", "[", "[", "{", "{"]).join
    assert_equal 294, compute_score('])}>'.chars)
  end
end
