require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  open_dict = {
    '(' => ')',
    '[' => ']',
    '{' => '}',
    '<' => '>',
  }
  close_dict = {
    ')' => '(',
    ']' => '[',
    '}' => '{',
    '>' => '<',
  }

  points_dict = {
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25137,
  }
  total_points = Hash.new(0)
  autocomplete_points = []
  # {([(<{}[<>[]}>{[]{[(<()> - Expected ], but found } instead.
  # [[<[([]))<([[{}[[()]]] - Expected ], but found ) instead.
  # [{[{({}]{}}([{[{{{}}([] - Expected ), but found ] instead.
  # [<(<(<(<{}))><([]([]() - Expected >, but found ) instead.
  # <{([([[(<>()){}]>(<<{{ - Expected ], but found > instead.

  array.each_with_index do |line_r|
    ongoing = []
    incomplete = true
    line_r.chars.each do |char|
      # if it's open just add it on, we can add as many opens as we want
      if open_dict.keys.include?(char)
        ongoing.push(char)
      else
        # else it's a closing character,
        # that means... 
        # we need to work backwards to make sure we are kosher on our openings
        last = ongoing.pop
        next if close_dict[char] == last

        total_points[char] += points_dict[char]
        incomplete = false
        break
      end
    end
    closings = autocomplete(ongoing) if incomplete
    autocomplete_points.push(compute_score(closings)) if incomplete
  end


  # ): 3 points.
  # ]: 57 points.
  # }: 1197 points.
  # >: 25137 points.
  [total_points.values.sum, autocomplete_points.sort[autocomplete_points.length / 2]]
end

def autocomplete(line)
  # ["[", "(", "{", "(", "[", "[", "{", "{"]
  open_dict = {
    '(' => ')',
    '[' => ']',
    '{' => '}',
    '<' => '>',
  }
  closings = []
  while line.length > 0
    closings.push(open_dict[line.pop])
  end
  closings
end

def compute_score(closings)
  points_dict = {
    ')' => 1,
    ']' => 2,
    '}' => 3,
    '>' => 4,
  }
  # ])}>
  score = 0
  closings.each do |closing|
    score *= 5
    score += points_dict[closing]
  end

  score
end

def median(array)
  return nil if array.empty?
  sorted = array.sort
  len = sorted.length
  (sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0
end

puts "Part 1/2: #{program(input_array)}"
