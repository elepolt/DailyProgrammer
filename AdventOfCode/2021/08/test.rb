require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe',
      'edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc',
      'fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg',
      'fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb',
      'aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea',
      'fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb',
      'dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe',
      'bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef',
      'egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb',
      'gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce',
    ]
  end

  def test_code
    assert_equal [26, 61_229], program(@raw_input)
    input_array = %w[acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab]
    test_dict = {
      'ab' => 1,
      'gcdfa' => 2,
      'fbcad' => 3,
      'eafb' => 4,
      'cdfbe' => 5,
      'cdfgeb' => 6,
      'dab' => 7,
      'acedgfb' => 8,
      'cefabd' => 9,
      'cagedb' => 0,
    }
    decoder_output = display_decoder(input_array)
    assert_equal test_dict['ab'], decoder_output['ab'] # 1
    assert_equal test_dict['eafb'], decoder_output['eafb'.chars.sort.join] # 4
    assert_equal test_dict['dab'], decoder_output['dab'.chars.sort.join] # 7
    assert_equal test_dict['acedgfb'], decoder_output['acedgfb'.chars.sort.join] # 8
    assert_equal test_dict['cefabd'], decoder_output['cefabd'.chars.sort.join] # 9
    assert_equal test_dict['cagedb'], decoder_output['cagedb'.chars.sort.join] # 0
    assert_equal test_dict['cdfgeb'], decoder_output['cdfgeb'.chars.sort.join] # 6
    assert_equal test_dict['cdfbe'], decoder_output['cdfbe'.chars.sort.join] # 5
    assert_equal test_dict['fbcad'], decoder_output['fbcad'.chars.sort.join] # 3
    assert_equal test_dict['gcdfa'], decoder_output['gcdfa'.chars.sort.join] # 2
  end

  def test_control
    input_array = %w[abcefg cf acdeg acdfg bcdf abdfg abdefg acf abcdefg abcdfg]
    test_dict = {
      'cf' => 1,
      'acdeg' => 2,
      'acdfg' => 3,
      'bcdf' => 4,
      'abdfg' => 5,
      'abdefg' => 6,
      'acf' => 7,
      'abcdefg' => 8,
      'abcdfg' => 9,
      'abcefg' => 0,
    }
    decoder_output = display_decoder(input_array)
    assert_equal test_dict['cf'], decoder_output['cf'] # 1
    assert_equal test_dict['bcdf'], decoder_output['bcdf'] # 4
    assert_equal test_dict['acf'], decoder_output['acf'] # 7
    assert_equal test_dict['abcdefg'], decoder_output['abcdefg'] # 8
    assert_equal test_dict['abcdfg'], decoder_output['abcdfg'] # 9
    assert_equal test_dict['abcefg'], decoder_output['abcefg'] # 0
    assert_equal test_dict['abdefg'], decoder_output['abdefg'] # 6
    assert_equal test_dict['abdfg'], decoder_output['abdfg'] # 5
    assert_equal test_dict['acdfg'], decoder_output['acdfg'] # 3
    assert_equal test_dict['acdeg'], decoder_output['acdeg'] # 2
  end
end
