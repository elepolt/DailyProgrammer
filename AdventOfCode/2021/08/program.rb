require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)

  part_one_segments = 0
  part_two_segments = 0

  array.each do |values|
    signal_string, output_string = split_string(values, '|')
    signals = split_string(signal_string, ' ')
    outputs = split_string(output_string, ' ')

    display_dictionary = {
      'a' => nil,
      'b' => nil,
      'c' => nil,
      'd' => nil,
      'e' => nil,
      'f' => nil,
      'g' => nil
    }

    output_value = []
    segment_decoder = display_decoder(signals)
    outputs.each do |output|
      part_one_segments += 1 if [2, 4, 3, 7].include?(output.length)
      output_value.push(segment_decoder[output.chars.sort.join])
    end

    part_two_segments += output_value.join.to_i
  end

  [part_one_segments, part_two_segments]
end

require 'set'

def display_decoder(segments)
  segment_map = {}
  segments.map! { |s| s.chars.sort.join }
  # I need to turn {"be"=>2, "cfbegad"=>7, "cgeb"=>4, "edb"=>3}
  known_numbers = {}
  segments.delete_if do |segment|
    known_numbers[1] = segment.chars.sort if segment.length == 2
    known_numbers[4] = segment.chars.sort if segment.length == 4
    known_numbers[7] = segment.chars.sort if segment.length == 3
    known_numbers[8] = segment.chars.sort if segment.length == 7
    [2, 3, 4, 7].include?(segment.length)
  end
  segment_map['a'] = known_numbers[7] - known_numbers[1]

  # Let's find 9.
  segments.each do |segment|
    next if segment.length == 5

    remaining_number = segment.chars - known_numbers[4] - segment_map['a']
    next unless remaining_number.length == 1

    known_numbers[9] = segment.chars.sort
    segment_map['g'] = remaining_number
  end
  segments.delete(known_numbers[9].join)

  # Right now!
  # We know 1, 4, 7, 8, 9
  # We also know a and g

  # I believe that we can find 6 and 0 at this point,
  # They will have similar missing strings, so we need to check that remaining number to see which it is
  segments.each do |segment|
    next if segment.length == 5

    remaining_number = known_numbers[8] - segment.chars
    next unless remaining_number.length == 1

    # 0 will give us 'd', 6 will give us 'c'
    # for 6 we want to make sure that the remaining number is not in our [1]
    if known_numbers[1].include?(remaining_number[0])
      known_numbers[6] = segment.chars.sort
      segment_map['c'] = remaining_number
    elsif known_numbers[4].include?(remaining_number[0])
      # so for 0 we want to make sure that the remaining number is not in our [1]
      known_numbers[0] = segment.chars.sort
      segment_map['d'] = remaining_number
    end
  end
  segments.delete(known_numbers[6].join)
  segments.delete(known_numbers[0].join)

  # Ok! We know 1, 4, 6, 7, 8, 9, 0
  # We also know a, c, d, g

  # We can get 'b' 'e' with some of the things we know now
  # a, b, c, d, e, g
  segment_map['b'] = known_numbers[4] - known_numbers[1] - segment_map['d']
  segment_map['e'] = known_numbers[8] - known_numbers[9]

  # Let's find five know that it's just 6 - e
  segments.each do |segment|
    remaining_number = known_numbers[6] - segment.chars

    next unless remaining_number == segment_map['e']

    known_numbers[5] = segment.chars.sort
  end
  segments.delete(known_numbers[5].join)

  # 1, 4, 5, 6, 7, 8, 9, 0
  known_numbers[3] = known_numbers[9] - segment_map['b']
  segments.delete(known_numbers[3].join)

  # And now we know the last remaining segment is 2
  known_numbers[2] = segments
  known_numbers.transform_values { |v| v.sort.join }.invert
end

# puts "Part 1/2: #{program(input_array)}"

def smarter(array)
  # Build our dictionary of known values, where a-g are 'normal'
  control_dict = {
    'cf' => 1,
    'acdeg' => 2,
    'acdfg' => 3,
    'bcdf' => 4,
    'abdfg' => 5,
    'abdefg' => 6,
    'acf' => 7,
    'abcdefg' => 8,
    'abcdfg' => 9,
    'abcefg' => 0
  }

  # Choose three unique numbers, like from part 1
  one = 'cf'.chars
  four = 'bcdf'.chars
  eight = 'abcdefg'.chars

  number_dict = {}
  control_dict.each do |k, v|
    # Go through each number in our control and multiply it by how many
    # segments it intersects with of our three unique numbers
    common = (k.chars & one).length
    common *= (k.chars & four).length
    common *= (k.chars & eight).length
    # ie: 2 intersects with 1 once, 4, twice, and 8 five times, giving it a product of 10
    number_dict[common] = v
  end

  answer = 0
  array.each do |values|
    signals, outputs = split_string(values, '|').map { |string| split_string(string, ' ') }

    # Now build the new product dict with this round of signals, it builds out as a dictionary of strings to ints
    # {"abd"=>12, "abcef"=>10, ... }
    product_dict = build_product(signals)

    value = []
    outputs.each do |output|
      # Now for each output we can map it to the product dict,
      # IE: "abcef" = 10
      product = product_dict[output.chars.sort.join]
      # And then we know from num_dict that 10 = 2
      value.push(number_dict[product])
    end
    # we end with an array [0, 0, 8, 6], in which we join and convert to give us 86
    answer += value.join.to_i
  end

  answer
end

def build_product(array)
  one = array.select { |a| a.length == 2 }[0].chars
  four = array.select { |a| a.length == 4 }[0].chars
  eight = array.select { |a| a.length == 7 }[0].chars

  number_dict = {}
  array.each do |k|
    common = (k.chars & one).length
    common *= (k.chars & four).length
    common *= (k.chars & eight).length
    number_dict[k.chars.sort.join] = common
  end

  number_dict
end

puts "smarter! #{smarter(input_array)}"
