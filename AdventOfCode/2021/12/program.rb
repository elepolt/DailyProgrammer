require_relative '../../base'
require 'set'

input_array = read_lines('input.txt')
# parse_intengers(array_of_ints)

def get_neighbors(values)
  neighbors = {}
  values.each do |value|
    a, b = split_string(value, '-')
    neighbors[a] = [] unless neighbors.keys.include?(a)
    neighbors[b] = [] unless neighbors.keys.include?(b)
    neighbors[a].push(b)
    neighbors[b].push(a)
  end

  neighbors
end

# def traverse(neighbors, current, seen, paths)
#   neighbors[current].each do |neighbor|
#     seen[current] = [] unless seen.keys.include?(current)

#     if neighbor == 'end'
#       paths += 1
#       next
#     end

#     next if neighbor == 'start'
#     next if seen[current].include?(neighbor)

#     if is_lower?(neighbor)
#       seen[current].push(neighbor)
#       next if seen.keys.include?(neighbor)
#     end

#     paths += traverse(neighbors, neighbor, seen, paths)
#   end

#   paths
# end

def program(neighbors)    
  #    start
  #    /   \
  # c--A----b--d
  #    \   /
  #     end

  # seen = Set.new({})
  seen = {}
  paths = traverse(neighbors, 'start', seen, 0)
  # starts = get_points_from_start(array, 'start')
  # ends = get_points_from_end(array, 'end')
  # middle = array - starts - ends 
  # starts.each do |connection|
  #   starting_point, ending_point = connection.split('-')
  # end
  [paths, nil]
end

def is_lower?(string)
  string != string.upcase
end

# neighbors = get_neighbors(input_array)
# puts "Part 1/2: #{program(neighbors)}"
