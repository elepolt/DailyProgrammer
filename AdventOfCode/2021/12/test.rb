require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'start-A',
      'start-b',
      'A-c',
      'A-b',
      'b-d',
      'A-end',
      'b-end',
    ]
  end

  def test_code
    neighbors = get_neighbors(@raw_input)
    assert_equal [10, nil], program(neighbors)
  end
end
