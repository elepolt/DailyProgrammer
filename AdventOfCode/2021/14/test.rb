require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'CH -> B',
      'HH -> N',
      'CB -> H',
      'NH -> C',
      'HB -> C',
      'HC -> B',
      'HN -> C',
      'NN -> C',
      'BH -> H',
      'NC -> B',
      'NB -> B',
      'BN -> B',
      'BB -> N',
      'BC -> B',
      'CC -> N',
      'CN -> C',
    ]
  end

  def test_code
    assert_equal [1588, nil], program(@raw_input, 'NNCB', 10)
    # assert_equal [2_188_189_693_529, nil], program(@raw_input, 'NNCB', 40)
  end
end
