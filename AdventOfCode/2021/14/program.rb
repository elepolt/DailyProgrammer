require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, template, iterations)
  rules = array.map { |a| a.split(' -> ') }.to_h
  counts = Hash.new(0)
  (0..template.length - 2).each do |x|
    pair = template[x], template[x + 1]
    counts[pair.join('')] = 1
  end

  iterations.times do
    next_counts = Hash.new(0)
    counts.each do |key, value|
      pair = key.split('')
      rule_char = rules[key]

      next_counts[[pair[0], rule_char].join] += value
      next_counts[[rule_char, pair[1]].join] += value
    end
    counts = next_counts
  end

  letter_counts = Hash.new(0)
  counts.each do |key, value|
    pair = key.split('')
    letter_counts[pair[0]] += value
  end
  # The above works great, but never gets the last letter, since it's not technically a 'pairing', and never changes
  letter_counts[template.split('').last] += 1

  min, max = letter_counts.values.minmax
  [max - min, nil]
end
puts "Part 1: #{program(input_array, 'NNSOFOCNHBVVNOBSBHCB', 5)}"
puts "Part 2: #{program(input_array, 'NNSOFOCNHBVVNOBSBHCB', 40)}"
