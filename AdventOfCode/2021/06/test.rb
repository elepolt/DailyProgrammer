# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [3, 4, 3, 1, 2]
  end

  def test_code
    assert_equal [[2, 3, 2, 0, 1].length, nil], program(@raw_input, 1)
    assert_equal [[1, 2, 1, 6, 0, 8].length, nil], program(@raw_input, 2)
    assert_equal [[0, 1, 0, 5, 6, 7, 8].length, nil], program(@raw_input, 3)
    assert_equal [[6, 0, 6, 4, 5, 6, 7, 8, 8].length, nil], program(@raw_input, 4)
    assert_equal [[5, 6, 5, 3, 4, 5, 6, 7, 7, 8].length, nil], program(@raw_input, 5)
    assert_equal [[4, 5, 4, 2, 3, 4, 5, 6, 6, 7].length, nil], program(@raw_input, 6)
    assert_equal [[3, 4, 3, 1, 2, 3, 4, 5, 5, 6].length, nil], program(@raw_input, 7)
    assert_equal [[6, 0, 6, 4, 5, 6, 0, 1, 1, 2, 6, 0, 1, 1, 1, 2, 2, 3, 3, 4, 6, 7, 8, 8, 8, 8].length, nil],
                 program(@raw_input, 18)
    assert_equal [5934, nil], program(@raw_input, 80)
    assert_equal [26_984_457_539, nil], program(@raw_input, 256)
  end
end
