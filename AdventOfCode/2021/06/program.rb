require_relative '../../base'

input_array = parse_intengers(split_string(read_lines('input.txt')[0], ','))

def program(array, number_of_days)
  fish_dictionary = {}
  array.each do |fish|
    fish_dictionary[fish] = 0 unless fish_dictionary.keys.include?(fish)
    fish_dictionary[fish] += 1
  end
  # I really need to remember the .tally function for creating maps
  # fish_dictionary = array.tally
  # fish_dictionary.default = 0

  number_of_days.times do
    next_day = {}
    fish_dictionary.each do |fish_days, fish_count|
      fish_days -= 1
      if fish_days == -1
        next_day[8] = 0 unless next_day.keys.include?(8)
        next_day[6] = 0 unless next_day.keys.include?(6)
        next_day[8] += fish_count
        next_day[6] += fish_count
      else
        next_day[fish_days] = 0 unless next_day.keys.include?(fish_days)
        next_day[fish_days] += fish_count
      end
    end
    fish_dictionary = next_day

    # All that can be completed by these four lines, how cool!
    # fish_dictionary.transform_keys! { |days| days - 1 }
    # fish_dictionary[8] = fish_dictionary[-1]
    # fish_dictionary[6] += fish_dictionary[-1]
    # fish_dictionary.delete(-1)
  end
  [fish_dictionary.values.sum, nil]
end

puts "Part 1/2: #{program(input_array, 80)}"
puts "Part 1/2: #{program(input_array, 256)}"
