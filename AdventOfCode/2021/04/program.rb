require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  first_score = 0

  bingo_numbers = get_bingo_numbers(array[0])
  boards = build_boards(array[2..])
  bingo_numbers.each do |number|
    break if first_score > 0

    boards.each do |board|
      break if first_score > 0

      board.check_number(number)
      if board.has_bingo?
        first_score = board.current_score * number
        break
      end
    end
  end

  final_score = 0
  boards = build_boards(array[2..])
  # Just loop through every board and assume it's the last winning board
  bingo_numbers.each do |number|
    boards.delete_if do |board|
      board.check_number(number)
      final_score = board.current_score * number

      # And delete it if it has a bingo. The correct 'final score' will be the last board
      board.has_bingo?
    end
  end


  [first_score, final_score]
end

class BingoBoard
  def initialize
    @all_numbers = []
    @board = []

    # This will be used to easily check if
    @checked_board = []
    5.times do
      @checked_board.push(Array.new(5, ''))
    end
  end

  def current_score
    @all_numbers.sum
  end

  def push_row(row)
    @board.push(row)
    @all_numbers = (@all_numbers + row).flatten
  end

  def show_board(checked = false)
    pp @board unless checked
    pp @checked_board if checked
  end

  def check_number(number)
    # byebug if number == 16
    @board.each_with_index do |row, idx|
      next unless row.include?(number)

      @all_numbers.delete(number)
      column = row.index(number)
      @checked_board[idx][column] = 'x'
    end
  end

  def has_bingo?
    @checked_board.each_with_index do |row, idx|
      return true if row.uniq == ['x']
      return true if @checked_board.map { |v| v[idx] }.uniq == ['x']
    end
    diagonal_values = []
    (0..@board.length - 1).each do |value|
      # byebug if @all_numbers == [3, 15, 22, 18, 19, 8, 25, 20, 12, 6]
      diagonal_values.push('x') if @checked_board[value][value] == 'x'
    end
    # diagonal_values.uniq == ['x']
    false
  end
end

def get_bingo_numbers(input_string)
  parse_intengers(split_string(input_string, ','))
end

def build_boards(array)
  all_boards = []
  tmp_board = BingoBoard.new
  array.each do |board_string|
    if board_string.empty?
      all_boards.push(tmp_board)
      tmp_board = BingoBoard.new
      next
    end

    board_numbers = parse_intengers(split_string(board_string, ' '))
    tmp_board.push_row(board_numbers)
  end
  all_boards.push(tmp_board)

  all_boards
end
# puts "Part 1: #{program(input_array)}"
# puts "Part 2: #{program(input_array)}"
