require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @parsed_array = [
      [2,1,9,9,9,4,3,2,1,0],
      [3,9,8,7,8,9,4,9,2,1],
      [9,8,5,6,7,8,9,8,9,2],
      [8,7,6,7,8,9,6,7,8,9],
      [9,8,9,9,9,6,5,6,7,8],
    ]
    @raw_input = [
      '2199943210',
      '3987894921',
      '9856789892',
      '8767896789',
      '9899965678',
    ]
  end

  def test_code
    assert_equal [15, 1134], program(@raw_input)
    assert_equal [2,9,3,9,8], check_neighbors(@raw_input, 0, 1)
    assert_equal [9, 4, 7, 8, 9], check_neighbors(@raw_input, 0, 4)
    square = [
      ['1', '2', '1'],
      ['2', 'x', '2'],
      ['1', '2', '1'],
    ]
    assert_equal [[0,1], [1,0], [1, 2], [2,1]], check_neighbors(square, 1, 1, true)
    # byebug
    # assert_equal [1, 2, 3], find_basin(@raw_input, 0, 1, 1)
    # assert_equal [0, 1, 1, 2, 2, 2, 3, 4, 4], find_basin(@raw_input, 0, 8, 0).sort
  end
end
