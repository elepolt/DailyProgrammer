require_relative '../../base'
require 'set'

input_array = read_lines('input.txt')

def program(array)
  array.map! { |a| parse_intengers(split_string(a, ''))}
  total_risk = 0
  basins = []

  (0..array.length - 1).each do |idy|
    (0..array[0].length - 1).each do |idx|
      height = array[idy][idx]
      neighbors = check_neighbors(array, idy, idx)
      next unless neighbors.select { |n| n < height }.empty?

      total_risk += height + 1

      # When we run into a low spot we should find the basin
      basin = find_basin(array, idy, idx, height, Set.new)
      basins.push(basin.length)
    end
  end
  [total_risk, basins.max(3).inject(:*)]
end

def check_neighbors(array, idy, idx, include_coords = false)
  neighbors = []
  (-1..1).each do |y|
    next if y + idy < 0
    next if y + idy > (array.length - 1)

    (-1..1).each do |x|
      next if y == 0 &&  x == 0
      next if x + idx < 0
      next if x + idx > (array[0].length - 1)

      check_height = array[y + idy][x + idx]
      neighbors.push(check_height) unless include_coords
      next unless include_coords

      # For AoC we want to ignore diagonals... so how do we do that...
      # I think when x/y are equal it's a diagonal, IE: -1 -1, 1 1,
      # but also when it's opposite, ie: -1 1
      # 1 2 1
      # 2 x 2
      # 1 2 1
      # byebug
      next if y == x || y == -x

      neighbors.push([y + idy, x + idx])
    end
  end
  neighbors
end

def find_basin(array, idy, idx, height, basin)
  basin.add([idy, idx])
  neighbors = check_neighbors(array, idy, idx, true)
  # Get all the neighbors who's height equal to or higher than our current
  higher_neighbors = neighbors.select { |n| array[n[0]][n[1]] >= height && array[n[0]][n[1]] != 9 }
  higher_neighbors.each do |neighbor|
    ny = neighbor[0]
    nx = neighbor[1]
    next if basin.include?([ny, nx])

    basin.add([ny, nx])
    basin.merge(find_basin(array, ny, nx, array[ny][nx], basin))
  end

  basin
end

puts "Part 1/2: #{program(input_array)}"
