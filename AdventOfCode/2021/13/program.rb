require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, folds)
  # array = array.to_set
  # fold = 655
  # fold = folds.first
  # fold = ['y', 7]
  # fold = ['x', 655]
  array.map! { |a| parse_intengers(split_string(a, ',')) }
  part_one = fold(array.dup, folds.first).to_set.count
  folds.each do |fold|
    fold(array, fold)
  end
  paper = []
  width = array.map{ |a| a[0] }.max + 1
  height = array.map{ |a| a[1] }.max + 1
  width.times do
    paper.push(Array.new(height, ' '))
  end
  puts ''
  array.uniq.each do |coords|
    # byebug
    paper[coords[0]][coords[1]] = 'X'
  end
  paper.transpose.each do |row|
    puts row.join('  ')
  end
  puts ''
  [part_one, nil]
end

def fold(array, fold)
  array.each_with_index do |coords, idx|
    x, y = coords

    # if fold[0] == y
    comparison_coord = fold[0] == 'x' ? 0 : 1
    fold_number = fold[1]
    # byebug
    next if coords[comparison_coord] <= fold_number

    # How far away is the number from the fold, then multiply that by 2
    x -= (x - fold_number) * 2 if comparison_coord.zero?
    y -= (y - fold_number) * 2 if comparison_coord.positive?

    array[idx] = [x, y]
  end

  array
end

# 859 too high

folds = [
  ['x', 655],
  ['y', 447],
  ['x', 327],
  ['y', 223],
  ['x', 163],
  ['y', 111],
  ['x', 81],
  ['y', 55],
  ['x', 40],
  ['y', 27],
  ['y', 13],
  ['y', 6],
]
puts "Part 1/2: #{program(input_array, folds)}"
