require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '6,10',
      '0,14',
      '9,10',
      '0,3',
      '10,4',
      '4,11',
      '6,0',
      '6,12',
      '4,1',
      '0,13',
      '10,12',
      '3,4',
      '3,0',
      '8,4',
      '1,10',
      '2,14',
      '8,10',
      '9,0',
    ]
    @raw_input2 = [
      '0,0',
      '0,1',
      '0,3',
      '1,4',
      '2,0',
      '3,0',
      '3,4',
      '4,1',
      '4,3',
      '5,0',
      '5,2',
      '5,4',
      '7,4',
      '8,0',
      '8,4',
      '9,2',
      '9,4',
    ]
    #   #.##.|#..#.
    #   #...#|.....
    #   .....|#...#
    #   #...#|.....
    #   .#.#.|#.###
  end

  def test_code
    assert_equal [17, nil], program(@raw_input, [['y', 7]])
    assert_equal [17, nil], program(@raw_input, [['y', 7], ['x', 5]])
    # assert_equal [16, nil], fold(@raw_input2, ['x', 5])
  end
end
