require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  # The plan is to keep a dictionary of xy coords, and to 
  total_map = {}
  diagonal_map = {}

  array.each do |coordinates_string|
    from, to = split_coordinates(coordinates_string)
    get_inbetween_coords(from, to, false).each do |coords|
      x, y = coords
      total_map[x] = {} unless total_map.keys.include?(x)
      total_map[x][y] = 0 unless total_map[x].keys.include?(y)
      total_map[x][y] += 1
    end
    get_inbetween_coords(from, to, true).each do |coords|
      x, y = coords
      diagonal_map[x] = {} unless diagonal_map.keys.include?(x)
      diagonal_map[x][y] = 0 unless diagonal_map[x].keys.include?(y)
      diagonal_map[x][y] += 1
    end
  end
  total_dupes = 0
  diagonal_dupes = 0

  # All the x values
  total_map.each_value do |values|
    # then all the y values that are hit more than once
    total_dupes += values.values.select { |v| v > 1 }.count
  end

  diagonal_map.each_value do |values|
    # then all the y values that are hit more than once
    diagonal_dupes += values.values.select { |v| v > 1 }.count
  end
  [total_dupes, diagonal_dupes]
end

def split_coordinates(string)
  from, to = split_string(string, ' -> ')
  from = parse_intengers(split_string(from, ','))
  to = parse_intengers(split_string(to, ','))
  [from, to]
end

# Given the from/to coords, we need to get all the coords between the two
# IE: [0, 0] -> [0, 2] should also give us [0, 1]
def get_inbetween_coords(from, to, include_diagonals)
  from_x, from_y = from
  to_x, to_y = to

  # part 1 says to ignore diagonals
  return [] if from_x != to_x && from_y != to_y && !include_diagonals

  # Essentially we know that if xy_range are both > 1, then it's a diagonal
  # and we can just zip our ranges and be off
  y_range = get_range(from_y, to_y)
  x_range = get_range(from_x, to_x)
  return x_range.zip(y_range) if x_range.length == y_range.length

  # Else just get the combinations of the two since x/y will only be one
  x_range.product(y_range)
end

def get_range(from, to)
  min, max = [from, to].minmax
  range = (min..max).to_a
  # Reverse it if necessary so that when we zip it properly goes to/from proper coordinates
  range.reverse! if to == min
  range
end

puts "Parts 1/2: #{program(input_array)}"
