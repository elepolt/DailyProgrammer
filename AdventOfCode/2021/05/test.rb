require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '0,9 -> 5,9',
      '8,0 -> 0,8',
      '9,4 -> 3,4',
      '2,2 -> 2,1',
      '7,0 -> 7,4',
      '6,4 -> 2,0',
      '0,9 -> 2,9',
      '3,4 -> 1,4',
      '0,0 -> 8,8',
      '5,5 -> 8,2',
    ]
  end

  def test_code
    assert_equal [5, 12], program(@raw_input)
    assert_equal [[0, 9], [5, 9]], split_coordinates('0,9 -> 5,9')
    assert_equal [[0, 9], [1, 9], [2, 9], [3, 9], [4, 9], [5, 9]], get_inbetween_coords([0, 9], [5, 9], false)
    assert_equal [], get_inbetween_coords([0, 0], [1, 1], false)
    # assert_equal [[0, 0], [1, 1]], get_inbetween_coords([0, 0], [1, 1], true)
    assert_equal [[9,7], [8,8], [7,9]], get_inbetween_coords([9, 7], [7, 9], true)
    assert_equal [[3, 4], [2, 4], [1, 4]], get_inbetween_coords([3, 4], [1, 4], false)
    assert_equal [1, 2, 3], get_range(1, 3)
    assert_equal [3, 2, 1], get_range(3, 1)
  end
end
