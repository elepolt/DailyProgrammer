require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  total_flashes = 0
  board = array.map { |a| parse_intengers(split_string(a, '')) }
  step_count = 0
  while true do
    step_count += 1

    # Add 1 to every value
    board = energy_step(board)

    # Now loop through and flash all the ones
    board.length.times do |idy|
      board[idy].length.times do |idx|
        next unless board[idy][idx] > 9

        # Flash, and then add energy to each of the neighbors
        board = flash_neighbors(board, [idy, idx])
        # neighbors = traverse(board, [idy, idx])

      end
    end

    flash_count = board.map{ |y| y.count(0) }.sum
    break if flash_count  == board.length * board[0].length
    # Stop keeping track of part1
    next if step_count > 100
    # Get all the 0s here, and that should be the flash count for the step
    total_flashes += flash_count
    # pp board
  end

  # byebug
  [total_flashes, step_count]
end

def energy_step(array)
  array.each_with_index do |row, idy|
    row.each_with_index do |_column_value, idx|
      array[idy][idx] += 1
    end
  end
  array
end

def flash_neighbors(array, coordinates)
  idy, idx = coordinates
  # byebug if coordinates == [0,2]
  array[idy][idx] = 0
  neighbors = traverse(array, [idy, idx])
  neighbors.each do |neighbor_coords|
    nidy, nidx = neighbor_coords
    # Essentially, if the neighbor has already been flashed, don't add to it
    next if array[nidy][nidx] == 0

    # Then add 1 for flashing and continue
    array[nidy][nidx] += 1
    next if array[nidy][nidx] <= 9

    array = flash_neighbors(array, [nidy, nidx])
  end

  array
end

puts "Part 1/2: #{program(input_array)}"
