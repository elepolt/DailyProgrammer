# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'two1nine',
      'eightwothree',
      'abcone2threexyz',
      'xtwone3four',
      '4nineeightseven2',
      'zoneight234',
      '7pqrstsixteen',
    ]
  end

  def test_code
    assert_equal 2, get_number('two1nine', false)
    assert_equal 9, get_number('two1nine', true)
    assert_equal 8, get_number('eightwothree', false)
    assert_equal 3, get_number('eightwothree', true)
    assert_equal 1, get_number('abcone2threexyz', false)
    assert_equal 3, get_number('abcone2threexyz', true)
    assert_equal 2, get_number('xtwone3four', false)
    assert_equal 4, get_number('xtwone3four', true)
    assert_equal 4, get_number('4nineeightseven2', false)
    assert_equal 2, get_number('4nineeightseven2', true)
    assert_equal 1, get_number('zoneight234', false)
    assert_equal 4, get_number('zoneight234', true)
    assert_equal 7, get_number('7pqrstsixteen', false)
    assert_equal 6, get_number('7pqrstsixteen', true)
    
    assert_equal [209, 281], program(@raw_input)
  end
end
