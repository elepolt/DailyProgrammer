# frozen_string_literal: true

require_relative '../../base'

def get_number(string, reversed)
  written_nums = ['one','two','three','four','five','six','seven','eight','nine']
  if reversed
    string = string.reverse
    written_nums = written_nums.map(&:reverse)
  end
  chars = string.split('')
  found_char = chars.find{ |c| c.to_i != 0}
  # this was probably not necessary, but the test cases had some spots with no numbers
  found_char_index = chars.index(found_char).nil? ? (string.length + 1) : chars.index(found_char)

  chars.each_with_index do |char, i|
    # Loops through to see where a written number exists in our string
    num = written_nums.find { |num| string[..i].index(num) != nil }
    # And returns it as long as it comes before the digit
    return written_nums.index(num) + 1 if num != nil && i < found_char_index
  end
  found_char.to_i
end

def program(array)
  sum = 0
  array.each do |string|
    chars = string.split('')
    first_char = chars.find{ |c| c.to_i != 0}
    last_char = chars.reverse.find{ |c| c.to_i != 0}
    sum += [first_char, last_char].join('').to_i
  end

  sum2 = 0
  array.each do |string|
    first_char = get_number(string, false)
    last_char = get_number(string, true)

    sum2 += [first_char, last_char].join('').to_i
  end

  [sum, sum2]
end


input_array = read_lines('input.txt')
puts "Part 1/2: #{program(input_array)}"