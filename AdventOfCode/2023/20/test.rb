# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'broadcaster -> a, b, c',
      '%a -> b',
      '%b -> c',
      '%c -> inv',
      '&inv -> a'
    ]

    @second = [
      'broadcaster -> a',
      '%a -> inv, con',
      '&inv -> b',
      '%b -> con',
      '&con -> output'
    ]
  end

  def test_code
    assert_equal [32000000, nil], program(@raw_input)
    assert_equal [11687500, nil], program(@second)
    # 11687500
  end
end
