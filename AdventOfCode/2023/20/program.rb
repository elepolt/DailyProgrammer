# frozen_string_literal: true

require_relative '../../base'
POSSIBLE_ROUTES = Hash.new(0)

def program(array)
  modules = build_modules(array)
  p1 = 0
  count = 0
  10_000.times do
    count += 1
    button_press(modules, count)
    p1 = modules['broadcaster'][:high] * modules['broadcaster'][:low]
  end

  [p1, POSSIBLE_ROUTES.values.reduce(&:lcm)]
end

def button_press(modules, count)
  button_destinations = modules['broadcaster'][:destinations]
  modules['broadcaster'][:low] += (1 + button_destinations.length)
  next_details = []
  button_destinations.each do |key|
    # swap state - this makes an assumption that the broadcaster is not attached to an inverter
    modules[key][:state] = !modules[key][:state]
    next_details.append(key)
  end
  next_iteration(modules, next_details, count)
end

def next_iteration(modules, details, count)
  return if details.empty?

  next_details = []
  details.each do |key|
    modules[key][:destinations].each do |destination|
      # P2: We want to get the cycle of when the conjunction modules send a low voltage to 'rx'
      # We know that when they are ALL true then we have our answer by finding LCM. So just get their first
      # datapoint for when each one is true
      # This is a bit hardcoded from looking at my dataset and noting that bb goes to rx
      POSSIBLE_ROUTES[key] = count if destination == 'bb' && modules[key][:state] && !POSSIBLE_ROUTES.key?(key)

      modules[key][:state] ? modules['broadcaster'][:high] += 1 : modules['broadcaster'][:low] += 1
      next if modules[destination].nil?

      nd = modules[destination]
      # If a flip flop is passing a high state to another flip flop, nothing happens
      if nd[:type] == 'ff'
        next if modules[key][:state]

        # Then it's passing low, and it should change
        nd[:state] = !nd[:state]
        next_details.append(destination)
        next
      end

      pp nd if nd[:type] != 'inv'
      # If we get here, then nd is an inverter...
      # Set the last signal to the key that was received
      nd[:connections][key] = modules[key][:state]
      # if it remembers high pulses for all inputs, it sends a low pulse; otherwise, it sends a high pulse.
      nd[:state] = nd[:connections].values.uniq != [true]
      next_details.append(destination)
    end
  end
  next_iteration(modules, next_details, count)
end

def build_modules(array)
  modules = {}
  array.each do |line|
    src, destinations = parse_line(line)
    next if src == 'output' # Just skip it

    if src == 'broadcaster'
      modules[src] = {}
      modules[src][:destinations] = destinations
      modules[src][:high] = 0
      modules[src][:low] = 0
      next
    end

    key = src[1..]
    modules[key] = {}
    modules[key][:destinations] = destinations
    modules[key][:type] = src[0] == '%' ? 'ff' : 'inv'
    modules[key][:connections] = {} if modules[key][:type] == 'inv'
    modules[key][:state] = false if src[0] == '%'
  end
  # Now we need to go through each flipflop and get 
  inverters = modules.select { |k, m| m[:type] == 'inv' }.keys
  modules.select { |k, m| m[:type] == 'ff' }.each do |k, val|
    (val[:destinations] & inverters).each do |inv|
      modules[inv][:connections][k] = false
    end
  end
  modules
end

def parse_line(line)
  a, b = line.split(' -> ')
  [a, b.split(', ')]
end

input_array = read_lines('input.txt')
# 910517168 too high
puts "Part 1/2: #{program(input_array)}"
