# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '0,0,2~2,0,2',
      '0,2,3~2,2,3',
      '0,0,4~0,2,4',
      '1,0,1~1,2,1',
      '2,0,5~2,2,5',
      '0,1,6~2,1,6',
      '1,1,8~1,1,9'
    ]
  end

  def test_code
    assert_equal [5, nil], program(@raw_input)
  end
end
