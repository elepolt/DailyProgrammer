# frozen_string_literal: true

require_relative '../../base'

def program(array)
  bricks = parse_lines(array)
  min_z = 1000
  bricks.each do |brick|
    front, back = brick
    min_z = [min_z, front.last, back.last].min
  end

  pp min_z

  [nil, nil]
end

def parse_lines(array)
  bricks = []
  array.each do |line|
    front, back = line.split('~')
    front = front.split(',').map(&:to_i)
    back = back.split(',').map(&:to_i)
    bricks.append([front, back])
  end

  bricks
end

# input_array = read_lines('input.txt')
# puts "Part 1/2: #{program(input_array)}"
