# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  mapping = []
  array.each do |line|
    mapping.append(parse_intengers(line.split(' ')))
  end

  p1 = 0
  p2 = 0
  mapping.each do |history|
    rows = get_mapping(history, [history])
    p1 += rows.map(&:last).sum

    rows2 = get_mapping(history.reverse, [history.reverse])
    p2 += rows2.map(&:last).sum
  end

  [p1, p2]
end

def get_mapping(slice, rows)
  return rows if slice.uniq == [0]

  next_mapping = []
  slice.each_cons(2) do |items|
    next_mapping.append(items[1] - items[0])
  end
  rows.append(next_mapping)
  get_mapping(next_mapping, rows)
end
puts "Part 1/2: #{program(input_array)}"
