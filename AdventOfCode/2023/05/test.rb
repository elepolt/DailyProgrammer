# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'seeds: 79 14 55 13',
      '',
      'seed-to-soil map:',
      '50 98 2',
      '52 50 48',
      '',
      'soil-to-fertilizer map:',
      '0 15 37',
      '37 52 2',
      '39 0 15',
      '',
      'fertilizer-to-water map:',
      '49 53 8',
      '0 11 42',
      '42 0 7',
      '57 7 4',
      '',
      'water-to-light map:',
      '88 18 7',
      '18 25 70',
      '',
      'light-to-temperature map:',
      '45 77 23',
      '81 45 19',
      '68 64 13',
      '',
      'temperature-to-humidity map:',
      '0 69 1',
      '1 0 69',
      '',
      'humidity-to-location map:',
      '60 56 37',
      '56 93 4'
    ]
  end

  def test_code
    assert_equal [35, 46], program(@raw_input)
    seeds, results = parse_seeds_and_mapping(@raw_input.dup)
    assert_equal [79, 14, 55, 13], seeds
    results_check = { [98, 99] => -48, [50, 97] => 2 }
    assert_equal results_check, results['seed-to-soil']

    assert_equal [[79, 92], [55, 67]], part_two_seeds_count(seeds)

    # "seed-to-soil"=>{"98-99"=>"50-51", "50-97"=>"52-99"},
    iteration = get_new_ranges([[79, 92], [55, 67]], results, 'seed-to-soil')
    assert_equal [[55, 67], [79, 92]], iteration

    iteration = new_mapping(iteration, results, 'seed-to-soil')
    assert_equal [[57, 69], [81, 94]], iteration

    # "soil-to-fertilizer"=>{"15-51"=>"0-36", "52-53"=>"37-38", "0-14"=>"39-53"},
    iteration = get_new_ranges(iteration, results, 'soil-to-fertilizer')
    assert_equal [[57, 69], [81, 94]], iteration
    iteration = new_mapping(iteration, results, 'soil-to-fertilizer')
    assert_equal [[57, 69], [81, 94]], iteration

    # "fertilizer-to-water"=>{"0-6"=>"42-48", "7-10"=>"57-60", "11-52"=>"0-41", "53-60"=>"49-56"},
    iteration = get_new_ranges(iteration, results, 'fertilizer-to-water')
    assert_equal [[57, 60], [61, 69], [81, 94]], iteration
    iteration = new_mapping(iteration, results, 'fertilizer-to-water')
    assert_equal [[53, 56], [61, 69], [81, 94]], iteration

    # "water-to-light"=>{"18-24"=>"88-94", "25-94"=>"18-87"},
    iteration = get_new_ranges(iteration, results, 'water-to-light')
    assert_equal [[53, 56], [61, 69], [81, 94]], iteration
    iteration = new_mapping(iteration, results, 'water-to-light')
    assert_equal [[46, 49], [54, 62], [74, 87]], iteration

    # "light-to-temperature"=>{"45-63"=>"81-99", "64-76"=>"68-80", "77-99"=>"45-67"},
    iteration = get_new_ranges(iteration, results, 'light-to-temperature')
    assert_equal [[46, 49], [54, 62], [74, 76], [77, 87]], iteration
    iteration = new_mapping(iteration, results, 'light-to-temperature')
    assert_equal [[45, 55], [78, 80], [82, 85], [90, 98]], iteration

    # # "temperature-to-humidity"=>{"69-69"=>"0-0", "0-68"=>"1-69"},
    iteration = get_new_ranges(iteration, results, 'temperature-to-humidity')
    assert_equal [[45, 55], [78, 80], [82, 85], [90, 98]], iteration
    iteration = new_mapping(iteration, results, 'temperature-to-humidity')
    assert_equal [[46, 56], [78, 80], [82, 85], [90, 98]], iteration


    # {"seed-to-soil"=>{[98, 99]=>-48, [50, 97]=>2},
    # "soil-to-fertilizer"=>{[15, 51]=>-15, [52, 53]=>-15, [0, 14]=>39},
    # "fertilizer-to-water"=>{[53, 60]=>-4, [11, 52]=>-11, [0, 6]=>42, [7, 10]=>50},
    # "water-to-light"=>{[18, 24]=>70, [25, 94]=>-7},
    # "light-to-temperature"=>{[77, 99]=>-32, [45, 63]=>36, [64, 76]=>4},
    # "temperature-to-humidity"=>{[69, 69]=>-69, [0, 68]=>1},
    # "humidity-to-location"=>{[56, 92]=>4, [93, 96]=>-37}}

    iteration = get_new_ranges(iteration, results, 'humidity-to-location')
    assert_equal [[46, 55], [56, 56], [78, 80], [82, 85], [90, 92], [93, 96], [93, 98], [97, 98]], iteration

    iteration = new_mapping(iteration, results, 'humidity-to-location')
    assert_equal [[46, 55], [56, 59], [60, 60], [82, 84], [86, 89], [94, 96], [97, 98]], iteration

  end
end
