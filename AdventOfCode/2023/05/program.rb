# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  seeds, results = parse_seeds_and_mapping(array.dup)

  part1 = 265483689800
  seeds.each do |seed|
    next_mapping_id = seed
    results.each_key do |k|
      results[k].each do |range, diff|
        next unless next_mapping_id >= range[0] && next_mapping_id <= range[1]

        next_mapping_id = diff + next_mapping_id
        break
      end
    end
    part1 = [part1, next_mapping_id].min
  end

  seed_array = part_two_seeds_count(seeds)
  next_array = seed_array
  results.each_key do |key|
    next_array = get_new_ranges(next_array, results, key)
    next_array = new_mapping(next_array, results, key)
  end

  [part1, next_array.flatten.min]
end

def parse_seeds_and_mapping(array)
  seeds = array.shift.split(': ')[1].split(' ').map(&:to_i)
  array.shift
  results = parse_mapping(array)
  [seeds, results]
end

def part_two_seeds_count(seeds)
  seed_array = []
  seeds.each_slice(2) do |seed_details|
    seed_start = seed_details[0]
    seed_count = seed_details[1]

    # seed_array = [#{seed_start}-#{seed_start + seed_count - 1}"]
    seed_array.append([seed_start, seed_start + seed_count - 1])
  end
  seed_array
end

def get_new_ranges(seed_array, results, key)
  next_array = []

  seed_array.sort.each do |sr|
    # byebug if key == 'fertilizer-to-water'
    keys = keys_in_range(results[key].keys.sort, sr)
    next_array.append(sr) if keys.empty?
    keys.each do |k|
      ss_in_range = in_range(k, sr[0])
      se_in_range = in_range(k, sr[1])

      if ss_in_range && !se_in_range
        next_array.append([k[1] + 1, sr[1]])
        next_array.append([sr[0], k[1]])
      end

      if se_in_range && !ss_in_range
        next_array.append([sr[0], k[0] - 1])
        next_array.append([k[0], sr[1]])
      end

      if ss_in_range && se_in_range
        next_array.append(sr)
      end

      if !ss_in_range && !se_in_range
        next_array.append([sr[0], k[0] - 1])
        next_array.append([k[1]+1, sr[1]])
        next_array.append(k)
      end
    end
  end

  return seed_array if next_array.empty?

  next_array.uniq.sort
end

def new_mapping(seed_array, results, key)
  next_array = []
  seed_array.each do |sr|
  #   # find which mapping the start and end match with
    start_seed = sr[0]
    end_seed = sr[1]
    keys = results[key].keys
    t_key_start = ''
    t_key_end = ''
    keys.each do |result_key|
      key_start = result_key[0]
      key_end = result_key[1]
      t_key_start = result_key if start_seed >= key_start && start_seed < key_end
      t_key_end = result_key if end_seed >= key_start && end_seed <= key_end
    end

    # If the keys are the same, and they didn't map to anything, then we just append the numbers
    if t_key_end == t_key_start && t_key_end == ''
      next_array.append(sr)
      next
    end

    # byebug if key == 'soil-to-fertilizer'
    # If they're the same, and they fall within the same mapping, do math and append the new mappings
    if t_key_end == t_key_start && t_key_end != ''
      diff = results[key][t_key_start]
      new_start = diff + sr[0]
      new_end = diff + sr[1]
      next_array.append([new_start, new_end])
      next
    end
  end

  next_array.sort
end

def in_range(range, num)
  num >= range[0] && num <= range[1]
end

def keys_in_range(keys, range)
  in_range = []
  keys.each do |key|
    in_range.append(key) if range[0] >= key[0] && range[0] <= key[1]
    in_range.append(key) if key[0] >= range[0] && key[0] <= range[1]
    in_range.append(key) if range[1] >= key[0] && range[1] <= key[1]
    in_range.append(key) if key[1] >= range[0] && key[1] <= range[1]
  end

  in_range.uniq
end

def parse_mapping(array)
  results = {}
  type = ''
  array.each do |line|
    next if line == ''

    if line.include?(' map')
      type = line.split(' map')[0]
      # pp type
      results[type] = {}
      next
    end

    nums = line.split(' ').map(&:to_i)
    destination = "#{nums[0]}-#{nums[0] + nums[2] - 1}"
    source = "#{nums[1]}-#{nums[1] + nums[2] - 1}"
    results[type][[nums[1], nums[1] + nums[2] - 1]] = nums[0] - nums[1]
  end

  results
end
# 194601276 too high
puts "Part 1/2: #{program(input_array)}"
