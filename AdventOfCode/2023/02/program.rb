# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  results = {}
  array.each do |line|
    game_id, sets = line.split(': ')
    id = game_id.split(' ')[1].to_i
    games = sets.split("\;")
    results[id] = games
  end
  part2 = 0
  possible = []
  results.each do |id, games|
    is_possible = true
    max_counts = Hash.new(0)
    games.each do |game|
      results = {}
      counts = Hash.new(0)
      # This .each turns 3 red, 4 green into {'red': 3, 'green': 4}
      # it also keeps track of the max count of each color for part 2
      game.split(', ').each do |r|
        color, count = r.split(' ').reverse
        counts[color] = count.to_i
        max_counts[color] = [max_counts[color], count.to_i].max
      end
      # only 12 red cubes, 13 green cubes, and 14 blue cubes
      is_possible = false if (counts['red'] > 12) || (counts['green'] > 13) || (counts['blue'] > 14)
    end

    possible.append(id) if is_possible
    part2 += max_counts.values.reduce(:*)
  end
  [possible.sum, part2]
end

puts "Part 1/2: #{program(input_array)}"
