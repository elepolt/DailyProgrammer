# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7'
    ]
  end

  def test_code
    assert_equal 52, math('HASH')
    assert_equal 30, math('rn=1')
    # assert_equal 253, math('cm-')
    # assert_equal 97, math('qp=3')
    # assert_equal 47, math('cm=2')
    # assert_equal 14, math('qp-')
    # assert_equal 180, math('pc=4')
    # assert_equal 9, math('ot=9')
    # assert_equal 197, math('ab=5')
    # assert_equal 48, math('pc-')
    # assert_equal 214, math('pc=6')
    # assert_equal 231, math('ot=7')

    assert_equal [1320, 145], program(@raw_input[0])
  end
end
