# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')[0]
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(long_string)
  codes = long_string.split(',')
  p1 = 0
  codes.each do |code|
    p1 += math(code)
  end

  [p1, p2(codes)]
end

def p2(codes)
  hashmap = Hash.new([])
  codes.each do |code|
    chars = code.chars.last == '-' ? code.split('-') : code.split('=')
    box_idx = math(chars[0])

    hashmap[box_idx].reject! { |t| t.split('=')[0] == chars[0] } if chars.length == 1
    next unless chars.length == 2

    h_idx = hashmap[box_idx].find_index { |t| t.split('=')[0] == chars[0] }

    if h_idx.nil?
      hashmap[box_idx] += [code]
    else
      hashmap[box_idx][h_idx] = code
    end
  end

  total = 0
  hashmap.each do |k, v|
    v.each_with_index do |code, idx|
      tmp = (k + 1) * (idx + 1) * code.split('=')[1].to_i
      total += tmp
    end
  end
  total
end

def math(code)
  val = 0

  code.each_byte do |ch|
    val += ch
    val *= 17
    val = val % 256
  end
  val
end

puts "Part 1/2: #{program(input_array)}"
