# frozen_string_literal: true

require_relative '../../base'
require 'pqueue'

def program(array)
  grid = build_grid(array)

  # Ok. Dijkstra. Here we go.
  starting_node = [0, grid[0].find_index('.')]
  ending_coords = [grid.length - 1, grid.last.find_index('.')]
  unvisited_nodes = PQueue.new
  unvisited_nodes.push([0, starting_node])

  dijkstra_values = {}
  dijkstra_values[starting_node] = {
    'value': 0,
    'previous_vertex': starting_node
  }

  values = explore(dijkstra_values, starting_node, ending_coords, grid)
  pp values

  values
end

def explore(dijkstra_values, starting_node, ending_coords, grid)
  unvisited_nodes = PQueue.new
  unvisited_nodes.push([0, starting_node])
  next_coords = []
  until unvisited_nodes.empty?
    risk, current_node = unvisited_nodes.shift

    neighbors = traverse(grid, current_node, 0, false)
    pp current_node
    # byebug if current_node == [5, 4]
    neighbors.reject! { |n| grid[n[0]][n[1]] == '#' }
    neighbors.reject! { |n| is_uphill(grid, current_node, n) } # && !part2 ?
    neighbors.reject! { |n| dijkstra_values[current_node][:previous_vertex] == n }

    neighbors.each do |coord|
      next if is_uphill(grid, current_node, coord) # && !part2

      # coord_value = grid[coord[0]][coord[1]]
      # next if visited.include?(coord)
      # visited.append(coord)

      # If this is new, initialize it and push it into our unvisited queue
      unless dijkstra_values.key?(coord)
        dijkstra_values[coord] = {
          'value': 0
        }
      end

      # Then compare the current values to it
      value = 1 + risk

      next if value <= risk

      dijkstra_values[coord][:value] = value
      dijkstra_values[coord][:previous_vertex] = current_node
      unvisited_nodes.push([value, coord])
      return dijkstra_values[ending_coords][:value] if coord == ending_coords
    end

    # byebug if current_node == [5, 3]
    if neighbors.length == 2
      next_coords = neighbors
      break
    end
  end

  return dijkstra_values[ending_coords][:value] if next_coords.empty?

  values = []
  next_coords.each do |coord|
    values.append(explore(dijkstra_values, coord, ending_coords, grid))
  end

  # pp dijkstra_values
  # coord = ending_coords
  # while coord != [0, 1]
  #   # pp coord
  #   grid[coord[0]][coord[1]] = '0'
  #   coord = dijkstra_values[coord][:previous_vertex]
  # end
  # pp grid
  # grid.each do |line|
  #   puts line.join
  # end
  # [dijkstra_values[ending_coords][:value], nil]
  values
end

def is_uphill(grid, current_node, coord)
  return false if grid[coord[0]][coord[1]] == '.'
  return true if grid[coord[0]][coord[1]] == 'v' && coord[0] == current_node[0] - 1
  return true if grid[coord[0]][coord[1]] == '^' && coord[0] == current_node[0] + 1
  return true if grid[coord[0]][coord[1]] == '>' && coord[1] == current_node[1] - 1

  grid[coord[0]][coord[1]] == '<' && coord[1] == current_node[1] + 1
end

def build_grid(array)
  grid = []
  array.each do |line|
    grid.append(line.chars)
  end
  grid
end

input_array = read_lines('input.txt')
# puts "Part 1/2: #{program(input_array)}"
