# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  rules, scores = parse_lines(array)
  totals = []
  scores.each do |score|
    result = follow_rules(score, rules, 'in')
    totals.append(score.map(&:last)) if result == 'A'
  end
  [totals.flatten.sum, nil]
end

def follow_rules(score, rules, current_rule)
  tmp_rules = rules.find { |idx| idx.first == current_rule }[1]
  tmp_rules.each do |tmp_rule|
    comparison, rule = tmp_rule.split(':')
    # If the rule is nil it means we got to the end of the possible rules
    # So the 'comparison' is now actually the rule because there's nothing to compare
    if rule.nil?
      return 'A' if comparison == 'A'
      return 'R' if comparison == 'R'

      return follow_rules(score, rules, comparison)
    end

    comparison_score = score.find { |idx| idx.first == comparison[0] }[1]
    if comparison[1] == '<' && (comparison_score < comparison[2..].to_i)
      return 'A' if rule == 'A'
      return 'R' if rule == 'R'

      return follow_rules(score, rules, rule)
    end
    next unless comparison[1] == '>' && (comparison_score > comparison[2..].to_i)
    return 'A' if rule == 'A'
    return 'R' if rule == 'R'

    return follow_rules(score, rules, rule)
  end
  'R'
end

def parse_lines(array)
  rules = []
  scores = []
  output = [rules, scores]
  idx = 0
  array.each do |line|
    if line == ''
      idx += 1
      next
    end
    parsed_line = idx.zero? ? parse_rules(line) : parse_scores(line)
    output[idx].append(parsed_line)
  end

  output
end

def parse_rules(line)
  key, rule_strings = line.split('{')
  rules = rule_strings[..-2].split(',')
  [key, rules]
end

def parse_scores(line)
  scores = line[1..-2].split(',')
  scores.map { |idy| idy.split('=') }.map { |j| [j.first, j.last.to_i] }
end
puts "Part 1/2: #{program(input_array)}"
