# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  total_scratch_cards = Hash.new(0)
  part1 = 0
  part2 = 0
  array.each do |game|
    id = game.split(':')[0].split(' ')[1].to_i
    winning_numbers, playing_numbers = parse_string(game)
    winning_count = (winning_numbers & playing_numbers).count

    score = winning_count.positive? ? 2**(winning_count - 1) : 0

    total_scratch_cards[id] += 1

    part1 += score
    part2 += total_scratch_cards[id]

    (id + 1..id + winning_count).to_a.each do |next_id|
      total_scratch_cards[next_id] += 1 * total_scratch_cards[id]
    end
  end

  [part1, part2]
end

def parse_string(string)
  numbers = string.split(':')[1]
  winning_numbers, playing_numbers = numbers.split('|')
  winning_numbers = winning_numbers.split(' ').map(&:to_i)
  playing_numbers = playing_numbers.split(' ').map(&:to_i)
  [winning_numbers, playing_numbers]
end
puts "Part 1/2: #{program(input_array)}"
