# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  grid = []
  array.each_with_index do |line, i|
    grid[i] = line.split('')
  end
  cool_symbols = %w[! @ # $ % ^ & * ( ) - + / =]

  number_indices = Hash.new(0)

  grid.each_with_index do |row, idy|
    number = 0
    row.each_with_index do |cell, idx|
      # Reset our number when we hit a symbol/period
      number = 0 unless cell.is_integer?
      next unless cell.is_integer?

      # If we got here, it means that we found a number
      if number.zero?
        # If it's zero then we need to figure out what the number is that we're dealing with
        number_ends = row[(idx + 1)..].find_index { |a| !a.is_integer? }
        # Literal edge case; find_index returns nil if number ends at the edge of the grid
        number_ends = row[(idx + 1)..].length if number_ends.nil?
        number = row[idx..idx + number_ends].join.to_i if number.zero?
      end
      # Then keep track of what coordinates deal with what number.
      number_indices["#{idy} #{idx}"] = number
    end
  end

  part1 = 0
  part2 = 0
  # Ok. It's inefficient but I don't care.
  # Now we go through the grid a second time, but this time we're looking for symbols
  grid.each_with_index do |row, idy|
    row.each_with_index do |cell, idx|
      next unless cool_symbols.include?(cell)

      # Once we find a symbol, get the neighbors
      neighbors = traverse(grid, [idy, idx], 0, true)
      # Check the indices from above to see if what numbers it's touching
      neighbor_numbers = neighbors.map { |n| number_indices[n.join(' ')] }.uniq - [0]
      # NOTE: This solution only works because a number never touches more than one symbol
      part1 += neighbor_numbers.sum
      part2 += neighbor_numbers.reduce(:*) if neighbor_numbers.count == 2
    end
  end

  [part1, part2]
end

puts "Part 1/2: #{program(input_array)}"
