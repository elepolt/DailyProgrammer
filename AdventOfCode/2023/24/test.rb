# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = []
  end

  def test_code
    assert_equal [nil, nil], program(@raw_input)
  end
end
