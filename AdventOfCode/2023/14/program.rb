# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  grid = []
  array.each_with_index do |line, idx|
    grid[idx] = line.split('')
  end

  # Moves the grid so that north is west
  # next_grid = []
  # grid = grid.transpose
  # grid.each do |line|
  #   next_grid.append(move_rocks(line))
  # end
  # # And moves it back
  # pp next_grid.transpose
  grid = cycle(grid)

  # .transpose.map(&:reverse)

  # Counts load
  p1 = 0
  next_grid.each do |line|
    line.each_with_index do |space, idx|
      p1 += line.length - idx if space == 'O'
    end
  end
  [p1, nil]
end

# This will move the rocks north, then west, then south, then east
def cycle(grid)
  # Moves the grid so that north is west
  next_grid = []
  grid = grid.transpose
  grid.each do |line|
    next_grid.append(move_rocks(line))
  end
  # And moves it back
  pp next_grid.transpose

  grid = next_grid
  # Move all rocks west
  grid.each do |line|
    next_grid.append(move_rocks(line))
  end

  pp grid
  # grid = next_grid.reverse.transpose

  grid
end

# This method moves rocks from east to west.
def move_rocks(line)
  # 'O', 'O', '.', 'O', '.', 'O', '.', '.', '#', '#']
  # becomes
  # 'O', 'O', 'O', 'O', '.', '.', '.', '.', '#', '#']
  next_line = []
  empty = []
  line.each do |cell|
    next_line.append(cell) if cell == 'O'
    empty.append(cell) if cell == '.'

    next if cell != '#'

    next_line += empty
    next_line.append('#')
    empty = []
  end

  next_line += ['.'] * (line.length - next_line.length)
  next_line
end

# puts "Part 1/2: #{program(input_array)}"
