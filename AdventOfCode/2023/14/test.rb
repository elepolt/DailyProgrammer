# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'O....#....',
      'O.OO#....#',
      '.....##...',
      'OO.#O....O',
      '.O.....O#.',
      'O.#..O.#.#',
      '..O..#O..O',
      '.......O..',
      '#....###..',
      '#OO..#....'
    ]
  end

  def test_code
    assert_equal [136, nil], program(@raw_input)

    assert_equal ['O', 'O', 'O', 'O', '.', '.', '.', '.', '#', '#'], move_rocks(['O', 'O', '.', 'O', '.', 'O', '.', '.', '#', '#'])
  end
end
