# frozen_string_literal: true

require_relative '../../base'

def program(array, steps)
  grid = []
  start_coord = nil
  array.each_with_index do |line, idx|
    grid.append(line.chars)
    start_coord = [idx, line.chars.index('S')] unless line.chars.index('S').nil?
  end

  part1(grid, start_coord, steps)
end

def part1(grid, start_coord, steps)
  history = {}

  beds = [start_coord]
  steps.times do |idx|
    next_beds = []
    # byebug
    beds.each do |coord|
      if history.key?(coord)
        history[coord].each { |n| next_beds.append(n) }
        next
      end
      neighbors = traverse(grid, coord, 0, false)
      neighbors.select! { |c| grid[c[0]][c[1]] == '.' || grid[c[0]][c[1]] == 'S' }
      neighbors.each { |n| next_beds.append(n) }.uniq
      history[coord] = neighbors
    end
    beds = next_beds.uniq
  end

  # beds.each do |coord|
  #   grid[coord[0]][coord[1]] = '0'
  # end

  beds.uniq.length
end

def part2(array, steps)
  grid = []
  start_coord = nil
  array.each_with_index do |line, idx|
    grid.append(line.chars)
    start_coord = [idx, line.chars.index('S')] unless line.chars.index('S').nil?
  end

  history = {}

  beds = [start_coord]
  usebb = false
  total = 0
  steps.times do |idx|
    next_beds = []
    # byebug
    beds.each do |coord|
      coord[0] = grid.length if coord[0].zero?
      coord[0] = 0 if coord[0] == grid.length
      coord[1] = grid[0].length if coord[1].zero?
      coord[1] = 1 if coord[0] == grid.length
      if history.key?(coord)
        total += history[coord]
        next
      end

      # byebug if coord[0] == grid.length || coord[0].zero?
      neighbors = traverse(grid, coord, 0, false)
      neighbors.select! { |c| grid[c[0]][c[1]] == '.' || grid[c[0]][c[1]] == 'S' }
      neighbors.each { |n| next_beds.append(n) }.uniq
      history[coord] = neighbors.count
      total += history[coord]
    end
    beds = next_beds.uniq
  end

  beds.each do |coord|
    grid[coord[0]][coord[1]] = '0'
  end

  total
end

# input_array = read_lines('input.txt')
# puts "Part 1: #{program(input_array, 64)}"
# puts "Part 2: #{part2(input_array, 64)}"
