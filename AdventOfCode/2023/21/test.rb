# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '...........',
      '.....###.#.',
      '.###.##..#.',
      '..#.#...#..',
      '....#.#....',
      '.##..S####.',
      '.##..#...#.',
      '.......##..',
      '.##.#.####.',
      '.##..##.##.',
      '...........'
    ]
  end

  def test_code
    assert_equal 16, program(@raw_input, 6)
    assert_equal 1594, part2(@raw_input, 100)
  end
end
