# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      [7, 9],
      [15, 40],
      [30, 200]
    ]
  end

  def test_code
    assert_equal [288, 71503], program(@raw_input)
  end
end
