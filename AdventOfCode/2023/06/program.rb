# frozen_string_literal: true

require_relative '../../base'

def program(array)
  # Boat moves at 1mm/s
  wins = Hash.new(0)
  array.each do |game|
    time, distance = game
    key = "#{time}-#{distance}"
    wins[key] = []
    (1..time).to_a.each do |hold|
      outcome = hold * (time - hold)
      wins[key].append(hold) if outcome > distance
    end
  end

  p2 = 0
  time = array.map(&:first).join.to_i
  distance = array.map(&:last).join.to_i
  (1..time).to_a.each do |hold|
    outcome = hold * (time - hold)
    p2 += 1 if outcome > distance
  end

  [wins.values.map(&:count).reduce(:*), p2]
end

input_array = read_lines('input.txt')
game_times = input_array[0].split(': ')[1].split(' ').map(&:to_i)
game_distances = input_array[1].split(': ')[1].split(' ').map(&:to_i)
games = game_times.zip(game_distances)
puts "Part 1/2: #{program(games)}"
