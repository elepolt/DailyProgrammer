# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array, dist = 1)
  [calculate(array, 2), calculate(array, dist)]
end

def calculate(array, dist)
  map = []
  empty_rows = []
  galaxies = []
  array.each_with_index do |line, idx|
    s = line.split('')
    galaxy_idx = s.each_index.select { |i| line[i] == '#' }
    galaxy_idx.each do |num|
      galaxies.append([idx, num])
    end
    map.append(s)
    empty_rows.append(idx) if s.uniq == ['.']
  end

  empty_cols = []
  map[0].length.times do |idy|
    col_vals = map.map { |l| l[idy] }
    empty_cols.append(idy) if col_vals.uniq == ['.']
  end

  p2 = 0
  all_combos = galaxies.combination(2).to_a
  all_combos.each do |original_combo|
    combo = original_combo.map(&:clone)
    if empty_rows.length.positive?
      row_start, row_end = [combo[0][0], combo[1][0]].sort
      rows_crossed = ((row_start..row_end).to_a & empty_rows).length
      combo[0][0] += (rows_crossed * dist - rows_crossed) if combo[0][0] > combo[1][0]
      combo[1][0] += (rows_crossed * dist - rows_crossed) if combo[1][0] > combo[0][0]
    end
    if empty_cols.length.positive?
      col_start, col_end = [combo[0][1], combo[1][1]].sort
      cols_crossed = ((col_start..col_end).to_a & empty_cols).length
      combo[0][1] += (cols_crossed * dist - cols_crossed) if combo[0][1] > combo[1][1]
      combo[1][1] += (cols_crossed * dist - cols_crossed) if combo[1][1] > combo[0][1]
    end
    mh = manhattan(combo[0], combo[1])
    p2 += mh
  end

  p2
end

def manhattan(start_coord, end_coord)
  (start_coord[0] - end_coord[0]).abs + (start_coord[1] - end_coord[1]).abs
end

puts "Part 1/2: #{program(input_array, 1_000_000)}"
