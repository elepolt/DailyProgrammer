# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '...#......',
      '.......#..',
      '#.........',
      '..........',
      '......#...',
      '.#........',
      '.........#',
      '..........',
      '.......#..',
      '#...#.....'
    ]
  end

  def test_code
    assert_equal 9, manhattan([6, 1], [11, 5])
    assert_equal 15, manhattan([0, 4], [10, 9])
    assert_equal 17, manhattan([2, 0], [7, 12])
    assert_equal 5, manhattan([11, 0], [11, 5])
    assert_equal 6, manhattan([0, 4], [2, 0])
    assert_equal 6, manhattan([0, 4], [4, 6])
    assert_equal [374, 374], program(@raw_input, 2)
    assert_equal [374, 1030], program(@raw_input, 10)
    assert_equal [374, 8410], program(@raw_input, 100)
  end
end
