# frozen_string_literal: true

require_relative '../../base'

read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  # Damaged: #
  # Operational: .
  # Unknown: ?
  total = 0
  array.each_with_index do |line, _idx|
    springs, count = parse_line(line)
    result = eager(springs, count)
    total += result
  end
  [total, nil]
end

DP = {}

def eager(springs, count)
  return 1 if (springs.nil? || springs.empty?) && (count.nil? || count.empty?)
  return DP[springs, count] if DP.key([springs, count])

  return 0 if springs.nil? || springs.empty? && count&.length&.positive?

  byebug if springs.nil? || springs.empty? || count.empty? || count.nil?
  # Ok. Let's do this logically
  # We're going to start by saying we're trying to start our process at every available
  # index of our array of springs
  # Yeah. A lot of these are going to be worthless, but do it anyway. They'll fail fast, I promise
  total = 0

  # byebug
  (0..springs.length - 1).to_a.each do |idx|
    # If we get to a point where there are less springs then the amount we need to get to, return 0
    return 0 if springs[idx..].length < count.sum

    pp springs
    # I want to step through the amount of our count[0] and add # when I can to make it to our count,
    # or add # to our count
    cc = 0
    byebug
    (0..count[0]).to_a.each do |idy|
      spring = springs[idx + idy]
      # return total if spring.nil?

      # If we've hit our target number
      if cc == count[0]
        # And our spring is a '#'
        # Then we've broken our rules of adding in #
        # eager load from here
        if spring == '#'
          cc = 0
          next
        end

        # If we've hit a ?, great!
        # We'll set that spring to a .
        # remove one from our count and start over at the next spot
        if spring == '?' || spring == '.' || spring.nil?
          cc = 0
          total += 1 if eager(springs[(idx + idy + 1)..], count[1..]) && springs[idx..count[0]].include?('?')
          DP[[springs, count]] = total
          next
        end
      end

      if spring == '#'
        cc += 1
        next
      end

      if spring == '?'
        cc += 1
        next
      end

      # If we hit a . then we have to end, move on to the next eager method
      if spring == '.'
        cc = 0
        total += 1 if eager(springs[(idx + idy + 1)..], count) && springs.include?('?')
        DP[[springs, count]] = total
        next
      end

      cc = 0
      pp [spring, count]
      pp 'Howd you get here?'
    end
  end
  total
end

def parse_line(line)
  springs, count = line.split(' ').map { |i| i.split('') }
  count = count.map(&:to_i) - [0]

  [springs, count]
end

# puts "Part 1/2: #{program(input_array)}"
