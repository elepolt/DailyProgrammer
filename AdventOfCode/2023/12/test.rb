# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '???.### 1,1,3',
      '.??..??...?##. 1,1,3',
      '?#?#?#?#?#?#?#? 1,3,1,6',
      '????.#...#... 4,1,1',
      '????.######..#####. 1,6,5',
      '?###???????? 3,2,1'
    ]
  end

  def test_code
    # assert_equal [21, nil], program(@raw_input)

    # springs, count = parse_line('.### 3')
    # assert_equal 0, eager(springs, count)
    # springs, count = parse_line('### 3')
    # assert_equal 0, eager(springs, count)
    springs, count = parse_line('?.### 1,3')
    assert_equal 1, eager(springs, count)
    # springs, count = parse_line('???.### 1,1,3')
    # assert_equal 1, eager(springs, count)
    # springs, count = parse_line('.??..??...?##. 1,1,3')
    # assert_equal 4, eager(springs, count)
    # springs, count = parse_line('?#?#?#?#?#?#?#? 1,3,1,6')
    # assert_equal 1, eager(springs, count)
    # springs, count = parse_line('????.#...#... 4,1,1')
    # assert_equal 1, eager(springs, count)
    # springs, count = parse_line('????.######..#####. 1,6,5')
    # assert_equal 4, eager(springs, count)
    # springs, count = parse_line('?###???????? 3,2,1')
    # assert_equal 10, eager(springs, count)
  end
end
