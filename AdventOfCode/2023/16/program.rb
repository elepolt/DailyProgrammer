# frozen_string_literal: true

require_relative '../../base'

# input_array = read_lines('ex.txt')
input_array = read_lines('input.txt')

def program(array)
  grid = []
  array.each_with_index do |line, idx|
    grid[idx] = line.chars
  end

  scores = []
  (0..grid.length - 1).to_a.each do |idx|
    scores.append(shine_light(grid, [[[0, idx], 'd']]))
    scores.append(shine_light(grid, [[[grid.length - 1, idx], 'u']]))
  end

  (0..grid[0].length - 1).to_a.each do |idx|
    scores.append(shine_light(grid, [[[idx, 0], 'r']]))
    scores.append(shine_light(grid, [[[idx, grid[0].length - 1, idx], 'l']]))
  end

  lights = [[[0, -1], 'r']]
  p1 = shine_light(grid, lights)
  [p1, scores.max]
end

def shine_light(grid, lights)
  visited = {}
  loop do
    next_lights = []
    break if lights.empty?

    lights.each do |light|
      light[0] = next_coord(light[0].clone, light[1])
      next if out_of_bounds(grid, light[0])

      coord = grid[light[0][0]][light[0][1]]
      direction = light[1]

      next_lights.append(light) if coord == '.' || continue_line(coord, direction)

      if coord == '\\'
        didx = %w[r l u d].index(direction)
        direction = %w[d u l r][didx]
        next_lights.append([light[0], direction]) unless visited.key?([light[0], direction])
      end
      if coord == '/'
        didx = %w[l r d u].index(direction)
        direction = %w[d u l r][didx]
        next_lights.append([light[0], direction]) unless visited.key?([light[0], direction])
      end
      if coord == '|' && %w[l r].include?(direction)
        next_lights.append([light[0], 'u']) unless visited.key?([light[0], 'u'])
        next_lights.append([light[0], 'd']) unless visited.key?([light[0], 'd'])
      end
      if coord == '-' && %w[u d].include?(direction)
        next_lights.append([light[0], 'l']) unless visited.key?([light[0], 'l'])
        next_lights.append([light[0], 'r']) unless visited.key?([light[0], 'r'])
      end

      visited[[light[0], direction]] = '#'
    end
    lights = next_lights
  end

  visited.keys.map(&:first).uniq.count
end

def out_of_bounds(grid, light)
  light[0].negative? || light[0] >= grid.length || light[1].negative? || light[1] >= grid[0].length
end

def continue_line(coord, direction)
  (coord == '-' && %w[l r].include?(direction)) || (coord == '|' && %w[u d].include?(direction))
end

def print_grid(grid, visited)
  visited.each_key do |key|
    coord = key[0]
    grid[coord[0]][coord[1]] = '#'
  end
  pp grid
end

def next_coord(current, direction)
  current[1] += 1 if direction == 'r'
  current[1] -= 1 if direction == 'l'
  current[0] += 1 if direction == 'd'
  current[0] -= 1 if direction == 'u'
  current
end

puts "Part 1/2: #{program(input_array)}"
