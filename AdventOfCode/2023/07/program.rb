# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  hands = []
  bids = {}

  array.each do |hand|
    cards, bid = hand.split(' ')
    card_count = cards.split('')
    hands.append(card_count)
    bids[card_count] = bid.to_i
  end
  p1 = hands.sort { |a, b| compare_hands(a, b) }.each_with_index.map { |cards, i| (i + 1) * bids[cards] }.reduce(:+)
  p2 = hands.sort { |a, b| compare_hands(a, b, true) }.each_with_index.map { |cards, i| (i + 1) * bids[cards] }.reduce(:+)
  [p1, p2]
end

def compare_hands(hand1, hand2, use_jokers = false)
  h1 = define_hand(hand1, use_jokers)
  h2 = define_hand(hand2, use_jokers)

  hand_ranks = [
    'five-of-a-kind',
    'four-of-a-kind',
    'full-house',
    'three-of-a-kind',
    'two-pair',
    'pair',
    'high-card'
  ]
  return 1 if hand_ranks.index(h1) < hand_ranks.index(h2)
  return 0 if hand_ranks.index(h1) > hand_ranks.index(h2)

  # else compare index of the highest card returned
  rank = %w[A K Q J T 9 8 7 6 5 4 3 2]
  rank = %w[A K Q T 9 8 7 6 5 4 3 2 J] if use_jokers
  (0..hand1.count - 1).to_a.each do |idx|
    next if rank.index(hand1[idx]) == rank.index(hand2[idx])

    return 1 if rank.index(hand1[idx]) < rank.index(hand2[idx])

    return 0
  end
end

def define_hand(hand, use_jokers)
  count = hand.tally
  joker_count = hand.count('J')
  if use_jokers && joker_count.positive?
    new_hand = hand - ['J']
    new_count = new_hand.tally

    return 'five-of-a-kind' if new_count == {}
    return 'five-of-a-kind' if new_count.values.max == 4 && joker_count == 1
    return 'five-of-a-kind' if new_count.values.max == 3 && joker_count == 2
    return 'five-of-a-kind' if new_count.values.max == 1 && joker_count == 4
    return 'five-of-a-kind' if new_count.values.max == 2 && joker_count == 3
    return 'four-of-a-kind' if new_count.values.max == 3 && joker_count == 1
    return 'four-of-a-kind' if new_count.values.max == 2 && joker_count == 2
    return 'four-of-a-kind' if new_count.values.max == 1 && joker_count == 3
    return 'full-house' if new_count.values.count(2) == 2 && joker_count == 1
    return 'three-of-a-kind' if new_count.values.max == 2 && joker_count == 1
    return 'three-of-a-kind' if new_count.values.max == 1 && joker_count == 2
    return 'pair' if new_count.values.max == 1 && joker_count == 1
  end

  return 'five-of-a-kind' if count.values.max == 5
  return 'four-of-a-kind' if count.values.max == 4
  return 'two-pair' if count.values.count(2) == 2
  return 'full-house' if count.values.sort == [2, 3]
  return 'three-of-a-kind' if count.values.max == 3
  return 'pair' if count.values.max == 2
  return 'high-card' if count.values.max == 1

  nil
end

puts "Part 1/2: #{program(input_array)}"
