# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '32T3K 765',
      'T55J5 684',
      'KK677 28',
      'KTJJT 220',
      'QQQJA 483'
    ]
  end

  def test_code
    assert_equal [6440, 5905], program(@raw_input)
    assert_equal 'pair', define_hand('32T3K'.split(''), false)
    assert_equal 'two-pair', define_hand('KK677'.split(''), false)
    assert_equal 'two-pair', define_hand('KTJJT'.split(''), false)
    assert_equal 'three-of-a-kind', define_hand('T55J5'.split(''), false)
    assert_equal 'three-of-a-kind', define_hand('QQQJA'.split(''), false)
    assert_equal 'pair', define_hand('32T3K'.split(''), true)
    assert_equal 'two-pair', define_hand('KK677'.split(''), true)
    assert_equal 'four-of-a-kind', define_hand('T55J5'.split(''), true)
    assert_equal 'four-of-a-kind', define_hand('KTJJT'.split(''), true)
    assert_equal 'four-of-a-kind', define_hand('QQQJA'.split(''), true)
    assert_equal 'full-house', define_hand('T3T3J'.split(''), true)
    assert_equal 'three-of-a-kind', define_hand(["5", "4", "J", "A", "5"], true)
    assert_equal 'four-of-a-kind', define_hand(["J", "J", "J", "A", "7"], true)
    assert_equal 'five-of-a-kind', define_hand('2JJJJ'.split(''), true)
    assert_equal 1, compare_hands('Q2KJJ'.split(''), 'KK677'.split(''), true)
  end
end
