# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  directions = array[0].split('')
  coordinates = array[2..]
  map = {}
  coordinates.each do |coordinate|
    start, options = coordinate.split(' = ')
    options.gsub!('(', '').gsub!(')', '')
    map[start] = options.split(', ')
  end

  current = 'AAA'
  p1 = 0
  100_000.times do |idx|
    options = map[current]
    current = directions[idx % directions.length] == 'L' ? options[0] : options[1]
    next unless current == 'ZZZ'

    p1 = idx + 1
    break
  end

  results = []
  nodes = map.keys.select { |k| k[-1] == 'A' }
  100_000.times do |idx|
    next_nodes = []
    nodes.each do |node|
      options = map[node]
      next_node = directions[idx % directions.length] == 'L' ? options[0] : options[1]
      if next_node[-1] == 'Z'
        results.append(idx + 1)
      else
        next_nodes.append(next_node)
      end
    end

    nodes = next_nodes
    break if nodes.empty?
  end

  [p1, results.reduce(:lcm)]
end

puts "Part 1/2: #{program(input_array)}"
