# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @input = [
      'LR',
      '',
      'AAA = (11B, XXX)',
      '11B = (XXX, ZZZ)',
      'ZZZ = (11B, XXX)',
      '22A = (22B, XXX)',
      '22B = (22C, 22C)',
      '22C = (22Z, 22Z)',
      '22Z = (22B, 22B)',
      'XXX = (XXX, XXX)'
    ]
  end

  def test_code
    assert_equal [2, 6], program(@input)
  end
end
