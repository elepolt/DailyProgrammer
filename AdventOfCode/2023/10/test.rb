# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '.....',
      '.S-7.',
      '.|.|.',
      '.L-J.',
      '.....'
    ]

    @raw_input2 = [
      '..F7.',
      '.FJ|F',
      'SJ.L7',
      '|F--J',
      'LJ...'
    ]
  end

  def test_code
    assert_equal [4, 1], program(@raw_input)
    assert_equal [8, 1], program(@raw_input2)
  end

  def test_can_move_here
    map = []
    starting_coords = []
    @raw_input2.each_with_index do |line, idx|
      map[idx] = line.split('')
      starting_coords = [idx, map[idx].find_index('S')] if !map[idx].find_index('S').nil?
    end

    coord = next_move(map, [2, 1], [2, 0])
    assert_equal [1, 1], coord
    coord = next_move(map, coord, [2, 1])
    assert_equal [1, 2], coord
    coord = next_move(map, coord, [1, 1])
    assert_equal [0, 2], coord
    coord = next_move(map, coord, [1, 2])
    assert_equal [0, 3], coord
    coord = next_move(map, coord, [0, 2])
    assert_equal [1, 3], coord
    coord = next_move(map, coord, [0, 3])
    assert_equal [2, 3], coord
  end
end
