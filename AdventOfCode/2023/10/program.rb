# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  map = []
  starting_coords = []
  array.each_with_index do |line, idx|
    map[idx] = line.split('')
    starting_coords = [idx, map[idx].find_index('S')] unless map[idx].find_index('S').nil?
  end

  path = [starting_coords]
  coord = starting_coords
  # Get the first possible starting point:
  neighbors = traverse(map, coord, 0, false)
  neighbors.each do |neighbor|
    next if map[neighbor[0]][neighbor[1]] == '.'
    next unless can_move_here(map, coord, neighbor)

    coord = neighbor
    break
  end

  (map.length * map[0].length).times do
    from = path.last
    path.append(coord)
    coord = next_move(map, coord, from)
    break if current_pipe(map, coord) == 'S'
  end

  p1 = (path.length + 1) / 2
  area = 0
  # Shoelace Forumula to get area inside our polygon
  path.each_cons(2) do |p|
    area += (p[0][0] + p[1][0]) * (p[0][1] - p[1][1])
  end
  area *= 0.5
  # Then using Pick's Theorem where we know area, B/2 is our Part1 solution, then solve for I
  # I + B/2 − 1 = Area
  interior_points = area - p1 + 1

  [p1, interior_points.to_i]
end

def current_pipe(map, coord)
  map[coord[0]][coord[1]]
end

def next_move(map, coord, from_coord)
  direction_x = coord[0] - from_coord[0]
  direction_y = coord[1] - from_coord[1]
  direction = [[1, 0], [0, 1], [0, -1], [-1, 0]].find_index([direction_x, direction_y])
  case current_pipe(map, coord)
  when '7'
    coord = direction == 1 ? [coord[0] + 1, coord[1]] : [coord[0], coord[1] - 1]
  when 'L'
    coord = direction == 2 ? [coord[0] - 1, coord[1]] : [coord[0], coord[1] + 1]
  when 'J'
    coord = direction == 1 ? [coord[0] - 1, coord[1]] : [coord[0], coord[1] - 1]
  when '|'
    coord = direction.zero? ? [coord[0] + 1, coord[1]] : [coord[0] - 1, coord[1]]
  when '-'
    coord = direction == 1 ? [coord[0], coord[1] + 1] : [coord[0], coord[1] - 1]
  when 'F'
    coord = direction == 2 ? [coord[0] + 1, coord[1]] : [coord[0], coord[1] + 1]
  end
  coord
end

def can_move_here(map, current_coord, coord)
  direction_x = coord[0] - current_coord[0]
  direction_y = coord[1] - current_coord[1]
  direction = [[-1, 0], [0, -1], [0, 1], [1, 0]].find_index([direction_x, direction_y])

  above_pipes = %w[| 7 F]
  left_pipes = %w[- L F]
  right_pipes = %w[- 7 J]
  below_pipes = %w[| L J]
  possibilities = [above_pipes, left_pipes, right_pipes, below_pipes]

  possibilities[direction].include?(map[coord[0]][coord[1]])
end

puts "Part 1/2: #{program(input_array)}"
