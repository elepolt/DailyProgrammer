# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '2413432311323',
      '3215453535623',
      '3255245654254',
      '3446585845452',
      '4546657867536',
      '1438598798454',
      '4457876987766',
      '3637877979653',
      '4654967986887',
      '4564679986453',
      '1224686865563',
      '2546548887735',
      '4322674655533'
    ]

    @mine = [
      '14999',
      '23111',
      '99991'
    ]
  end

  def test_code
    # assert_equal [102, nil], program(@raw_input)
    assert_equal [102, nil], program(@mine)
  end
end
