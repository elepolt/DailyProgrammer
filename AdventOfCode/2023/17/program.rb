# frozen_string_literal: true

require 'pqueue'
require_relative '../../base'

def program(array)
  grid = []
  array.each do |line|
    grid.append(line.chars.map(&:to_i))
  end

  [dijkstra(grid), nil]
end

def dijkstra(grid)
  starting_node = [0, 0]
  ending_coords = [grid.length - 1, grid[0].length - 1]
  unvisited_nodes = PQueue.new
  unvisited_nodes.push([0, starting_node])

  dijkstra_values = {}
  dijkstra_values[starting_node] = {
    'value': 0,
    'previous_vertex': nil
  }

  until unvisited_nodes.empty?
    risk, current_node = unvisited_nodes.shift

    # byebug if dijkstra_values.count > 10
    # byebug if pns.count > 2

    current_dijkstra_value = dijkstra_values[current_node][:value]

    neighbors = traverse(grid, current_node, 2, false)
    neighbors.each do |coord|
      # Cannot move backwards
      next if coord == dijkstra_values[current_node][:previous_vertex]

      # If the current_node is on the same x/y of the previous three nodes we have to do something else
      pns = [coord, current_node] + previous_nodes(dijkstra_values, current_node, 2)

      next if pns.compact.count == 4 && pns.each_cons(2).map { |k| direction_moved(k[0], k[1]) }.uniq.count == 1

      # If this is new, initialize it and push it into our unvisited queue
      unless dijkstra_values.key?(coord)
        dijkstra_values[coord] = {
          'value': 99_999
        }
      end

      # Then compare the current values to it
      grid_value = grid[coord[0]][coord[1]]
      value = grid_value + current_dijkstra_value

      if value < dijkstra_values[coord][:value]
        dijkstra_values[coord][:value] = value
        dijkstra_values[coord][:previous_vertex] = current_node
        unvisited_nodes.push([value, coord])
      end
    end
  end

  # If you want to see the path uncomment
  coord = ending_coords
  while coord != [0, 0]
    # pp coord
    coord = dijkstra_values[coord][:previous_vertex]
    grid[coord[0]][coord[1]] = 'X'
  end
  pp grid

  dijkstra_values[ending_coords][:value]
end

def previous_nodes(dijkstra_values, coord, count)
  pn = []
  count.times do
    # pp coord
    next if dijkstra_values[coord].nil?
    next if dijkstra_values[coord][:previous_vertex].nil?

    coord = dijkstra_values[coord][:previous_vertex]
    pn.append(coord)
  end

  pn
end

def direction_moved(c1, c2)
  return 'v' if c1[0] != c2[0]
  # return 'h' if c1[1] != c2[1]
  'h'
end

# input_array = read_lines('input.txt')
# puts "Part 1/2: #{program(input_array)}"
