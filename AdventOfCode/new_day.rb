require 'date'
require 'net/http'
require 'uri'
require 'fileutils'
# require 'byebug'

if ARGV.empty? || ARGV.length > 1
  puts 'Just enter the day, ya dolt'
  return
end

this_year = DateTime.now.year
day = ARGV[0].to_i

# uri = URI.parse("https://adventofcode.com/#{this_year}/day/#{day}/input")
# request = Net::HTTP::Get.new(uri)
# session_cookie = File.read('.session')
# request['Cookie'] = "session=#{session_cookie}"

# req_options = {
#   use_ssl: uri.scheme == 'https'
# }

# response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
#   http.request(request)
# end
# puts response.body

file_path_day = day > 9 ? day.to_s : "0#{day}"
file_path = "#{this_year}/#{file_path_day}/"

FileUtils.mkdir_p(file_path)
FileUtils.cp('empty/program.rb', file_path)
FileUtils.cp('empty/test.rb', file_path)

# input_file = "#{file_path}/input.txt"
# File.open(input_file, 'w') { |file| file.write(response.body) }
