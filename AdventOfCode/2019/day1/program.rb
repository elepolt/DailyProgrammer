
def mass_calc(mass)
  # Do this no matter what
  new_mass = (mass/3).floor()
  new_mass -= 2

  # If the new calc is still divisible by 3, do it again
  if mass % 3 == 0
    mass_calc(new_mass)
  else
    return new_mass
  end

  new_mass
end

def mass_calc_part_two(mass)
  fuel_mass = mass_calc(mass)
  temp_calc = (mass/3).floor() - 2
  while temp_calc > 0
    # The last temp calc will be negative, so don't add that
    temp_calc = (temp_calc/3).floor() - 2
    if temp_calc > 0
      fuel_mass += temp_calc
    end
  end
  
  fuel_mass
end

def fuel
  fuel_amount = 0
  File.readlines('inputDay1.txt').each do |line|
    fuel_amount += mass_calc(line.to_i)
  end

  puts fuel_amount
end

def fuel_part_two
  fuel_amount = 0
  File.readlines('inputDay1.txt').each do |line|
    fuel_amount += mass_calc_part_two(line.to_i)
  end

  puts fuel_amount
end

# fuel
fuel_part_two