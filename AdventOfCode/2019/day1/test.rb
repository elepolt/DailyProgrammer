require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  def test_zero
    assert_equal 2, mass_calc(12)
  end

  def test_easy
    assert_equal 2, mass_calc(14)
  end

  def test_one
    assert_equal 654, mass_calc(1969)
  end

  def test_one_part_two
    assert_equal 966, mass_calc_part_two(1969)
  end

  def test_two
    assert_equal 33583, mass_calc(100756)
  end

  def test_two_part_two
    assert_equal 50346, mass_calc_part_two(100756)
  end
end
