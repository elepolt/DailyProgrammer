# 1 adds together numbers read from two positions and stores the result in a third position.
# 2 multiplies together numbers read from two positions and stores the result in a third position.
# 99 halts
def run_computer(intcode_array)
  index = 0
  intcode_block = intcode_array[index..index+3]

  while index < intcode_array.length - 1
    command = intcode_block[0]
    # if the command is 99, the program is done and return the first value of the array
    if command == 99
      # if intcode_block.length > 1 return intcode_block[1]?
      return intcode_array[0]
    # Else do some math!
    else
      # The two values that will be mathed
      index_1 = intcode_block[1]
      index_2 = intcode_block[2]
      # This is where the value will be stored
      index_3 = intcode_block[3]
      if command == 1
        value = intcode_array[index_1] + intcode_array[index_2]
      elsif command == 2
        value = intcode_array[index_1] * intcode_array[index_2]
        # else throw an error?
      end
      intcode_array[index_3] = value

      # Set index to the next opcode position
      index += 4
      # And get the next block of codes
      intcode_block = intcode_array[index..index+3]
    end
  end
  return intcode_array[0]
end

def split_intcode_string(intcode_string)
  intcode_string.split(',').map(&:to_i)
end

def program
  intcode_array = split_intcode_string(File.read('input.txt'))
  # Part 1
  # before running the program, replace position 1 with the value 12 and replace position 2 with the value 2
  # intcode_array[1] = 12
  # intcode_array[2] = 2
  # result = run_computer(intcode_array)
  # puts result # 4714701

  # Part two makes us want to change noun/verb to produce 19690720
  # Brute Force!
  (1..100).each do |noun|
    (1..100).each do |verb|
      intcode_array[1] = noun
      intcode_array[2] = verb
      
      # Apparently ruby does scope weird? Create a new array to pass in to the program
      result = run_computer([].replace(intcode_array))
      # puts result
      if result == 19690720
        puts 100*noun + verb
        break
      end
    end
  end
  # puts result
end

program