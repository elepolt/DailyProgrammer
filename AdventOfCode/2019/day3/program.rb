# What I'm thinking here is that I want to keep track of every coordinate the wires hit
# Then when I have both of them, I can compare the coordinates of each wire for duplicates
require 'byebug'
require 'set'

def get_points(wire)
  coords = Set[]
  current_coord = [0,0]

  wire.each do |coord|
    direction = coord[0]
    distance = coord[1..coord.length-1].to_i
    (1..distance).each_with_index do |step, idx|
      temp_coord = current_coord.dup()
      temp_coord[0] = temp_coord[0]+1 if direction == 'U'
      temp_coord[0] = temp_coord[0]-1 if direction == 'D'
      temp_coord[1] = temp_coord[1]+1 if direction == 'R'
      temp_coord[1] = temp_coord[1]-1 if direction == 'L'
      coords.add(temp_coord)
      current_coord = temp_coord
    end
  end

  coords
end

def get_points_array(wire)
  coords = []
  current_coord = [0,0]

  wire.each do |coord|
    direction = coord[0]
    distance = coord[1..coord.length-1].to_i
    (1..distance).each_with_index do |step, idx|
      temp_coord = current_coord.dup()
      temp_coord[0] = temp_coord[0]+1 if direction == 'U'
      temp_coord[0] = temp_coord[0]-1 if direction == 'D'
      temp_coord[1] = temp_coord[1]+1 if direction == 'R'
      temp_coord[1] = temp_coord[1]-1 if direction == 'L'
      coords.push(temp_coord)
      current_coord = temp_coord
    end
  end

  coords
end

def track_wires(wire_one, wire_two)
  coords_one = get_points(wire_one)
  coords_two = get_points(wire_two)

  distance_away = 0
  crossings = (coords_one & coords_two).to_a - [[0,0]]
  crossings.each do |cross|
    temp_distance = cross[0].abs() + cross[1].abs()
    if distance_away == 0
      distance_away = temp_distance
    elsif temp_distance < distance_away
      distance_away = temp_distance
    end
  end

  return distance_away
end

def track_wires_pt2(wire_one, wire_two)
  coords_one = get_points_array(wire_one)
  coords_two = get_points_array(wire_two)

  total_steps = 0
  crossings = (coords_one.to_set & coords_two.to_set).to_a - [[0,0]]
  crossings.each do |cross|
    coords_one_steps = coords_one.find_index(cross) + 1
    coords_two_steps = coords_two.find_index(cross) + 1
    temp_steps = coords_one_steps + coords_two_steps
    if total_steps == 0
      total_steps = temp_steps
    elsif temp_steps < total_steps
      total_steps = temp_steps
    end
  end
  
  return total_steps
end

def split_string(string)
  string.split(',')
end

def program
  wire_one = split_string(File.readlines('input.txt')[0])
  wire_two = split_string(File.readlines('input.txt')[1])
  result = track_wires_pt2(wire_one, wire_two)
  puts result
end

program