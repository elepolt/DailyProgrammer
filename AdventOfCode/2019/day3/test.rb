require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  # Part 1 tests
  # def test_zero
  #   wire_one = split_string('R75,D30,R83,U83,L12,D49,R71,U7,L72')
  #   wire_two = split_string('U62,R66,U55,R34,D71,R55,D58,R83')
  #   assert_equal 159, track_wires(wire_one, wire_two)
  # end

  # def test_one
  #   wire_one = split_string('R2,U2')
  #   wire_two = split_string('U2,R2')
  #   assert_equal 4, track_wires(wire_one, wire_two)
  # end

  # def test_two
  #   wire_one = split_string('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51')
  #   wire_two = split_string('U98,R91,D20,R16,D67,R40,U7,R15,U6,R7')
  #   assert_equal 135, track_wires(wire_one, wire_two)
  # end

  # part 2 tests
  def test_zero_2
    wire_one = split_string('R75,D30,R83,U83,L12,D49,R71,U7,L72')
    wire_two = split_string('U62,R66,U55,R34,D71,R55,D58,R83')
    assert_equal 610, track_wires_pt2(wire_one, wire_two)
  end

  def test_one_2
    wire_one = split_string('R2,U2')
    wire_two = split_string('U2,R2')
    assert_equal 8, track_wires_pt2(wire_one, wire_two)
  end

  def test_two
    wire_one = split_string('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51')
    wire_two = split_string('U98,R91,D20,R16,D67,R40,U7,R15,U6,R7')
    assert_equal 410, track_wires_pt2(wire_one, wire_two)
  end

end
