require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  def test_zero
    assert_equal 2, run_computer(split_intcode_string('1,0,0,0,99'), 1)
  end

  def test_easy
    # The examples given on AoC do not make sense because you're supposed to return the first
    # value of the array at the end, and they do not
    assert_equal 2, run_computer(split_intcode_string('2,3,0,3,99'), 1)
  end

  def test_one
    # The examples given on AoC do not make sense because you're supposed to return the first
    # value of the array at the end, and they do not
    assert_equal 2, run_computer(split_intcode_string('2,4,4,5,99,0'), 1)
  end

  def test_illustration
    assert_equal 3500, run_computer(split_intcode_string('1,9,10,3,2,3,11,0,99,30,40,50'), 1)
  end

  def test_input
    assert_equal 1, run_computer(split_intcode_string('3,0,4,0,99'), 1)
  end

  # def test_one_part_two
  #   assert_equal 966, run_computer_part_two(split_intcode_string(''), 1)
  # end

  def test_two
    assert_equal 30, run_computer(split_intcode_string('1,1,1,4,99,5,6,0,99'), 1)
  end

  # def test_two_part_two
  #   assert_equal 50346, run_computer_part_two(split_intcode_string(''), 1)
  # end
end
