# 1 adds together numbers read from two positions and stores the result in a third position.
# 2 multiplies together numbers read from two positions and stores the result in a third position.
# 99 halts
require 'byebug'
def run_computer(intcode_array, input)
  index = 0
  # intcode_block is now 2 or 4 depending on the command, so start with that
  command = intcode_array[0]
  intcode_block = intcode_array[index..index+3]
  if command == 3 || command == 4
    intcode_block = intcode_array[index..index+1]
  end

  while index < intcode_array.length - 1
    command = intcode_block[0]
    # The two values that will be mathed
    index_1 = intcode_block[1]
    index_2 = intcode_block[2]
    # This is where the value will be stored
    index_3 = intcode_block[3]
    # TODO - EDL: Command now needs to be be broken up
    # We essentially need to add zeroes to make the digit count equal 5
    # So a 1 is really 00001
    # 1002,4,3,4 would mean multiply postion 4 by value 3, and store it in positiion 4
    if command == 1
      value = intcode_array[index_1] + intcode_array[index_2]
      intcode_array[index_3] = value
    elsif command == 2
      value = intcode_array[index_1] * intcode_array[index_2]
      intcode_array[index_3] = value
    elsif command == 3
      intcode_array[index_1] = input
    # elsif command == 4
      #   something else
    elsif command == 99
      return intcode_array[0]
    end

    # Set index to the next opcode position
    index += intcode_block.length
    # And get the next block of codes, also depending on the current intcode block length
    intcode_block = intcode_array[index..index+intcode_block.length+1]
  end

  return intcode_array[0]
end

def split_intcode_string(intcode_string)
  intcode_string.split(',').map(&:to_i)
end

def program
  intcode_array = split_intcode_string(File.read('input.txt'))
  # Update the first code due to my puzzle input
  input = 1
  # Part 1
  # before running the program, replace position 1 with the value 12 and replace position 2 with the value 2
  # intcode_array[1] = 12
  # intcode_array[2] = 2
  result = run_computer(intcode_array, input)
  # puts result
end

program