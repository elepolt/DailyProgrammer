require "minitest/autorun"
require_relative "program"
class TestProgram < Minitest::Test
  # 111111 meets these criteria (double 11, never decreases).
  # 223450 does not meet these criteria (decreasing pair of digits 50).
  # 123789 does not meet these criteria (no double).
  # part 2 tests
  def test_zero_2
    assert_equal false, clever(111111)
  end

  def test_one_2
    assert_equal false, clever(223450)
  end

  def test_two
    assert_equal false, clever(123789)
  end

  def test_two_
    assert_equal false, clever(711119)
  end

  def test_dup
    assert_equal true, clever(111122)
  end
  
end
