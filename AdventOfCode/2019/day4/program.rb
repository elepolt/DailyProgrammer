require 'byebug'

# Clever way. each number set to an array, reverse it, pop the number for comparison
def clever(num_input)
  is_valid = true
  decreases = true
  # lolz, gotta be a better way to get an array here
  digits = num_input.to_s.chars.map(&:to_i)
  previous = digits[0]

  # Part two requires the duplicate to be exactly a pair, no triple+
  # So I ended up creating a dictionary that will keep track of how many occurences
  # in a row the number happens. So if the key == 1, it means that it was duplicated 1 time
  # meaning it is a pair
  duplicate_count = {}
  duplicate_num = -1

  # shift digits backwards to make comparisons
  digits[1..digits.length].each_with_index do |digit, idx|
    previous = digits[idx]
    current = digit

    # Contains duplicate
    if current == previous
      # If the duplicate_num is current, add an occurence in the count
      if current == duplicate_num
        duplicate_count[current] += 1
      else
        # Else initialize the count to 1
        duplicate_num = current
        duplicate_count[current] = 1
      end
    end

    # Just return if the password does not decrease
    if current < previous
      return false
    end

    previous = current
  end

  # Check that the duplicate count contains one exact pair
  duplicate_count.any? { |d, k| k == 1 }
end

def program
  count = 0
  (168630..718098).each do |num_input|
    count += 1 if clever(num_input)
  end
  puts count
end

# program