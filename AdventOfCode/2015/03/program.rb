require_relative '../../base.rb'

input_array = read_lines('input')[0]

def program(input_array)
  all_homes = []
  current_x = 0
  current_y = 0
  all_homes.push([current_x, current_y])
  split_string(input_array, '').each do |direction|
    current_x += 1 if direction == '^'
    current_x -= 1 if direction == 'v'
    current_y += 1 if direction == '>'
    current_y -= 1 if direction == '<'
    all_homes.push([current_x, current_y])
  end
  uniq_homes = all_homes.uniq.count
  [uniq_homes, nil]
end

def part_two(input_array)
  # ^v delivers presents to 3 houses, because Santa goes north, and then Robo-Santa goes south.
  # ^>v< now delivers presents to 3 houses, and Santa and Robo-Santa end up back where they started.
  # ^v^v^v^v^v now delivers presents to 11 houses, with Santa going one direction and Robo-Santa going the other.
  all_homes = []
  santa_x = 0
  santa_y = 0
  robot_x = 0
  robot_y = 0
  all_homes.push([santa_x, santa_y])
  split_string(input_array, '').each_slice(2) do |direction|
    santa_x += 1 if direction[0] == '^'
    santa_x -= 1 if direction[0] == 'v'
    santa_y += 1 if direction[0] == '>'
    santa_y -= 1 if direction[0] == '<'
    all_homes.push([santa_x, santa_y])

    robot_x += 1 if direction[1] == '^'
    robot_x -= 1 if direction[1] == 'v'
    robot_y += 1 if direction[1] == '>'
    robot_y -= 1 if direction[1] == '<'
    all_homes.push([robot_x, robot_y])
  end
  uniq_homes = all_homes.uniq.count
  uniq_homes
end
# puts program(input_array)
puts part_two(input_array)

require "minitest/autorun"

class TestProgram < Minitest::Test
  def setup
    # 
  end

  def test_program

    # delivers presents to 2 houses: one at the starting location, and one to the east.
    assert_equal [2, nil], program('>')
    # delivers presents to 4 houses in a square, including twice to the house at his starting/ending location.
    assert_equal [4, nil], program('^>v<')
    # delivers a bunch of presents to some very lucky children at only 2 houses.
    assert_equal [2, nil], program('^v^v^v^v^v')

    # delivers presents to 3 houses, because Santa goes north, and then Robo-Santa goes south.
    assert_equal 3, part_two('^v')
    # now delivers presents to 3 houses, and Santa and Robo-Santa end up back where they started.
    assert_equal 3, part_two('^>v<')
    # ^v^v^v^v^v
    # assert_equal [58, 34], program(['2x3x4'])
    # assert_equal [43, 14], program(['1x1x10'])
    # assert_equal [(58+43), (34+14)], program(['2x3x4','1x1x10'])
  end
end
