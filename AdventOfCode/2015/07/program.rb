require_relative '../../base.rb'

input_array = read_lines('input')

def program(input_array)
  circuit = run_instructions({}, input_array)


  circuit
end

def run_instructions(circuit, instructions)
  next_instructions = []

  instructions.each do |instruction|
    # byebug if 
    value, key = parse_instruction(circuit, instruction)
    if value.nil?
      next_instructions.push(instruction)
    else
      circuit[key] = value
    end
  end

  # byebug if circuit['b'] && circuit['b'] != 14146
  run_instructions(circuit, next_instructions) unless next_instructions.empty?

  circuit
end

def parse_instruction(circuit, instruction)
  # byebug if instruction == '1 AND r -> s'
  # parse -> first to get output
  command_string, output_key = instruction.split(' -> ')

  # if the command_string is just a number send it back
  return [command_string.to_i, output_key] if command_string.is_integer?

  command_array = command_string.split(' ')

  # If the command is `NOT var`, we want to return nil iff we don't have `var` in the circuit
  return nil if circuit[command_array.last].nil? && command_array.first == 'NOT'

  # if [0] is 'NOT'
  return [twos_complement(circuit[command_array.last]), output_key] if command_array.first == 'NOT'

  output_value = 0
  value_1, command, value_2 = command_array

  # value_1 can SOMETIMES be an integer, so if we don't have the value
  if circuit[value_1].nil?
    # return nil if it is NOT an integer
    return nil unless value_1.is_integer?
    value_1 = value_1.to_i
  else
    value_1 = circuit[value_1]
  end

  # byebug if output_key == 'a'
  case command
  when 'AND'
    return nil if circuit[value_2].nil?
    output_value = (value_1 & circuit[value_2])
  when 'OR'
    return nil if circuit[value_2].nil?
    output_value = (value_1 | circuit[value_2])
  when 'LSHIFT'
    output_value = (value_1 << value_2.to_i)
  when 'RSHIFT'
    output_value = (value_1 >> value_2.to_i)
  when nil
    output_value = value_1
  end
  # parse on list of accepted verbs
  #  %w(AND OR LSHIFT RSHIFT)
  # NOT

  return [output_value, output_key]
end

def twos_complement(input)
  input^65535
end

circuit = program(input_array)
puts 'a', circuit['a']
puts 'b', circuit['b']

require "minitest/autorun"

class TestProgram < Minitest::Test
  def setup
    # 
    @instructions = [
      'x AND y -> d',
      '123 -> x',
      '456 -> y',
      'x OR y -> e',
      'x LSHIFT 2 -> f',
      'y RSHIFT 2 -> g',
      'NOT x -> h',
      'NOT y -> i',
    ]
  end

  def test_program
    output = {
      'd' => 72,
      'e' => 507,
      'f' => 492,
      'g' => 114,
      'h' => 65412,
      'i' => 65079,
      'x' => 123,
      'y' => 456
    }
    assert_equal output, program(@instructions)
    assert_equal [123, 'x'], parse_instruction({}, '123 -> x')
    assert_equal [72, 'd'], parse_instruction({'x' => 123, 'y' => 456}, 'x AND y -> d')
    
  end
end

# {"d"=>72, "e"=>507, "f"=>492, "g"=>114, "h"=>65412, "i"=>65079, "x"=>123, "y"=>456}
# {"d"=>72, "e"=>507, "f"=>492, "g"=>114, "x"=>123, "y"=>456, "h"=>65535, "i"=>65535}