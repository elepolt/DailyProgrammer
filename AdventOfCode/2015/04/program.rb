require_relative '../../base.rb'
require 'digest'

input_string = 'yzbqklnj' # read_lines('input')[0]

# TODO: Refactor a bit so that parts 1 and 2 can be run at the same time
def program(secret_key)
  blah = []
  # byebug
  # 00001
  (0..11609043).each do |idx|
    key_with_leading_zeros = secret_key + prepend_zeroes(idx)
    md5 = Digest::MD5.hexdigest(key_with_leading_zeros)
    return idx if md5[0..4].split('').uniq == ['0']
    # blah.push()
  end
end

def prepend_zeroes(input)
  with_leading_zeros = 
  leading_zero_count = 6-input.to_s.length
  return input.to_s if input.to_s.length >= 5
  '0'*leading_zero_count + input.to_s
end


puts program(input_string)

require "minitest/autorun"

class TestProgram < Minitest::Test
  def setup
    # 
  end

  def test_program
    assert_equal('000001', prepend_zeroes(1))
    assert_equal('000011', prepend_zeroes(11))
    # assert_equal(609043, program('abcdef'))
  end
end
