require_relative '../../base.rb'

input_array = read_lines('input')

def program(input_array)
  board = []
  1000.times do
    board.push(Array.new(1000, 0))
  end

  input_array.each do |command_string|
    # part 1
    command, lights = command_details(command_string)
    starting_lights, final_lights = lights
    s_x, s_y = starting_lights
    f_x, f_y = final_lights
    (s_x..f_x).each do |idx|
      (s_y..f_y).each do |idy|
        # Part 1
        board[idx][idy] = 0 if command == 'off'
        board[idx][idy] = 1 if command == 'on'
        board[idx][idy] = board[idx][idy] == 0 ? 1 : 0 if command == 'toggle'

        # Part 2
        # board[idx][idy] -= 1 if command == 'off' && board[idx][idy] > 0
        # board[idx][idy] += 1 if command == 'on'
        # board[idx][idy] += 2 if command == 'toggle'
      end
    end
  end

  # Part 1
  board.flatten.count(1)
  # Part 2
  # board.flatten.sum
end

def command_details(command_string)
  command_array = command_string.split(' ')
  command = command_array.shift
  command = command_array.shift unless command == 'toggle'
  lights = command_array.values_at(0,-1).map{|xy| xy.split(',').map(&:to_i)}
  [command, lights]
end

puts program(input_array)

require "minitest/autorun"

class TestProgram < Minitest::Test
  def setup
    # 
  end

  def test_program
    assert_equal ['on', [[887, 9], [959, 629]]], command_details('turn on 887,9 through 959,629')
    assert_equal ['toggle', [[294, 259], [474, 326]]], command_details('toggle 294,259 through 474,326')
  end
end
