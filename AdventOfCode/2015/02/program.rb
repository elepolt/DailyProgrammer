require_relative '../../base.rb'

input_array = read_lines('input')

def program(input_array)
  total_square_feet = 0
  total_bow = 0

  input_array.each do |dimension_string|
    dimensions = split_string(dimension_string, 'x').map(&:to_i)
    dimension_combinations_array = dimensions.combination(2).to_a

    # Part 1
    dimension_combinations = dimension_combinations_array.map { |a| a.inject(:*) }
    # byebug
    total_square_feet += dimension_combinations.map {|a| a*2}.sum
    total_square_feet += dimension_combinations.min
    # dimension_combinations.each { |a| total_square_feet += a.inject(:*) }

    # part 2
    dimensions = dimensions.sort.reverse!
    total_bow += dimensions.inject(:*)
    2.times do
      total_bow += dimensions.pop * 2
    end
  end
  # 1297 too low
  [total_square_feet, total_bow]
end

puts program(input_array)

require "minitest/autorun"
# require_relative "program"

class TestProgram < Minitest::Test
  def setup
    # 
  end

  def test_program
    assert_equal [58, 34], program(['2x3x4'])
    assert_equal [43, 14], program(['1x1x10'])
    assert_equal [(58+43), (34+14)], program(['2x3x4','1x1x10'])
  end
end
