require_relative '../../base.rb'

input_array = read_lines('input')

def program(input_array)
  count = 0
  count_2 = 0

  input_array.each do |string|
    count += 1 if is_naughty_or_nice(string)
    count_2 += 1 if is_naughty_or_nice_updated(string)

    # unless string =~ /([a-z][a-z]).*\1/
    #   puts string
    #   # if string =~ /([a-z])[a-z]\1/
    #   #   puts string
    #   # end
    # end
  end
  [count, count_2]
end


# A nice string is one with all of the following properties:
#     It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
#     It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
#     It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
def is_naughty_or_nice(string)
  bad_strings = ['ab', 'cd', 'pq', 'xy']
  return false unless bad_strings.select { |s| string.include?(s) }.empty?

  string_array = string.split('')
  return false unless has_double_letter(string_array)

  vowels = %w(a e i o u)
  string_tally = string_array.tally
  values = string_tally.select{|k,v| vowels.include?(k)}.values
  return false if values.empty? || values.reduce(:+) < 3

  true
end

def has_double_letter(string_array)
  (0..string_array.length-2).each do |idx|
    return true if string_array[idx] == string_array[idx+1]
  end
  false
end

def is_naughty_or_nice_updated(string)
  string_array = string.split('')
  return false unless has_double_pair(string_array)
  return false unless has_sandwich_letter(string_array)

  true
end

# The big kicker here is that the double pair cannot overlap
# So 'xxx' is bad, but 'xxxx' is good.
def has_double_pair(string_array)
  sliced_array = []
  (0..string_array.length-2).each do |idx|
    first = string_array[idx]
    second = string_array[idx+1]


    # If our current pair matches the last pair, we need to be assured that it's not
    # due to an overlap. I think I can do this by checking our current index against the index two behind,
    # This is because we are always using our current index, and the next number. So if the next number
    # and the one two before that match, then... we.... I don't know. this works though
    # 'hiywwwwnu'
    #                            [2-3, 3-4, 4-5]
    # We start comparing at www, [yw, ww, *check*]. So because 5 and 3 are equal, we know that there are four in a row
    # 'gfzzzdptaktytnqg'
    # Whereas for this one, once we get to our second zz, we compare z to f and realize that it's just three in a row
    # This only works for 4 in a row, I think. If the rules changed for three pairs I'd probably be screwed.
    push_pair = true
    if sliced_array.last == [first, second]
      push_pair = false unless string_array[idx-2] == second
    end

    sliced_array.push([first, second]) if push_pair
  end

  sliced_array.tally.select{|k,v| v > 1}.count > 0
end

def has_sandwich_letter(string_array)
  (0..string_array.length-3).each do |idx|
    return true if string_array[idx] == string_array[idx+2]
  end
  false
end

# part 2 is 51.
answer_1, answer_2 = program(input_array)
puts(answer_1, answer_2)

require "minitest/autorun"

class TestProgram < Minitest::Test
  def setup
    # 
  end

  def test_program

    assert_equal true, is_naughty_or_nice_updated('rxexcbwhiywwwwnu')
    assert_equal false, is_naughty_or_nice_updated('gfzzzdptaktytnqg')

    assert_equal true, is_naughty_or_nice('ugknbfddgicrmopn')
    assert_equal false, is_naughty_or_nice('jchzalrnumimnmhp')
    assert_equal false, is_naughty_or_nice('haegwjzuvuyypxyu')
    assert_equal false, is_naughty_or_nice('dvszwmarrgswjxmb')
    assert_equal true, is_naughty_or_nice_updated('qjhvhtzxzqqjkmpb')
    assert_equal true, is_naughty_or_nice_updated('xxyxx')
    assert_equal true, is_naughty_or_nice_updated('xxxxyx')
    assert_equal false, is_naughty_or_nice_updated('uurcxstgmygtbstg')
    assert_equal false, is_naughty_or_nice_updated('ieodomkazucvgmuy')
    assert_equal false, is_naughty_or_nice_updated('axxxa')
    assert_equal true, is_naughty_or_nice_updated('xxxx')
  end
end
