require_relative '../../base.rb'

input_array = read_lines('input')[0]

current_floor = 0
has_hit_basement = false
input_array.split('').each_with_index do |direction, idx|
  current_floor += 1 if direction == '('
  current_floor -= 1 if direction == ')'
  # Answer 2
  puts idx + 1 if current_floor == -1 && !has_hit_basement
  has_hit_basement = true if current_floor == -1
end

# Answer 1
puts current_floor
