# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'Sabqponm',
      'abcryxxl',
      'accszExk',
      'acctuvwj',
      'abdefghi'
    ]
  end

  def test_code
    assert_equal [31, 29], program(@raw_input)
    assert_equal([2, 5], find_coords(@raw_input, 'E'))
  end
end
