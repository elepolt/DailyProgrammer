# frozen_string_literal: true

require_relative '../../base'
require 'pqueue'

input_array = read_lines('input.txt')
# input_array = read_lines('input_2.txt')

def program(array)
  grid = []
  array.each do |line|
    grid.push(line.split(''))
  end

  ending_coords = find_coords(array, 'E')

  # Ok. Dijkstra. Here we go.
  starting_node = find_coords(array, 'S')

  # part 1
  part_1 = dijkstra(grid, starting_node, ending_coords)
  part_2 = 412

  # lolz, probably a smarter way to do this... but only the left column of the input were valid options,
  # so I'm just running each one through the program
  # I can _probably_ pass in the known values, and check against those.... maybe later
  starting_coords = (0..grid.length - 1).map { |idx| [idx, 0]}
  starting_coords.each_with_index do |coord, idx|
    ans = dijkstra(grid, coord, ending_coords)
    part_2 = ans if ans < part_2
  end


  [part_1, part_2]
end

def dijkstra(grid, starting_node, ending_coords)
  alphabet = ('a'..'z').to_a
  unvisited_nodes = PQueue.new
  unvisited_nodes.push([0, starting_node])

  dijkstra_values = {}
  dijkstra_values[starting_node] = {
    'value': 0,
    'previous_vertex': [0,0]
  }

  until unvisited_nodes.empty?
    risk, current_node = unvisited_nodes.shift

    current_dijkstra_value = dijkstra_values[current_node][:value]

    neighbors = traverse(grid, current_node, 0, false)
    neighbors.each do |coord|
      # If this is new, initialize it and push it into our unvisited queue
      unless dijkstra_values.key?(coord)
        dijkstra_values[coord] = {
          'value': 99999
        }
      end

      # byebug if grid[current_node[0]].nil?
      current_alpha = grid[current_node[0]][current_node[1]]
      current_alpha_val = alphabet.index(current_alpha)

      current_alpha_val = 0 if current_alpha == 'S'
      current_alpha_val = 25 if current_alpha == 'E'

      grid_value = grid[coord[0]][coord[1]]
      neighbor_alpha_val = alphabet.index(grid_value)
      neighbor_alpha_val = 0 if grid_value == 'S'
      neighbor_alpha_val = 25 if grid_value == 'E'


      if (current_alpha_val - neighbor_alpha_val) < -1
        unvisited_nodes.push([99999, coord])
        next
      end

      value = 1 + risk

      if value < dijkstra_values[coord][:value]
        dijkstra_values[coord][:value] = value
        dijkstra_values[coord][:previous_vertex] = current_node
        unvisited_nodes.push([value, coord])
      end
    end
  end

  # If you want to see the path uncomment
  # coord = ending_coords
  # traverse_grid = grid.dup
  # while coord != [0,0]
    # coord = dijkstra_values[coord][:previous_vertex]
    # byebug if dijkstra_values[coord][:value] == 42
    # traverse_grid[coord[0]][coord[1]] = '.'
  # end
  # grid.each do |line|
  #   pp "#{line.join('')}"
  # end

  dijkstra_values[ending_coords][:value]
end

def find_coords(array, letter)
  array.each_with_index do |line, idx|
    end_index = line.index(letter)
    return [idx, end_index] unless end_index.nil?
  end
end

# puts "Part 1/2: #{program(input_array)}"
