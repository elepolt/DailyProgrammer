# frozen_string_literal: true

require_relative '../../base'
require 'pqueue'



# Dynamic programming is my downfall
def program(array)
  cavern = build_cavern(array)
  super_map = {}
  cavern.each_key do |starting_cave|
    super_map[starting_cave] = cavern_mapping(cavern, starting_cave)
  end

  time = 30

  valves_with_flow = cavern.select { |_k, v| (v[:flow_rate]).positive? && v[:opened] == false }
  total = 0
  (1..valves_with_flow.keys.length).to_a.each do |i|
    # build_permutations('AA', [], 30, valves_with_flow.keys.rotate(i), valves_with_flow, super_map)
    score = part_one('AA', 30, valves_with_flow.keys.rotate(i), valves_with_flow, super_map)
    # pp score
    total = [total, score].max
  end
  #   pp i
  #   next unless score > total

  #   total = score
  #   puts total
  #   pp valves_with_flow.keys.rotate(i)
  # end

  [total, nil]
end

RESULTS = Hash.new(0)
PERMUTATIONS = []

def part_one(current_cave, time, keys, caves, super_map)
  return 0 if keys.empty?

  # The cave we're moving to
  cave = keys.first
  flow_rate = caves[cave][:flow_rate]
  # Move there
  time_to_cave = super_map[current_cave][cave]
  # Knock the time off, and the second to open
  time = time - time_to_cave[:value] - 1
  return 0 if time.negative?
  return 0 if time.zero?

  score = 0
  # The key will be made of the time remaining, the cave we are _at_, and the remainders
  # So we know that if we just moved to EE, and CC is remaining,
  max_score = 0
  # The idea is that we'd not even both passing in keys that we can't get.
  # So if there's 3 minutes left, and it takes 5 minutes to get to a cave, don't both trying to visit that cave
  keys = keys[1..].select { |k| (time - 1 - super_map[cave][k][:value]).positive? }

  score = (time * flow_rate)
  return score if keys.empty?

  (0..keys.length).to_a.each do |i|
    rotated_keys = keys.rotate(i)
    # rotated_keys = keys[1..].rotate(i)
    key = "#{time}, #{cave} #{rotated_keys.join('-')}"
    # pp rotated_keys
    if RESULTS.keys.include?(key)
      score = RESULTS[key]
    else
      # Get our score
      score = (time * flow_rate)
      score += part_one(cave, time, rotated_keys, caves, super_map)
      RESULTS[key] = score
    end
    max_score = [max_score, score].max
  end

  max_score
end

def build_cavern(array)
  valves = {}
  array.each do |string|
    valve, details = parse_string(string)
    valves[valve] = details
  end
  valves
end

# Work backwards from ending_cave to get to the current
def get_path(current_cave, ending_cave, cavern)
  next_cave = ending_cave
  path = [ending_cave]
  while next_cave != current_cave

    tmp = cavern[next_cave][:previous_vertex]
    path.unshift(tmp)
    next_cave = tmp
  end
  path
end

def parse_string(string)
  # 'Valve AA has flow rate=0; tunnels lead to valves DD, II, BB',
  details, valves = string.split('; ')

  valve = details.split(' ')[1]
  flow = details.split('=')[1].to_i
  valves.gsub!('valve', 'valves') if valves.split(' ')[3] == 'valve'
  valves = valves.split('valves ')[1].split(', ')
  [valve, {
    flow_rate: flow,
    valves: valves,
    opened: false
  }]
end

def cavern_mapping(cavern, starting_cave)
  # So what I want is a mapping.
  # I want a mapping for every combination of valves, and how many steps it takes to get from one to the other
  # So from the test input, given AA and JJ, I want it to spit out 2
  # I think this is BFS?
  unvisited_nodes = PQueue.new
  unvisited_nodes.push([0, starting_cave])
  dijkstra_values = {}
  dijkstra_values[starting_cave] = {
    value: 0,
    previous_vertex: nil
  }
  until unvisited_nodes.empty?
    steps, current_node = unvisited_nodes.shift
    neighbors = cavern[current_node][:valves]
    break if neighbors.nil?

    neighbors.each do |cave|
      # If this is new, initialize it and push it into our unvisited queue
      unless dijkstra_values.key?(cave)
        dijkstra_values[cave] = {
          value: 99_999,
          previous_vertex: nil
        }
      end

      value = 1 + steps

      next unless value < dijkstra_values[cave][:value]

      dijkstra_values[cave][:value] = value
      dijkstra_values[cave][:previous_vertex] = current_node
      unvisited_nodes.push([value, cave])
    end
  end
  dijkstra_values
end

def get_steps(start, endx, mapping)
  mapping[start][endx]
end

# 1049 too low
# input_array = read_lines('test.txt')
input_array = read_lines('input.txt')
puts "Part 1/2: #{program(input_array)}"
