# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'Valve AA has flow rate=0; tunnels lead to valves DD, II, BB',
      'Valve BB has flow rate=13; tunnels lead to valves CC, AA',
      'Valve CC has flow rate=2; tunnels lead to valves DD, BB',
      'Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE',
      'Valve EE has flow rate=3; tunnels lead to valves FF, DD',
      'Valve FF has flow rate=0; tunnels lead to valves EE, GG',
      'Valve GG has flow rate=0; tunnels lead to valves FF, HH',
      'Valve HH has flow rate=22; tunnel leads to valve GG',
      'Valve II has flow rate=0; tunnels lead to valves AA, JJ',
      'Valve JJ has flow rate=21; tunnel leads to valve II'
    ]
  end

  def test_code
    # assert_equal ['AA', { :flow_rate => 0, :valves => ['DD', 'II', 'BB'], :status => 'closed'}], parse_string(@raw_input.first)
    assert_equal [1651, nil], program(@raw_input)
    # assert_equal [364, nil], program(@raw_input)
    # valves = build_cavern(@raw_input)
    # assert_equal ['DD', 'AA', 'II', 'JJ'], get_path('DD', 'JJ', dijkstra(valves, 'DD'))
  end
end
