# frozen_string_literal: true

require_relative '../../base'

input_array = File.readlines('input.txt').map(&:chomp)
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(stacks, array)
  stacks_2 = {}
  stacks.each do |k, v|
    stacks_2[k] = v.dup
  end

  array.each do |order|
    order_array = split_string(order, ' ')
    count = order_array[1].to_i
    from = order_array[3]
    to = order_array[5]

    count.times do
      crate = stacks[from].pop
      stacks[to].push(crate)
    end
    crates = stacks_2[from].pop(count)
    stacks_2[to] += crates
  end

  result = stacks.values.map(&:last).join
  result_2 = stacks_2.values.map(&:last).join

  [result, result_2]
end

def parse_stacks(array)
  stacks = Hash.new([])
  array.each do |line|
    line_array = line.split('')
    line_array[1..].each_slice(4).with_index do |letter, idx|
      next if letter.first == ' '

      stacks[(idx + 1).to_s] += [letter.first]
    end
  end
  stacks
end

stacks = parse_stacks(input_array[0..7].reverse)
orders = input_array[10..]

# puts "Part 1/2: #{program(stacks, orders)}"
