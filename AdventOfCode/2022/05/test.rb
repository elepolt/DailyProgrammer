# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @orders = [
      'move 1 from 2 to 1',
      'move 3 from 1 to 3',
      'move 2 from 2 to 1',
      'move 1 from 1 to 2'
    ]
    @raw_stacks = [
      '    [D]    ',
      '[N] [C]    ',
      '[Z] [M] [P]'
    ]
  end

  def test_code
    stacks = parse_stacks(@raw_stacks.reverse)
    assert_equal %w[CMZ MCD], program(stacks, @orders)
  end
end
