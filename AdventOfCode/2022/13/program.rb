# frozen_string_literal: true

require 'json'
require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, part2 = false)
  array += ['', '[[2]]', '[[6]]'] if part2
  check_array = []
  ans = 0

  idx = 0
  array.each_slice(3) do |arrays|
    idx += 1
    left = json_parse(arrays[0])
    right = json_parse(arrays[1])
    ans += idx if compare_json(left, right) == -1

    # Pre-emptive part two
    check_array.push(left)
    check_array.push(right)
  end

  return ans unless part2

  final_array = check_array.sort(&method(:compare_json))
  # # I'm pretty happy with this solution
  # # I WAS pretty happy with it until someone showed how to use sort like above
  # # So in part 1, we populated an array with just the json-parsed values, and no empty lines
  # while check_array.count > 0
  #   # No we'll shift through each value
  #   check_value = check_array.shift
  #   idx = -1
  #   while idx <= final_array.length
  #     idx += 1
  #     # And if we hit the end of our final_array, push the value in (at the end), and break
  #     if final_array[idx].nil?
  #       final_array.push(check_value)
  #       break
  #     end

  #     # Else if check_value is less than where we're at,
  #     # insert it there and break
  #     if compare_json(check_value, final_array[idx])
  #       final_array.insert(idx, check_value)
  #       break
  #     end
  #   end
  # end

  # Get the indices of our packet (+1) and multiply
  (final_array.index([[6]]) + 1) * (final_array.index([[2]]) + 1)
end

def json_parse(line)
  JSON.parse(line)
end

def compare_json(left, right)
  max = [left.length, right.length].max
  idx = -1
  while idx < max
    idx += 1
    left_comp = left[idx]
    right_comp = right[idx]
    return -1 if left_comp.nil?
    return 1 if right_comp.nil?
    next if left_comp == right_comp

    if [left_comp.class, right_comp.class].uniq == [Integer]
      return left_comp <=> right_comp
    end

    if left_comp.class == Array && right_comp.class == Integer
      return compare_json(left_comp, [right_comp])
    end

    if right_comp.class == Array && left_comp.class == Integer
      return compare_json([left_comp], right_comp)
    end

    if [left_comp.class, right_comp.class].uniq == [Array]
      return compare_json(left_comp, right_comp)
    end
  end
end

puts "Part 1: #{program(input_array)}"
puts "Part 2: #{program(input_array, true)}"
