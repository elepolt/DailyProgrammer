# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '[1,1,3,1,1]',
      '[1,1,5,1,1]',
      '',
      '[[1],[2,3,4]]',
      '[[1],4]',
      '',
      '[9]',
      '[[8,7,6]]',
      '',
      '[[4,4],4,4]',
      '[[4,4],4,4,4]',
      '',
      '[7,7,7,7]',
      '[7,7,7]',
      '',
      '[]',
      '[3]',
      '',
      '[[[]]]',
      '[[]]',
      '',
      '[1,[2,[3,[4,[5,6,7]]]],8,9]',
      '[1,[2,[3,[4,[5,6,0]]]],8,9]',
    ]
  end

  def test_code
    assert_equal 13, program(@raw_input)
    left = json_parse('[1,1,3,1,1]')
    right = json_parse('[1,1,5,1,1]')
    assert_equal true, compare_json(left, right)
    left = json_parse('[[1],[2,3,4]]')
    right = json_parse('[[1],4]')
    assert_equal true, compare_json(left, right)
    left = json_parse('[9]')
    right = json_parse('[[8,7,6]]')
    assert_equal false, compare_json(left, right)
    left = json_parse('[[4,4],4,4]')
    right = json_parse('[[4,4],4,4,4]')
    assert_equal true, compare_json(left, right)
    left = json_parse('[7,7,7,7]')
    right = json_parse('[7,7,7]')
    assert_equal false, compare_json(left, right)
    left = json_parse('[]')
    right = json_parse('[3]')
    assert_equal true, compare_json(left, right)
    left = json_parse('[[[]]]')
    right = json_parse('[[]]')
    assert_equal false, compare_json(left, right)
    left = json_parse('[1,[2,[3,[4,[5,6,7]]]],8,9]')
    right = json_parse('[1,[2,[3,[4,[5,6,0]]]],8,9]')
    assert_equal false, compare_json(left, right)
    left = json_parse('[[0,0],2]')
    right = json_parse('[[0,0],1]')
    assert_equal false, compare_json(left, right)
    left = json_parse('[1]')
    right = json_parse('[[1,2,3]]')
    assert_equal true, compare_json(left, right)
    left = json_parse('[1]')
    right = json_parse('[[1,2,3]]')
    assert_equal true, compare_json(left, right)
    left = json_parse('[[],[[3,[0,3,0,7,9],3],[0,1]],[[],4],[[[8,5]]],[9,5,5,1]]')
    right = json_parse('[[[[3,1,9,3],[1,1,1,9],1,[4,9,3,2],[9,1,8,7]],[1],[[4,2,10,2,5],[]],[[9,7,1],5,2,2,10],2],[],[[10,10,[],0]]]')
    assert_equal true, compare_json(left, right)
    left = json_parse('[[[5],7,0]]')
    right = json_parse('[[3,8,5],[9,[[4,10,0,4],[2,8,8,8,1],[2,6],[9,5,1,0,5]],10,6,5],[]]')
    assert_equal false, compare_json(left, right)
    left = json_parse('[[[[3,8,6,7,8],[],10,10]],[[10,[],[]],[[1]],4,[]],[5,8,7],[[2,6,0],[3,[3,7,10,8,6]],6],[9,0]]')
    right = json_parse('[[0],[10,[],5,2,[[4],[0,6],8]],[3],[9,1,3]]')
    assert_equal false, compare_json(left, right)

    assert_equal 140, program(@raw_input, true)
  end
end
