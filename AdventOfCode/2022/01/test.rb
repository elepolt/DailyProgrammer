require "minitest/autorun"
require_relative "program"

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      1000,
      2000,
      3000,
      0,
      4000,
      0,
      5000,
      6000,
      0,
      7000,
      8000,
      9000,
      0,
      10000,
    ]
  end

  def test_code
    assert_equal [24000, 45000], program(@raw_input)
  end
end
