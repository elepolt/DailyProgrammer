# frozen_string_literal: true

require_relative '../../base'

input_array = parse_intengers(read_lines('input.txt'))

def program(array)
  idx = 0
  elves = Hash.new(0)
  array.each do |value|
    # my `parse_integers` turns the empty line into a 0, thankfully none of the lines contain a 0 by itself!
    if value.zero?
      idx += 1
    else
      elves[idx] += value
    end
  end
  [elves.values.max, elves.values.max(3).sum]
end

puts "Part 1/2: #{program(input_array)}"
