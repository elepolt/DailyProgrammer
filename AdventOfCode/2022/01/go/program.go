package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
		"strconv"
    "sort"
)
// Just an example of what a function looks like
func sum(array []int) int {
	total := 0
	for _, v := range array {
		total += v
	}
	return total
}

func readFile() []int {
	var values []int
	file, err := os.Open("/home/evan/Documents/Code/DailyProgrammer/AdventOfCode/2022/01/input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if scanner.Text() == "" {
			values = append(values, 0)
		} else {
			value, converr := strconv.Atoi(scanner.Text())
			if converr != nil {
				fmt.Println(converr)
			}
			values = append(values, value)
		}
	}
	return values
}

func main() {
	values := readFile()
	var totals []int
	currentSum := 0
	for _, calories := range values {
		if calories == 0 {
			totals = append(totals, currentSum)
			currentSum = 0
		} else {
			currentSum += calories
		}
	}

	sort.Ints(totals)
	var totalLength = len(totals)
	fmt.Println(totals[totalLength - 1])
	fmt.Println(sum(totals[totalLength - 3:]))
}
