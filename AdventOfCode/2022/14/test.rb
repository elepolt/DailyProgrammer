# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '498,4 -> 498,6 -> 496,6',
      '503,4 -> 502,4 -> 502,9 -> 494,9',
    ]
  end

  def test_code
    assert_equal [[498, 4], [498, 6], [496, 6]], split_line_string('498,4 -> 498,6 -> 496,6')
    assert_equal 24, program(@raw_input)
    assert_equal 93, program(@raw_input, true)
  end
end
