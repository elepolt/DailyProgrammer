# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, part_2 = false)
  max_x = 0
  min_y = 500
  max_y = 500
  map = Hash.new {|map, key| map[key] = {} }

  array.each do |lines_string|
    lines = split_line_string(lines_string)
    lines.each_cons(2) do |pairs|
      start_y, start_x = pairs[0]
      end_y, end_x = pairs[1]
      max_x = [max_x, start_x, end_x].max
      max_y = [max_y, start_y, end_y].max
      min_y = [min_y, start_y, end_y].min

      move_x = (start_x <=> end_x) * -1
      move_y = (start_y <=> end_y) * -1
      move_num = [(start_x - end_x).abs, (start_y - end_y).abs].max

      map[start_x][start_y] = '#'
      move_num.times do
        start_x += move_x
        start_y += move_y
        map[start_x][start_y] = '#'
      end
    end
  end

  if part_2
    min_y = (500 - max_x) - 2
    max_y = (500 + max_x) + 2
    (min_y..max_y).each do |idy|
      map[max_x + 2][idy] = '#'
    end
  end

  while true do
    break if drop_sand_sets(map, max_x, part_2) != 'o'
  end

  # print_cave(map, min_y, max_y, max_x)
  
  count = 0
  map.values.each do |idx|
    count += idx.values.count('o')
  end

  count
end

def print_cave(map, min_y, max_y, max_x)
  width = (max_y - min_y) + 1
  cave = []
  (0..max_x).each do |idx|
    cave.push(Array.new(width, ' '))
    cave[0].each_with_index do |_line, idy|
      cave[idx][idy] = map[idx][idy+min_y] if map[idx][idy+min_y]
    end
    pp cave[idx].join('')
  end
end

def drop_sand_sets(map, max_x, part_2)
  idx, idy = [-1,500]
  timing = []
  while true
    return true if !part_2 && idx > max_x
    return true if part_2 && map[0][500] == 'o'

    # If it can move down, do it
    if map[idx+1][idy].nil?
      idx += 1
      next
    end

    # Same with diagonally
    if map[idx+1][idy-1].nil?
      idx +=1
      idy -= 1
      next
    end
    if map[idx+1][idy+1].nil?
      idx += 1
      idy += 1
      next
    end

    # If it gets to here it stops and we mark it down
    break
  end
  map[idx][idy] = 'o'
end

def split_line_string(line_string)
  line_string.split(' -> ').map { |line| line.split(',').map(&:to_i)}
end

puts "Part 1: #{program(input_array)}"
puts "Part 2: #{program(input_array, true)}"
