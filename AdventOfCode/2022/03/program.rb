require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  alphabet_array = ('a'..'z').to_a + ('A'..'Z').to_a
  alphabet = {}
  alphabet_array.each_with_index do |k,v|
    alphabet[k] = v+1
  end

  group = []
  total = 0
  total2 = 0
  array.each_with_index do |sack, idx|
    idx += 1
    # TODO: this should be it's own function
    half_point = sack.length / 2 - 1
    front = sack[0..half_point].split('')
    back = sack[(half_point + 1)..sack.length].split('')

    (front & back).each do |value|
      total += alphabet[value]
    end

    group.push(sack.split(''))
    next unless (idx % 3).zero?

    # TODO: Not sure how to union a dynamic amount of arrays
    union = group[0] & group[1] & group[2]
    union.each do |value|
      total2 += alphabet[value]
    end

    # group_string = group.each
    group = []
  end
  [total, total_2]
end

puts "Part 1/2: #{program(input_array)}"
