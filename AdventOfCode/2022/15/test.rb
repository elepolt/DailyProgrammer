# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'Sensor at x=2, y=18: closest beacon is at x=-2, y=15',
      'Sensor at x=9, y=16: closest beacon is at x=10, y=16',
      'Sensor at x=13, y=2: closest beacon is at x=15, y=3',
      'Sensor at x=12, y=14: closest beacon is at x=10, y=16',
      'Sensor at x=10, y=20: closest beacon is at x=10, y=16',
      'Sensor at x=14, y=17: closest beacon is at x=10, y=16',
      'Sensor at x=8, y=7: closest beacon is at x=2, y=10',
      'Sensor at x=2, y=0: closest beacon is at x=2, y=10',
      'Sensor at x=0, y=11: closest beacon is at x=2, y=10',
      'Sensor at x=20, y=14: closest beacon is at x=25, y=17',
      'Sensor at x=17, y=20: closest beacon is at x=21, y=22',
      'Sensor at x=16, y=7: closest beacon is at x=15, y=3',
      'Sensor at x=14, y=3: closest beacon is at x=15, y=3',
      'Sensor at x=20, y=1: closest beacon is at x=15, y=3',
    ]
  end

  def test_code
    assert_equal [[2,18], [-2, 15]], split_line('Sensor at x=2, y=18: closest beacon is at x=-2, y=15')
    assert_equal [[17,20], [21, 22]], split_line('Sensor at x=17, y=20: closest beacon is at x=21, y=22')
    # assert_equal([
    #   [0, 0],
    #   [0, 1],
    #   [0, 2],
    #   [0, 3],
    #   [0, -1],
    #   [0, -2],
    #   [0, -3],
    #   [1, 0],
    #   [2, 0],
    #   [3, 0],
    #   [-1, 0],
    #   [-2, 0],
    #   [-3, 0],
    #   [1, -1],
    #   [1, -2],
    #   [1, 1],
    #   [1, 2],
    #   [-1, -1],
    #   [-1, -2],
    #   [-1, 1],
    #   [-1, 2],
    #   [-2, 1],
    #   [-2, -1],
    #   [2, 1],
    #   [2, -1]
    # ].sort, get_empty_coords([0,0], [0,3]).sort)

    sensors = parse_sensors(@raw_input)
    assert_equal 26, program(@raw_input, 10)
    assert_equal [14, 11], find_broken_beacon_better(parse_sensors(@raw_input), 20)
    assert_equal 56000011, program(@raw_input, 10, 20)
    assert_equal false, find_reach([14, 11], parse_sensors(@raw_input))
    assert_equal [2,18], find_reach([0, 15], parse_sensors(@raw_input))[:location]
    assert_equal [8,7], find_reach([4, 11], parse_sensors(@raw_input))[:location]
    assert_equal 10, step_amount([0,15], sensors[[8,7]])
    assert_equal 10, step_amount([4,11], sensors[[8,7]])
    assert_equal 8, step_amount([10,7], sensors[[8,7]])

  end
end
