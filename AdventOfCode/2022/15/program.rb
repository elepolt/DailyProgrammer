# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array, check_row, end_coord = 0)
  max_x = 0
  max_y = 0
  min_x = 0
  min_y = 0
  map = Hash.new {|map, key| map[key] = {} }.dup
  # coords = Hash.new {|map, key| map[key] = {} }.dup
  coords = {}
  beacons = []
  array.each do |line|
    sensor, beacon = split_line(line)

    map[sensor[0]][sensor[1]] = 'S'
    map[beacon[0]][beacon[1]] = 'B'
    beacons.push([beacon[0], beacon[1]])

    distance = manhattan(sensor, beacon)
    coords[[sensor[0], sensor[1]]] = {
      :beacon => [beacon[0], beacon[1]],
      :distance => distance,
      :location => [sensor[0], sensor[1]]
    }

    max_x = [max_x, sensor[0] + distance, sensor[0] - distance, beacon[0]].max
    max_y = [max_y, sensor[1] + distance, sensor[1] - distance, beacon[1]].max
    min_x = [min_x, sensor[0] + distance, sensor[0] - distance, beacon[0]].min
    min_y = [min_y, sensor[1] + distance, sensor[1] - distance, beacon[1]].min
    # get_empty_coords(sensor, beacon).each do |coord|
      # map[coord[0]][coord[1]] = '#' if map[coord[0]][coord[1]].nil?
    # end
  end


  row = []
  (min_x..max_x).each_with_index do |idx, ind|
    point = [idx, check_row]
    pp point if idx % 250000 == 0
    is_covered = false
    coords.each do |coord, details|
      next if point == coord || point == details[:beacon]
      is_covered = true if manhattan(coord, point) <= details[:distance]
      break if is_covered
    end
    blah = is_covered ? '#' : '.'
    row.push(blah)
  end
  return row.count('#') if end_coord.zero?

  broken = find_broken_beacon_better(coords, end_coord)
  pp broken
  4000000 * broken[0] + broken[1]
  # find_broken_beacon(coords, end_coord)
end

# gives our idx amount to move it out of reach of the current sensor
def step_amount(location, sensor)
  move_to_x = sensor[:location][0] - location[0]
  # move_to_x = 0 if move_to_x.negative?
  idx = move_to_x

  # Calculate how far the coord can reach 
  ys = [sensor[:location][1], location[1]].sort
  y_diff = ys[1] - ys[0]
  reach = sensor[:distance] - y_diff
  idx += reach
  idx += 1
  idx
end

def find_reach(location, coords)
  coords.each do |coord, details|
    distance = manhattan(coord, location)
    if distance <= details[:distance]
      return coords[coord]
    end
  end
  false
end

def parse_sensors(array)
  coords = Hash.new {|map, key| map[key] = {} }
  array.each do |line|
    sensor, beacon = split_line(line)
    distance = manhattan(sensor, beacon)
    coords[[sensor[0], sensor[1]]] = {
      :beacon => [beacon[0], beacon[1]],
      :distance => distance,
      :location => [sensor[0], sensor[1]]
    }
  end
  coords
end
def find_broken_beacon(coords, end_coord)
  count = 0
  (0..end_coord).each_with_index do |idx, indx|
    count += 0
    puts count if count % 500000 == 0
    (0..end_coord).each_with_index do |idy, indy|
      puts [indx * indy] if indy % 250000 == 0
      point = [idx, idy]
      is_covered = false
      coords.each do |coord, details|
        is_covered = true if manhattan(coord, point) <= details[:distance]
        break if is_covered
      end
      return point unless is_covered
    end
  end
  nil
end

def find_broken_beacon_better(coords, end_coord)
  idx = 0
  idy = 0
  while true do
    point = [idx, idy]
    # pp point if idy % 250000 == 0
    sensor = find_reach(point, coords)
    if sensor != false
      idx += step_amount(point, sensor)

      if idx > end_coord
        idx = 0
        idy += 1
        next
      end
    else
      if idx > end_coord
        idx = 0
        idy += 1
        next
      end

      return [idx, idy]
    end
  end
end

def split_line(line)
  # AoC makes you do some weird things...
  line.split(': ').map {|l| l.split(' ')}.map { |l| l.last(2)}.map{|l| [l[0][2..-2].to_i, l[1][2..].to_i]}
end

def get_empty_coords(sensor, beacon)
  # x=8, y=7: closest beacon is at x=2, y=10
  # puts 'test'
  distance = manhattan(sensor, beacon)
  combos = get_range(sensor[0] - distance, sensor[0] + distance).product(get_range(sensor[1] - distance, sensor[1] + distance))
  combos.select {|p| manhattan(sensor, p) <= distance }
end

def manhattan(point_one, point_two)
  distance = (point_one[0] - point_two[0]).abs + (point_one[1] - point_two[1]).abs
end

def get_range(point_one, point_two)
  s, e = [point_one, point_two].sort
  (s..e).to_a
end
puts "Part 1: #{program(input_array, 2000000)}"
# puts "Part 2: #{program(input_array, 2000000, 4000000)}"
