# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  file_system = {'/' => {
    'children' => {},
    'parent' => nil,
    'files' => {},
    'files_size' => 0,
    'total_size' => 0
  }}
  previous_directory = nil
  current_directory = file_system['/']
  ls_used = false
  array[1..].each do |command_string|
    next_command = nil
    commands = command_string.split(' ')

    if commands.first == '$'
      next_command = commands[1]
      arg = commands.last if commands.count == 3

      # If we run ls, make a note of it, and move on to the next pieces
      if next_command == 'ls'
        ls_used = true
        # next
      else
        ls_used = false
      end

      if next_command == 'cd'
        if arg == '..'
          byebug if current_directory.nil?
          current_directory = current_directory['parent']
          next
        end
        current_directory = current_directory['children'][arg]
      end
    end


    if ls_used
      if commands.first.is_integer?
        current_directory['files'][commands.last] = commands.first.to_i
        current_directory['files_size'] += commands.first.to_i
        current_directory['total_size'] += commands.first.to_i
        tmp_dir = current_directory['parent']
        while tmp_dir != nil
          tmp_dir['total_size'] += commands.first.to_i
          tmp_dir = tmp_dir['parent']
        end
      else
        current_directory['children'][commands.last] = {
            'children' => {},
            'parent' => current_directory,
            'files' => {},
            'files_size' => 0,
            'total_size' => 0
          }
      end
    end
  end

  # Total Disk Space: 70000000
  total_used = file_system['/']['total_size']
  amount_available = 70000000 - total_used
  amount_needed = 30000000 - amount_available

  navigate_children(file_system['/'], 0, amount_needed, total_used)
end

def navigate_children(directory, answer, amount_needed, smallest_deletion)
  dir_size = directory['total_size']
  answer += dir_size if dir_size < 100000

  # If the dir_size is larger than amount needed, and less than smallest_deletion, update it
  if dir_size >= amount_needed && dir_size < smallest_deletion
    smallest_deletion = dir_size
  end

  child_dirs = directory['children']
  child_dirs.keys.each do |dir|
    tmp, smallest_deletion = navigate_children(directory['children'][dir], 0, amount_needed, smallest_deletion)
    answer += tmp
  end
  [answer, smallest_deletion]
end

puts "Part 1/2: #{program(input_array)}"
