# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = 'mjqjpqmgbljsphdztnvjfqwrcgsmlb'.split('')
  end

  def test_code
    assert_equal 7, program(@raw_input, 4)
    assert_equal 5, program('bvwbjplbgvbhsrlpgdmjqwftvncz'.split(''), 4)
    assert_equal 19, program(@raw_input, 14)
    assert_equal 23, program('bvwbjplbgvbhsrlpgdmjqwftvncz'.split(''), 14)
  end
end
