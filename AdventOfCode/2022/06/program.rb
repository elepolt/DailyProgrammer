# frozen_string_literal: true

require_relative '../../base'

char_array = read_lines('input.txt')[0].split('')

def program(chars, distinct_count)
  seen = []
  chars.each_with_index do |char, idx|
    seen.push(char)
    return idx + 1 if seen.last(distinct_count).uniq.count == distinct_count
  end
end

puts "Part 1: #{program(char_array, 4)}"
puts "Part 2: #{program(char_array, 14)}"
