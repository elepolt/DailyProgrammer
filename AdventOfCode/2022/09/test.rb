# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'R 4',
      'U 4',
      'L 3',
      'D 1',
      'R 4',
      'D 1',
      'L 5',
      'R 2'
    ]

    @test_2 = [
      'R 5',
      'U 8',
      'L 8',
      'D 3',
      'R 17',
      'D 10',
      'L 25',
      'U 20',
    ]
  end

  def test_code
    assert_equal true, touching([4, 1], [4, 0]) # horizontally
    assert_equal true, touching([3, 0], [4, 0]) # vertically
    assert_equal true, touching([3, 1], [4, 0]) # diagonally
    assert_equal [4, 1], move_rope([4, 2], [4, 0]) # horizontally
    assert_equal [3, 0], move_rope([2, 0], [4, 0]) # vertically
    assert_equal [3, 1], move_rope([2, 1], [4, 0]) # diagonally

    assert_equal 13, program(@raw_input)
    assert_equal 36, program(@test_2, true)
  end
end
