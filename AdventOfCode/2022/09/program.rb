# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, part_two = false)
  grid = []
  # It'd be smarter to generate the grid dynamically
  # It'd probably be even smarter to utilize math instead of a grid
  1000.times do
    grid.push(Array.new(1000, '.'))
  end
  dirs = [[-1, 0], [1, 0], [0, -1], [0, 1]]
  rope_count = part_two ? 10 : 2
  ropes = []
  rope_count.times do
    ropes.push([500, 500])
  end

  array.each do |details|
    direction, count = details.split(' ')
    dir = %w[U D L R].index(direction)

    count.to_i.times do
      ropes.each_with_index do |rope, idx|
        # No matter what we move the head
        if idx.zero?
          ropes[idx][0] += dirs[dir][0]
          ropes[idx][1] += dirs[dir][1]
        else
          # Then move all the ropes tht trail
          rope = move_rope(ropes[idx - 1], rope) unless touching(ropes[idx - 1], rope)

          # Only mark where the last piece of rope was
          grid[rope[0]][rope[1]] = '#' if idx == ropes.count - 1
        end
      end
    end
  end
  grid.flatten.count('#')
end

def move_rope(head, tail)
  x = head[0] - tail[0]
  y = head[1] - tail[1]
  move_x = x.negative? ? -1 : 1
  move_y = y.negative? ? -1 : 1
  tail[0] += move_x if x.abs.positive?
  tail[1] += move_y if y.abs.positive?

  tail
end

# Might be the whiskey, but this is a surprisngly complex check
def touching(head, tail)
  x = head[0] - tail[0]
  y = head[1] - tail[1]

  # if head && tail are in the same row/column, then they're not touching when they're two apart
  return (x.abs + y.abs) == 1 if head[0] == tail[0] || head[1] == tail[1]

  # else if they're diagonal then they become not touching when they're three apart
  (x.abs + y.abs) < 3
end

# puts "Part 1: #{program(input_array)}"
# puts "Part 2: #{program(input_array, true)}"
