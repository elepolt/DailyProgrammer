# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array)
  cycles = 0
  register = 1
  signal_strengths = {}
  signal_checks = [20, 60, 100, 140, 180, 220]
  row_change = [41, 81, 121, 161, 201]
  image = []

  current_row = 0
  6.times do
    image.push([])
  end

  array.each do |command_string|
    cycles += 1
    current_row += 1 if row_change.include?(cycles)

    draw_pixel(register, image[current_row])
    signal_strengths[cycles] = (cycles * register) if signal_checks.include?(cycles)
    next if command_string == 'noop'

    cycles += 1
    current_row += 1 if row_change.include?(cycles)
    draw_pixel(register, image[current_row])

    signal_strengths[cycles] = (cycles * register) if signal_checks.include?(cycles)

    register += command_string.split(' ').last.to_i
  end

  [signal_strengths.values.sum, image]
end

def draw_pixel(sprite_position, crt)
  pixel = sprite_visible(sprite_position, crt.count) ? '#' : ' '
  crt.push(pixel)
end

def sprite_visible(position, cycle)
  (position - cycle).abs < 2
end

answers = program(input_array)
puts "Part 1: #{answers[0]}"

puts 'Part 2:'
answers[1].each do |row|
  puts row.join('')
end
