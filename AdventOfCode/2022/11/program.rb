# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array, part_two = false)
  monkeys = {}
  array.each_slice(7) do |things|
    monkey_id = things[0].split(' ')[1][0].to_i
    starting_items = things[1].split(':')[1].strip.split(',').map(&:to_i)
    operation_string = things[2].split('old ')[1]
    operation = operation_string[0]
    # switch on math notation
    operation_count = operation_string.split(' ')[1].to_i
    operation_count = 'old' if operation_string.split(' ')[1] == 'old'
    # do math to each starting_items
    # divide each starting_item by 3... round?
    test_string = things[3]
    divisible_int = test_string.split(' ').last.to_i
    # if worrylevel divisible by divisible_int
    # do true
    true_monkey_id = things[4].split(' ').last.to_i
    # else do false
    false_monkey_id = things[5].split(' ').last.to_i
    empty = things[6]

    monkeys[monkey_id] = {}
    monkeys[monkey_id]['inspect_count'] = 0
    monkeys[monkey_id]['items'] = starting_items
    monkeys[monkey_id]['operation'] = operation
    monkeys[monkey_id]['operation_count'] = operation_count
    monkeys[monkey_id]['divisible_int'] = divisible_int
    monkeys[monkey_id]['true_monkey_id'] = true_monkey_id
    monkeys[monkey_id]['false_monkey_id'] = false_monkey_id
  end

  divisible_ints = monkeys.values.map{|m| m['divisible_int'] }
  super_mod = divisible_ints.reduce(&:*)

  round_count = part_two ? 10000 : 20
  round_count.times do
    monkeys.each do |monkey_id, monkey|
      while monkey['items'].count > 0
        t = Time.now
        item = monkey['items'].shift
        monkey['inspect_count'] += 1
        operation_count = monkey['operation_count']
        operation_count = item if operation_count == 'old'
        worry_level = item
        if monkey['operation'] == '+'
          worry_level = operation_count + item
        elsif monkey['operation'] == '*'
          worry_level = operation_count * item
        end

        worry_level /= 3 unless part_two
        worry_level %= super_mod if part_two

        divisible_int = monkey['divisible_int']
        remainder = worry_level % divisible_int
        next_monkey_id = remainder.zero? ? monkey['true_monkey_id'] : monkey['false_monkey_id']

        monkeys[next_monkey_id]['items'].push(worry_level)
      end
    end
  end

  ans1 = monkeys.values.map{|m| m['inspect_count'] }.max(2)
  pp ans1
  ans1[0] * ans1[1]
end

# puts "Part 1: #{program(input_array)}"
puts "Part 2: #{program(input_array, true)}"