# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')

def program(array, part_two = false)
  total = 0

  rps = ['A', 'B', 'C']
  xyz = ['X', 'Y', 'Z']

  array.each do |guide|
    them, me = split_string(guide, ' ')
    round = 0
    # idx will follow whether I win or lose by being the throw before/after my opponent
    # an idx of 1 when opponent is 0 means they threw rock, and I threw paper
    opp_idx = rps.index(them)
    my_idx = opp_idx - 1 # Just assume I lose, and handle winning/drawing situations later

    if part_two
      my_idx += 1 if me == 'Y'
      my_idx += 2 if me == 'Z'
    else
      my_idx += 1 if xyz.index(me) == opp_idx
      my_idx += 2 if xyz.index(me) > opp_idx
    end

    round += 3 if my_idx == opp_idx
    round += 6 if my_idx > opp_idx

    my_idx = my_idx % 3
    round += [1, 2, 3][my_idx]

    total += round

  end
  [total]
end

puts "Part 1: #{program(input_array)}"
puts "Part 2: #{program(input_array, true)}"
 