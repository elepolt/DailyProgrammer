# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      'A Y',
      'B X',
      'C Z',
    ]
  end

  def test_code
    assert_equal [15], program(@raw_input)
    assert_equal [12], program(@raw_input, true)
  end
end
