# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  forest = []
  array.each do |line|
    forest.push(line.split('').map(&:to_i))
  end

  # start with outer forest
  visible_count = (forest.count - 1) * 4
  top_score = 0
  # Go to - 2 because we don't care about the edge
  (1..forest.count - 2).to_a.each do |idx|
    (1..forest[0].count - 2).to_a.each do |idy|
      tree = forest[idx][idy]
      is_visible = false

      # returns neighbors in all directions in an array
      neighbors = get_neighbors(idx, idy, forest)

      # loop through to get all trees visible in each direction
      direction_tree_count = []
      neighbors.each do |neighbor_direction|
        # Add number of visible trees to array
        direction_tree_count.push(trees_seen(tree, neighbor_direction))
        # And check if visible from the outside. Don't overwrite is_visible to false
        # if we've already deteremed that we're visible from the outside from another direction
        is_visible ||= !neighbor_direction.map { |t| t < tree }.include?(false)
      end
      scenic_score = direction_tree_count.reduce(&:*)
      top_score = [scenic_score, top_score].max

      visible_count += 1 if is_visible
    end
  end

  [visible_count, top_score]
end

def get_neighbors(idx, idy, forest)
  neighbors = []

  dirs = [[-1, 0], [1, 0], [0, -1], [0, 1]]
  end_x = forest.count - 1
  end_y = forest[0].count - 1
  dirs.each do |dir|
    neighbors_direction = []
    tmp_x = idx
    tmp_y = idy

    while tmp_x.positive? && tmp_x < end_x && tmp_y.positive? && tmp_y < end_y
      tmp_x += dir[0]
      tmp_y += dir[1]
      neighbors_direction.push(forest[tmp_x][tmp_y])
    end
    neighbors.push(neighbors_direction)
  end

  neighbors
end

def trees_seen(tree, neighbor_trees)
  byebug if should_byebug
  trees_seen = 0
  neighbor_trees.each do |tmp_tree|
    trees_seen += 1

    return trees_seen if tmp_tree >= tree
  end

  trees_seen
end

puts "Part 1/2: #{program(input_array)}"
