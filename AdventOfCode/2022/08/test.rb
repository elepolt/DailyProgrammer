# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'program'

class TestProgram < Minitest::Test
  def setup
    @raw_input = [
      '30373',
      '25512',
      '65332',
      '33549',
      '35390',
    ]
  end

  def test_code
    assert_equal [21, 8], program(@raw_input)

    forest = []
    @raw_input.each do |line|
      forest.push(line.split('').map(&:to_i))
    end
    assert_equal get_neighbors(2, 2, forest), [[5, 3], [5, 3], [5, 6], [3, 2]]
    assert_equal get_neighbors(3, 2, forest), [[3, 5, 3], [3], [3, 3], [4, 9]]
    neighbors = get_neighbors(3, 2, forest)
    assert_equal 2, trees_seen(5, neighbors[0])
    assert_equal 2, trees_seen(5, neighbors[2])
    assert_equal 1, trees_seen(5, neighbors[1])
    assert_equal 2, trees_seen(5, neighbors[3])
  end
end
