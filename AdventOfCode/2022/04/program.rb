# frozen_string_literal: true

require_relative '../../base'

input_array = read_lines('input.txt')
# split_string(string, delimiter)
# parse_intengers(array_of_ints)

def program(array)
  total = 0
  overlaps = 0
  array.each do |pairs|
    x_string, y_string = split_string(pairs, ',')
    x_start, x_end = split_string(x_string, '-')
    y_start, y_end = split_string(y_string, '-')
    x_array = (x_start..x_end).to_a
    y_array = (y_start..y_end).to_a
    total += 1 if check_array_inclusion(x_array, y_array)
    overlaps += 1 if check_array_overlap(x_array, y_array)
  end
  [total, overlaps]
end

def check_array_inclusion(one, two)
  one & two == one || two & one == two
end

def check_array_overlap(one, two)
  return true if one & two != []

  two & one != []
end

puts "Part 1/2: #{program(input_array)}"
