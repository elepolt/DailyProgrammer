# frozen_string_literal: true

# Reverse Polish Notation is a method for representing arithmetic expressions where the operator is placed _after_ the operands (arguments), also known as **Postfix** notation; contrast this with standard arithmetic notation (**Infix**) where the operator is placed _between_ the operands (arguments), for example:
# ```
# Infix:  5 + 4 = 9
# RPN:    5 4 + = 9
# ```
#
# When multiple operators are present the operator is written after the second operand, either
#
# ```
# Infix: 3 - 4 + 5  = 4
# RPN:   3 4 - 5 +  = 4       # either format is valid
# RPN:   3 4 5 + -  = 4
# ```
#
# The advantage to using a notation like RPN is that it removes the need for parenthesis for evaluating an expression following the order of operations.
#
# Examples:
#
# Operators: + / NEG
#
# "3" => 3
# "1 1 +" => 2
# "1 2 3 + +" => 6
# "27 9 /" => 3
# "2 3 + 5 /" => 1
# "1 NEG" => -1
# "1 NEG NEG" => 1
# "2 1 NEG 5 + +" => 6
#
# Hint: think of evaluating an RPN expression as if it were elements on a stack.
#
# BONUS: Add specs for the operators not currently tested, subtraction (-) and multiplication (*)

require 'rspec'
require 'byebug'

OPERATORS = %w'+ / NEG' 

def rpn(input)
  values = input.split(' ')
  # Assuming happy path for a lot of things here, ie: Our input string will only include numbers and our operators
  numbers = values.reject { |v| OPERATORS.include?(v)}.map(&:to_i)
  operators = values.select { |v| OPERATORS.include?(v)}

  # Let's handle some cases
  # Not enough numbers
  raise 'invalid input' if numbers.count.zero?
  # without operators and more than 1 digit
  raise 'invalid input' if numbers.count > 1 && operators.count == 0

  # We are now making the assumption that we have operators to match with all our numbers
  # We will check this when asked to perform an operation on multiple numbers
  operators.each do |operator|
    if operator == 'NEG'
      numbers[0] *= -1
    else
      raise 'invalid input' if numbers.count == 1

      new_value = numbers[0..1].reduce(operator)
      numbers = [new_value] + numbers[2..]
    end
  end

  numbers[0]
end

RSpec.describe 'rpn' do
  subject { rpn(input) }

  context 'perform_operation' do
    context
  end

  context 'valid input' do
    context 'a string' do
      let(:input) { '3' }

      it 'returns the string' do
        expect(subject).to eq(3)
      end
    end

    context 'addition' do
      let(:input) { '1 1 +' }

      it 'returns the calculated amount' do
        expect(subject).to eq(2)
      end
    end

    context 'division' do
      let(:input) { '27 9 /' }

      it 'returns the calculated amount' do
        expect(subject).to eq(3)
      end
    end

    context 'negative' do
      let(:input) { '1 NEG' }

      it 'returns the calculated amount' do
        expect(subject).to eq(-1)
      end
    end

    context 'multiple operators' do
      let(:input) { '2 3 + 5 /' }

      it 'returns the calculated amount' do
        expect(subject).to eq(1)
      end
    end
  end

  context 'invalid input' do
    context 'without enough digits' do
      let(:input) { '9 /' }

      it 'raises an error' do
        expect { subject }.to raise_error('invalid input')
      end
    end

    context 'without operators and more than 1 digit' do
      let(:input) { '9 2' }

      it 'raises an error' do
        expect { subject }.to raise_error('invalid input')
      end
    end

    context 'with only a operator' do
      let(:input) { '+' }

      it 'raises an error' do
        expect { subject }.to raise_error('invalid input')
      end
    end
  end
end

RSpec::Core::Runner.run([$__FILE__])
