import collections
import pdb

challenge_inputs = ['CCl2F2', 'NaHCO3', 'C4H8(OH)2', 'PbCl(NH3)2(COOH)2']
challenge_output = {'NaHCO3': {'Na': 1, 'H': 1, 'C': 1, 'O': 3},
                    'CCl2F2': {'C': 1, 'Cl': 4, 'F': 2}, 
                    'C4H8(OH)2': { 'C': 8, 'H': 10,'O': 2}, 
                    'PbCl(NH3)2(COOH)2': {'Pb': 1, 'Cl': 1, 'N': 2, 'H': 8, 'C': 2, 'O': 4}
                    }

# challenge_inputs = ['PbCl(NH3)2(COOH)2']

def is_capital(letter):
    return letter == letter.upper()

def is_integer(letter):
    try:
        int(letter)
        return True
    except:
        return False

def multiply_brackets(temp_input, multiplier):
    temp_output = {}
    for chemical_input, chemicals in temp_input.iteritems():
        for chemical, value in chemicals.iteritems():
            temp_output[chemical] = value * multiplier

    return temp_output

def check_chemical_exists(chemical, chemical_count):
    return chemical in chemical_count

def do_challenge(challenge_inputs):
    function_output = {}
    bracketed = False
    for challenge_input in challenge_inputs:
        temp_output = {}
        temp_input = ''
        current_chemical = challenge_input[0]
        current_chemical_count = 1
        chemical_count = {}

        for letter in challenge_input[1:]:
            if is_integer(letter) and not bracketed:
                current_chemical_count = int(letter)
                if current_chemical == '':
                    temp_chemical_count = multiply_brackets(temp_output, int(letter))
                    for temp_chemical in temp_chemical_count:
                        value = temp_chemical_count[temp_chemical]
                        if check_chemical_exists(temp_chemical, chemical_count):
                            chemical_count[temp_chemical] += value
                        else:
                            chemical_count[temp_chemical] = value
                else:
                    chemical_count[current_chemical] = current_chemical_count
            elif letter == "(":
                bracketed = True
                if current_chemical != '':
                    chemical_count[current_chemical] = current_chemical_count
            elif letter == ")":
                bracketed = False
                temp_output = do_challenge([temp_input])
                temp_input = ''
                current_chemical = ''
            elif bracketed:
                temp_input += letter

            elif is_capital(letter): # If the letter is capital it is a new chemical and
                if check_chemical_exists(current_chemical, chemical_count):
                    chemical_count[current_chemical] += current_chemical_count
                else:
                    chemical_count[current_chemical] = current_chemical_count
                current_chemical = letter
            else:
                current_chemical += letter

        if not check_chemical_exists(current_chemical, chemical_count) and current_chemical != '':
            chemical_count[current_chemical] = current_chemical_count

        function_output[challenge_input] = chemical_count

    return function_output

my_output = do_challenge(challenge_inputs)
print my_output == challenge_output