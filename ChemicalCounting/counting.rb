# challenge_input = ['CCl2F2', 'NaHCO3', 'C4H8(OH)2', 'PbCl(NH3)2(COOH)2']
challenge_input = ['C4H8(OH)2']

def is_capital?(letter)
    return letter == letter.capitalize
end

def is_integer?(letter)
    letter.to_i.to_s == letter
end


def do_challenge(challenge_input)
    output = {}
    bracketed = false
    temp_input =  ''
    challenge_input.each do |input|
        temp_output = {}
        current_chemical_letters = input.split("")
        current_chemical = current_chemical_letters.first
        current_chemical_count = 1
        chemical_count = {}

        current_chemical_letters.drop(1).each do |letter|
            # if letter == "("
            #     bracketed = true
            # elsif letter == ")"
            #     temp_output = do_challenge(temp_input)
            # else
            #     temp_input += letter
            # end

            if is_integer?(letter)
                current_chemical_count = letter.to_i
                chemical_count[current_chemical] = current_chemical_count
            elsif letter == "("
                bracketed = true
            elsif letter == ")"
                bracketed == false
                temp_output = do_challenge([temp_input])
                print temp_output
            elsif bracketed
                temp_input += letter

            elsif is_capital?(letter) # If the letter is capital it is a new chemical and 
                chemical_count[current_chemical] = current_chemical_count
                current_chemical = letter
            else
                current_chemical += letter
            end
        end
        output[input] = chemical_count
    end
    return output
end

output = do_challenge(challenge_input)
print output


print "\n"