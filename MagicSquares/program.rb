# Magic Squares
# 
# N x N grid of numbers
# Fulfills some set of criteria
# 1) contains the numbers 1 .. N^2 
# 2) row, col, diag sum to the same number 'magic number/sum'

# [[4, 9, 2],
#  [3, 5, 7],
#  [8, 1, 6]]
# sum == 15

# N * (N^2 + 1) / 2

def totalSum(n)
  return n * (n**2 + 1) /2
end

require 'pp'
def program(box)
  boxLength = box.length
  magicSum = totalSum(boxLength)
  isMagic = true
  
  diagSum = 0
  oppDiagSum = 0
  box.each_with_index do |row, idx|
    #  Handles rows
    rowSum = 0
    row.each { |a| rowSum+=a }
    isMagic = false if rowSum != magicSum
    
    # Handles columns
    colSum = 0
    0.upto(boxLength - 1) do |colIdx|
      colSum += box[colIdx][idx]
      
      diagSum += box[idx][colIdx] if idx == colIdx
      oppDiagSum += box[idx][boxLength-idx-1] if idx == colIdx
    end
    isMagic = false if colSum != magicSum

  end
  isMagic = false if diagSum != magicSum
  isMagic = false if oppDiagSum != magicSum
  isMagic
end

box = [[4, 9, 2],
       [3, 5, 7],
       [8, 1, 6]]
puts program(box)


# O(n!)
def brute_force(n)
  magicSum = totalSum(n)
  squared = n**2
  a = (1..squared).to_a
  combos = a.permutation(squared).to_a
  combos.each do |box|
    # pp box
    newBox = []
    # [1,2,3,4]
    wholeNumber = -1
    box.each_with_index do |cell, idx|
      modulus = idx % n
      wholeNumber = wholeNumber + 1 if modulus == 0
      newBox[wholeNumber] = [] if modulus == 0
      newBox[wholeNumber].push(cell)
    end
    
    isMagic = program(newBox)
    puts "Magic can exist" if isMagic
  end
end

# brute_force(3)

def build_row()

end

require 'byebug'
def less_force(n)
  magicSum = totalSum(n)
  squared = n**2
  squaredArray = (1..squared).to_a
  (1..squared).each do |i|
    (1..squared).each do |i|
      # this is where I need to do the shifting
    end
    # I want to create a new array by slicing away the index and adding it to the end
    # This is totally the right thing to do, I'm just getting my permutations wrong
    squaredArray.each_slice(3) do |y|
      test = y.inject(:+)
      pp test if test == magicSum
      # break if 
    end
  end

end

less_force(3)