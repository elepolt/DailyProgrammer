### Battleship ###

# Battleship Board:
#   - 10x10 alphanumeric matrix with letters denoting columns, numbers denoting rows
#   - Has many ships:
#     - `length`
#     - `position`
#     - `direction`
#     - `type`
#   - Ships can face in the 4 cardinal directions (N, S, E, W)
#   - 5 types of ships with different sizes:
#     - Carrier (5)
#     - Battleship (4)
#     - Submarine (3)
#     - Cruiser (2)
#     - Patrol (1)
#
#     A  B  C  D  E  F  G  H  I  J
#   1
#   2
#   3
#   4                                     N
#   5                                  W     E
#   6                                     S
#   7
#   8
#   9
#   10

# API

#  - Create a game with an array of ships
#  - fire!(): Accept a coordinate String and return a Boolean indicating whether a ship was hit
#  - [Optional: Get status of the game board]
#  - [Optional: fire!(): Return some indication of a ship being sunk]
#  - [Optional: fire!(): Return some indication of the last ship being sunk]

class Battleship
  def initialize(ships)
    @ships = ships
    @board = build_board
    add_ships
  end

  # TODO: Break if board size goes over 25, we currently only deal with A-Z
  def build_board(board_size = 10)
    board = []
    board_size.times do
      board.push(Array.new(10, 0))
    end
    board
  end

  def add_ships
    @ships.each do |ship|
      starting_coords = split_alpha_numeric_input(ship[:position])
      starting_row = starting_coords[0]
      starting_col = starting_coords[1]
      carrier_letter = ship[:type].to_s.split('').first.upcase
      alpha = get_numeric_index(starting_col)
      ship[:coordinates].push("#{alpha}#{starting_row+1}")

      # Loop through the ship length and change our row/column based on the direction
      # placing the ship's defining letter as we go
      ship[:length].times do
        @board[starting_row][starting_col] = carrier_letter
        starting_row += 1 if ship[:direction] == :south
        starting_row -= 1 if ship[:direction] == :north
        starting_col -= 1 if ship[:direction] == :west
        starting_col += 1 if ship[:direction] == :east
        alpha = get_numeric_index(starting_col)
        ship[:coordinates].push("#{alpha}#{starting_row+1}")
      end
    end
  end

  def fire!(xy)
    coords = split_alpha_numeric_input(xy)
    row = coords[0]
    col = coords[1]

    @ships.each do |ship|
      # byebug if ship[:type] == :battleship
      # byebug if xy == 'I7'
      if ship[:coordinates].include?(xy)
        # byebug
        ship[:coordinates].delete(xy)
        @board[row][col] = 'X'
        return true
      end
    end
    false
  end
end

# Brings in the likes of B3 and returns [2,1]
# Notes, deal with zero indexing here and swap Alpha with numeric as 3 is actually the row, and B is the column
# TODO: Build in check to ensure it's actually coming in as alphanumeric
# and maybe make sure it doesn't extend the length of the board
def split_alpha_numeric_input(alphanumeric_input)
  alpha = get_letter_index(alphanumeric_input.split('')[0])
  numeric = alphanumeric_input.split('')[1..-1].join.to_i - 1
  [numeric, alpha]
end

def get_letter_index(letter)
  ('A'..'J').to_a.find_index(letter.upcase)
end

# Ability to turn our index back into letters
def get_numeric_index(number)
  ('A'..'J').to_a[number]
end

require 'rspec'
require 'byebug'

RSpec.describe Battleship do
  let(:carrier) do
    {
      type: :carrier,
      position: 'A6',
      direction: :south,
      length: 5,
      coordinates: []
    }
  end

  let(:battleship) do
    {
      type: :battleship,
      position: 'D9',
      direction: :east,
      length: 4,
      coordinates: []
    }
  end

  let(:submarine) do
    {
      type: :submarine,
      position: 'I7',
      direction: :north,
      length: 3,
      coordinates: []
    }
  end

  let(:game) { Battleship.new([carrier, battleship, submarine]) }

  subject { game }

  context 'hits' do
    it 'handles helper methods' do
      expect(get_letter_index('a')).to eq(0)
      expect(get_letter_index('B')).to eq(1)
      expect(split_alpha_numeric_input('A1')).to match_array([0, 0])
      expect(get_numeric_index(3)).to eq('D')
    end
    it 'strikes the beginning edge' do
      turn = subject.fire!('I7') # Hits the Submarine

      expect(turn).to be_truthy
    end

    it 'strikes the middle' do
      turn = subject.fire!('G9') # Hits the Battleship

      expect(turn).to be_truthy
    end

    it 'strikes the ending edge' do
      turn = subject.fire!('A10') # Hits the Carrier

      expect(turn).to be_truthy
    end
  end
  context 'misses' do
    it 'does not strike the middle of the ocean' do
      turn = subject.fire!('E6')

      expect(turn).to be_falsey
    end
  end
end

RSpec::Core::Runner.run([$__FILE__])
