inputs = {
            '3': [
                [1, 3],
                [2, 3],
                [4, 5]
            ],
            '7': [
                [2, 4],
                [3, 6],
                [1, 3],
                [6, 8]
            ],
            '5':[
                [6, 8],
                [5, 8],
                [8, 9],
                [5, 7],
                [4, 7]
            ],
            '14': [
                [15, 18],
                [13, 16],
                [9, 12],
                [3, 4],
                [17, 20],
                [9, 11],
                [17, 18],
                [4, 5],
                [5, 6],
                [4, 5],
                [5, 6],
                [13, 16],
                [2, 3],
                [15, 17],
                [13, 14]
            ]
        }


def mvp():
    for hours, enter_exit in inputs.items():
        enter_exit.sort()
        time_spans = []
        for intervals in enter_exit:
            fits = False
            time_enter = intervals[0]
            time_exit = intervals[1]
            if len(time_spans) == 0:
                time_spans.append(intervals)
            else:
                for idx, time_span in enumerate(time_spans):
                    lowest_start = time_span[0]
                    lowest_end = time_span[1]
                    # If it's in the range
                    if lowest_start <= time_enter <= lowest_end:
                        fits = True
                        if time_exit > lowest_end:
                            time_spans[idx] = [lowest_start, time_exit]
                if not fits:
                    time_spans.append(intervals)

        
        total_hours = 0
        for time_span in time_spans:
            total_hours += time_span[1] - time_span[0]

        print (str(total_hours) == hours)

# Stolen from a guy on the internet.
# Totally makes sense though, I've got quite a bit to learn about 
# some of Python's math operators...
def better():
    for hours, enter_exit in inputs.items():
        ranges = (range(s, e) for s, e in enter_exit)
        print(int(hours) == len(set().union(*ranges)))

def main():
    # mvp()
    better()


if __name__ == "__main__":
    main()