require 'pp'
require 'byebug'

def randomize_square
  ['O', 'X'].sample
end

# Just looping through the height/width and adding a random dead/alive to the coordinate
def create_grid(height, width)
  grid = []
  (0..height-1).each do |x|
    grid[x] = []
    (0..width-1).each do |y|
      grid[x][y] = randomize_square
    end
  end
  grid
end

def calculate_life(current_cell, neighbors)
  dead_cells = neighbors.count('X')
  alive_cells = neighbors.count('O')

  next_generation_cell = 'X'
  # TODO - EDL: Should probably refactor this to be a little more verbose, maybe in V2.0
  # If current cell is dead and there are three live neighbors
  next_generation_cell = 'O' if current_cell == 'X' && alive_cells == 3
  # Alive cell surrounded by 2 or 3 other lives cells
  next_generation_cell = 'O' if current_cell == 'O' && [2,3].include?(alive_cells)

  next_generation_cell
end

# I do not like this at all, essentially 'hard coding' the edges, gotta be a more progmatic way to do that
# Commenting out, but leaving to discuss, see updated function below
# def get_neighbors1(grid, idx, idy)
#   neighbors = []
#   # if we're not in the top row
#   if idx > 0
#     neighbors.push(grid[idx-1][idy-1]) if idy > 0
#     neighbors.push(grid[idx-1][idy])
#     neighbors.push(grid[idx-1][idy+1]) if idy < grid[idx].length - 1
#   end

#   # if we're not at the edges
#   neighbors.push(grid[idx][idy-1]) if idy > 0
#   neighbors.push(grid[idx][idy+1]) if idy < grid[idx].length - 1

#   # If we're not in the bottom row
#   if idx < grid.length - 1
#     neighbors.push(grid[idx+1][idy-1]) if idy > 0
#     neighbors.push(grid[idx+1][idy])
#     neighbors.push(grid[idx+1][idy+1]) if idy < grid[idx].length - 1
#   end


#   neighbors
# end

# So, this is a 'fancier' version, but I think I like it better
def get_neighbors(grid, idx, idy)
  # The ultimate goal here is to get the values of the coords surrounding [idx][idy]
  height = grid.length - 1
  width = grid[0].length - 1

  # Instantiate an array of itself
  horizontal_coords = [idx]
  vertical_coords = [idy]

  # Then if x/y is on an edge, only add the index of the opposite direction
  horizontal_coords.push(idx-1) if idx > 0
  horizontal_coords.push(idx+1) if idx < height
  vertical_coords.push(idy-1) if idy > 0
  vertical_coords.push(idy+1) if idy < width
  # Example: For idx=0 and idy=1
  # horizontal_coords = [0, 1]
  # vertical_coords = [1, 0, 2]

  # Then we get all the combinations (neighbors)
  neighbor_coords = horizontal_coords.product(vertical_coords)
  # And remove the cell's location
  neighbor_coords.delete([idx,idy])
  # Then just loop through the coords and grab the values
  neighbors = neighbor_coords.collect { |x, y| grid[x][y] }
end

def next_generation(grid)
  next_grid = []
  grid.each_with_index do |x, idx|
    # Add the new row
    next_grid[idx] = []
    # Each with index here because the 'y' will be the actual cell (alive/dead)
    grid.each_with_index do |y, idy|
      # Don't forget to use the index, and not the actual value of the cell
      neighbors = get_neighbors(grid, idx, idy)
      current_cell = grid[idx][idy]
      next_grid[idx][idy] = calculate_life(current_cell, neighbors)
    end
  end
  next_grid
end

def put_grid(grid)
  # Just user prettyprint so it's stacked nicer
  grid.each { |g| pp g }
  puts ''
end

def play_game(height, width, generations)
  grid = create_grid(height, width)
  # print out the inital grid
  put_grid(grid)
  (0..generations-1).each do |i|
    # And all following generations
    grid = next_generation(grid)
    put_grid(grid)
  end
end

play_game(3,3,1)

require "minitest/autorun"
class TestProgram < Minitest::Test
  def setup
    @my_grid = [
      ['O', 'X', 'X'],
      ['X', 'O', 'O'],
      ['O', 'O', 'X']
    ]
  end
  def test_get_neighbors00
    neighbors = get_neighbors(@my_grid, 0, 0)
    assert_equal 3, neighbors.length
    assert_equal ['O', 'X', 'X'], neighbors.sort
  end

  def test_get_neighbors01
    neighbors = get_neighbors(@my_grid, 0, 1)
    assert_equal 5, neighbors.length
  end

  def test_get_neighbors02
    neighbors = get_neighbors(@my_grid, 0, 2)
    assert_equal 3, neighbors.length
  end

  def test_get_neighbors10
    neighbors = get_neighbors(@my_grid, 1, 0)
    assert_equal 5, neighbors.length
  end

  def test_get_neighbors11
    neighbors = get_neighbors(@my_grid, 1, 1)
    assert_equal 8, neighbors.length
    assert_equal ['O', 'O','O','O','X', 'X', 'X', 'X'], neighbors.sort
  end

  def test_get_neighbors12
    neighbors = get_neighbors(@my_grid, 1, 2)
    assert_equal 5, neighbors.length
  end

  def test_get_neighbors20
    neighbors = get_neighbors(@my_grid, 2, 0)
    assert_equal 3, neighbors.length
  end

  def test_get_neighbors21
    neighbors = get_neighbors(@my_grid, 2, 1)
    assert_equal 5, neighbors.length
  end

  def test_get_neighbors22
    neighbors = get_neighbors(@my_grid, 2, 2)
    assert_equal 3, neighbors.length
  end

  def test_calculate_life
    assert_equal 'X', calculate_life('X', ['X', 'O', 'O'])
  end
end