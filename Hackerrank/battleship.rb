# frozen_string_literal: true

### Battleship ###

# Battleship Board:
#   - 10x10 alphanumeric matrix with letters denoting columns, numbers denoting rows
#   - Has many ships:
#     - `length`
#     - `position`
#     - `direction`
#     - `type`
#   - Ships can face in the 4 cardinal directions (N, S, E, W)
#   - 5 types of ships with different sizes:
#     - Carrier (5)
#     - Battleship (4)
#     - Submarine (3)
#     - Cruiser (2)
#     - Patrol (1)
#
#     A  B  C  D  E  F  G  H  I  J
#   1
#   2
#   3
#   4                                     N
#   5                                  W     E
#   6                                     S
#   7
#   8
#   9
#   10

# API

#  - Create a game with an array of ships
#  - fire!(): Accept a coordinate String and return a Boolean indicating whether a ship was hit
#  - [Optional: Get status of the game board]
#  - [Optional: fire!(): Return some indication of a ship being sunk]
#  - [Optional: fire!(): Return some indication of the last ship being sunk]

class Battleship
  def initialize(ships)
    @ships = ships
    @board = build_board
    add_ships
  end

  def build_board
    board = []
    10.times do
      board.push(Array.new(10, 0))
    end
    board
  end

  def add_ships
    @ships.each do |ship|
      starting_row = ship[:position].split('')[1..].join.to_i - 1
      starting_col = get_letter_index(ship[:position].split('')[0])
      carrier_letter = ship[:type].to_s.split('').first

      # Loop through the ship length and change our row/column based on the direction
      # placing the ship's defining letter as we go
      ship[:length].times do
        @board[starting_row][starting_col] = carrier_letter
        starting_row += 1 if ship[:direction] == :south
        starting_row -= 1 if ship[:direction] == :north
        starting_col -= 1 if ship[:direction] == :west
        starting_col += 1 if ship[:direction] == :east
      end
    end
  end

  def fire!(xy)
    pp @board
    row = xy.split('')[1..].join.to_i - 1
    col = get_letter_index(xy.split('')[0])

    return true if @board[row][col] != 0

    false
  end

  def get_letter_index(letter)
    ('A'..'J').to_a.find_index(letter)
  end
end

require 'rspec'

RSpec.describe Battleship do
  let(:carrier) do
    {
      type: :carrier,
      position: 'A6',
      direction: :south,
      length: 5
    }
  end

  let(:battleship) do
    {
      type: :battleship,
      position: 'D9',
      direction: :east,
      length: 4
    }
  end

  let(:submarine) do
    {
      type: :submarine,
      position: 'I7',
      direction: :north,
      length: 3
    }
  end

  let(:game) { Battleship.new([carrier, battleship, submarine]) }

  subject { game }

  context 'hits' do
    it 'strikes the beginning edge' do
      turn = subject.fire!('I7') # Hits the Submarine

      expect(turn).to be_truthy
    end

    it 'strikes the middle' do
      turn = subject.fire!('G9') # Hits the Battleship

      expect(turn).to be_truthy
    end

    it 'strikes the ending edge' do
      turn = subject.fire!('A10') # Hits the Carrier

      expect(turn).to be_truthy
    end
  end
  context 'misses' do
    it 'does not strike the middle of the ocean' do
      turn = subject.fire!('E6')

      expect(turn).to be_falsey
    end
  end
end

RSpec::Core::Runner.run([$__FILE__])
