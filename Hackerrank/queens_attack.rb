# frozen_string_literal: true

require 'byebug'
require 'minitest/autorun'

def program(row_count, _obstacle_count, queen_row, queen_column, obstacles)
  # The max number of moves for the queen is always the row count squared, zero indexed
  max_moves = (row_count - 1)**2
  return max_moves if obstacles.nil?

  moves = 0
  all_directions = [-1, -1, 0, 1, 1].permutation(2).uniq
  all_directions.each do |coords|
    moves += count_direction(row_count, queen_row, queen_column, coords[0], coords[1], obstacles)
  end

  moves
end

def count_direction(row_count, queen_row, queen_column, col_direction, row_direction, obstacles)
  # given queen at 4,2
  # given end at 0,0
  count = 0
  row_count.times do
    queen_row += row_direction
    queen_column += col_direction
    # byebug if col_direction == -1 && row_direction == 1
    break if queen_row > row_count || queen_row.zero?
    break if queen_column > row_count || queen_column.zero?
    break if obstacles.include?([queen_row, queen_column])

    count += 1
  end
  count
end

def build_board(row_count, _obstacle_count, queen_row, queen_column, obstacles)
  board = []
  row_count.times do
    board.push Array.new(row_count, 0)
  end
  # byebug
  board[queen_row][queen_column] = 'Q'

  obstacles&.each do |obstacle_coords|
    o_x = obstacle_coords[0] - 1
    o_y = obstacle_coords[1] - 1
    board[o_x][o_y] = 'X'
  end

  board
end

class TestProgram < Minitest::Test
  def setup; end

  def test_code
    # assert_equal 9, program(4, 0, 4, 4, nil)
    obstacles = [
      [5, 5],
      [4, 2],
      [2, 3]
    ]
    assert_equal 10, program(5, 3, 4, 3, obstacles)
    # assert_equal [[0, 0, 0, 0, 0], [0, 0, "X", 0, 0], [0, 0, 0, 0, 0], [0, "X", "Q", 0, 0], [0, 0, 0, 0, "X"]], build_board(5, 3, 3, 2, obstacles)
    # assert_equal 3, count_direction(4, 4, 4, -1, -1, [1,1])
    # [-1, 0]
    # [-1, 1]
    # [0, -1]
    # [0, 1]
    # [1, -1]
    # [1, 0]
  end
end
