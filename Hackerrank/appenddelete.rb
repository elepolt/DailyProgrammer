# frozen_string_literal: true

require 'byebug'

# This is real close, missing two cases.
# I got my requirements wrong, it's supposed to be 'exactly' k many moves
# There's definitely a 'gotcha' somewhere regarding even/odd moves and the add/delete amounts
def appendAndDelete(s, t, k)
  similar_chars = 0
  minimum = [s.length, t.length].min
  (0..minimum).each do |i|
    break if s[i] != t[i]

    similar_chars += 1
  end
  return 'No' if k < similar_chars

  delete_amount = s[similar_chars..].nil? ? minimum : s[similar_chars..].length
  delete_amount = minimum if delete_amount.zero?
  add_amount = t[similar_chars..].nil? ? minimum : t[similar_chars..].length
  return 'No' if s.length.even? && t.length.odd? && k.even?
  return 'No' if s.length.odd? && t.length.even? && k.even?
  return 'No' if delete_amount + add_amount > k

  'Yes'
  # puts similar_chars
  # puts '-------'
end

require 'minitest/autorun'

class TestProgram < Minitest::Test
  # def setup
  # end

  def test_code
    assert_equal 'Yes', appendAndDelete('hackerhappy', 'hackerrank', 9)
    assert_equal 'Yes', appendAndDelete('aba', 'aba', 7)
    assert_equal 'No', appendAndDelete('ashley', 'ash', 2)
    assert_equal 'No', appendAndDelete('abcd', 'abcdert', 10)
  end
end
