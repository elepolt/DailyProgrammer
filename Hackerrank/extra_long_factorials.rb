# frozen_string_literal: true

require 'byebug'
require 'minitest/autorun'

def program(n)
  # Math.gamma(n+1).to_i
  (1..n).inject(:*) || 1
end

class TestProgram < Minitest::Test
  def setup; end

  def test_code
    assert_equal 15_511_210_043_330_985_984_000_000, program(25)
  end
end
