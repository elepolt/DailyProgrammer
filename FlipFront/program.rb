# frozen_string_literal: true

require 'byebug'
def flipfront(input, index)
  front = input[0..index - 1]
  back = input[index..-1]
  front.reverse + back
end

require 'minitest/autorun'
class TestProgram < Minitest::Test
  def test_1
    assert_equal [1, 0, 2, 3, 4], flipfront([0, 1, 2, 3, 4], 2)
    assert_equal [2, 1, 0, 3, 4], flipfront([0, 1, 2, 3, 4], 3)
    assert_equal [4, 3, 2, 1, 0], flipfront([0, 1, 2, 3, 4], 5)
    assert_equal [2, 2, 1, 2], flipfront([1, 2, 2, 2], 3)
  end
end
