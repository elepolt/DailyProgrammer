import itertools

inputs = [
    [79, 82, 34, 83, 69],
    [420, 34, 19, 71, 341],
    [17, 32, 91, 7, 46],
]
outputs = [
    [3469798283, 8382796934],
    [193413442071, 714203434119],
    [173246791, 917463217],
]

def main():
    challenge_output = []

    for challenge_input in inputs:
        smallest = biggest = concatenate_list(challenge_input)
        for permutation in itertools.permutations(challenge_input):
            int_permutation = concatenate_list(permutation)
            if int_permutation < smallest:
                smallest = int_permutation
            elif int_permutation > biggest:
                biggest = int_permutation

        challenge_output.append([smallest, biggest])

    print(challenge_output == outputs)

def concatenate_list(input_list):
    return int(''.join(map(str, input_list)))

if __name__ == "__main__":
    main()
