require 'byebug'
=begin
    1 - The number of teams engaged is maintained to 30.
    2 - The schedule is composed of 58 rounds of 15 games. 
        Each team plays 2 games against the other teams - one at home and the other away - for a total of 58 games. 
        All teams are playing on the same day within a round.
    3 - After the first half of the regular season (29 rounds), each team must have played exactly once against all other teams.
    4 - Each team cannot play more than 2 consecutive home games, and playing 2 consecutive home games cannot occur more than once during the whole season.
    5 - Rule 4 also applies to away games.
    6 - The schedule generated must be different every time the program is launched.
=end
print "\n"

# teams = ['Atlanta Hawks', 'Boston Celtics', 'Brooklyn Nets', 'Charlotte Hornets', 'Chicago Bulls', 'Cleveland Cavaliers', 'Dallas Mavericks', 'Denver Nuggets', 'Detroit Pistons', 'Golden State Warriors', 'Houston Rockets', 'Indiana Pacers', 'Los Angeles Clippers', 'Los Angeles Lakers', 'Memphis Grizzlies', 'Miami Heat', 'Milwaukee Bucks', 'Minnesota Timberwolves', 'New Orleans Pelicans', 'New York Knicks', 'Oklahoma City Thunder', 'Orlando Magic', 'Philadelphia 76ers', 'Phoenix Suns', 'Portland Trail Blazers', 'Sacramento Kings', 'San Antonio Spurs', 'Toronto Raptors', 'Utah Jazz', 'Washington Wizards']
# teams = ['Atlanta Hawks', 'Boston Celtics', 'Brooklyn Nets', 'Charlotte Hornets', 'Chicago Bulls', 'Cleveland Cavaliers', 'Dallas Mavericks', 'Denver Nuggets']
teams = ['Atlanta Hawks', 'Boston Celtics', 'Brooklyn Nets', 'Charlotte Hornets', 'Chicago Bulls', 'Cleveland Cavaliers']

number_of_teams = teams.count
schedule_combinations = teams.combination(2).to_a
rounds = []

teams_used = [] # Variable that holds which teams I've already used for this round
tmp = 0
round = []

def in_round?(teams, round)
    round.each do |matchup|
        if matchup.include? teams[0] or matchup.include? teams[1]
            return true
        end
    end
    return false
end

# I loop through all of the possible game combinations
while schedule_combinations.length != 0
    # 'shift' moves the game out of the array that I have
    game_teams = schedule_combinations.shift
    if !in_round?(game_teams, round)
        round.push(game_teams)
        if round.count == teams.count/2
            tmp = 0
            rounds.push(round)
            round = []
        end
    else
        schedule_combinations.push(game_teams)
        if tmp > schedule_combinations.count
            byebug
            break
        end
        tmp += 1
        
        # print "one of these teams has already played this round"
    end
    
    
    # If the either of the teams have already been used, move to else
    # if !teams_used.include? game_teams[0] and !teams_used.include? game_teams[1]
    #     # These few lines just add the teams to the variable I have to display the schedule at the end
    #     if !rounds.include? round
    #         rounds[round] = []
    #     end
    #     rounds[round].push(game_teams)
        
    #     teams_used.concat game_teams # Since it made it here add the teams to the variable so that they don't get used again
    #     if teams_used.count == number_of_teams # if the teams_used count equals the number of teams we have, move on to the next round
    #         tmp = 0
    #         round += 1
    #         teams_used = []
    #     end
    # else
    #     # This is for debugging my current issue. 
    #     tmp += 1
    #     if tmp > schedule_combinations.count
    #         remaining_teams = teams-teams_used
    #         print "missing teams: #{teams - teams_used}\n"
    #         print "Current round (#{round}: #{rounds[round]}"

    #         print "\n"
    #         rounds.each do |past_round, matchups|
    #             matchups.each do |matchup|
    #                 if matchup.include? remaining_teams[0] and matchup.include? remaining_teams[1]
    #                     print "in round #{past_round}, these two teams already played each other" 
    #                     print "\n"
    #                     print rounds[past_round]
    #                     print "\n"
    #                 end
    #             end
    #         end
            
    #         break
    #     end
    #     # else put them back into the array of games at the end to use later
    #     schedule_combinations.push(game_teams)
    # end
end

print rounds

# rounds.each do |round, matchups|
#     print round
#     matchups.each do |matchup|
#         print "\n\tHome: #{matchup[0]}"
#         print "\n\tAway: #{matchup[1]}"
#         print "\n"
#     end
# end

print "\n"