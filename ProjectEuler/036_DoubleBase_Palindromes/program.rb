require 'minitest/autorun'
require 'byebug'
# The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
# Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
# (Please note that the palindromic number, in either base, may not include leading zeros.)

def program
  sum = 0
  (0..1000000).each do |idx|
    sum += idx if is_palindrome(idx.to_s) && is_palindrome(idx.to_s(2))
  end
  puts sum
end

def is_palindrome(string)
  string_length = string.length
  half = (string_length / 2.0).ceil
  # If odd we need to subtract one due to indexing
  # 3/2 = 2, and we need to remove index 1
  half -= 1 if string_length.odd?
  string.slice!(half) if string_length.odd?
  front = string[..half - 1]
  back = string[half..]

  front == back.reverse
end

program

class TestProgram < Minitest::Test
  # def setup
  #   # Do global test setup here
  # end

  def test_program
    assert_equal 1, 1
    # assert_equal 1, program(1)
    assert_equal true, is_palindrome('1221')
    assert_equal false, is_palindrome('1212')
    assert_equal true, is_palindrome('121')
    assert_equal false, is_palindrome('221')
  end
end
