require 'minitest/autorun'
require 'byebug'
# The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
# Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
# (Please note that the palindromic number, in either base, may not include leading zeros.)

def program
  sum = 0
  (1..10).each do |idx|
    sum += idx ** idx
  end
  # byebug
  puts sum.to_s[-9..]
  # puts sum
end

program