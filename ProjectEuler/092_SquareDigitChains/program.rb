require 'minitest/autorun'
require 'byebug'

def program(square_input)
  sum = 0
  answers = { 89 => 89, 1 => 1 }

  (1..10**square_input).reverse_each do |idx|
    digit_chain_answer, answers = digit_chain(idx, answers)
    # Keep track of 89s
    sum += 1 if digit_chain_answer == 89
  end

  sum
end

def digit_chain(final_digit, answers)
  orig_digit = final_digit
  found_numbers = []
  until [1, 89].include?(final_digit)
    if answers.keys.include?(final_digit)
      final_digit = answers[final_digit]
      break
    end
    found_numbers.push(final_digit)
    final_digit = square_digit(final_digit)
  end
  found_numbers.each { |n| answers[n] = final_digit unless answers.keys.include?(n) }

  [final_digit, answers]
end

def square_digit(digit)
  split_digit(digit).map { |i| i**2 }.reduce(:+)
end

def split_digit(digit)
  digit.to_s.split('').map(&:to_i)
end

puts program(7)
# puts program(6)

# Top level comment
class TestProgram < Minitest::Test
  def setup
    # Do global test setup here
    @answers = { 89 => 89, 1 => 1, 85 => 89 }
  end

  def test_program
    # assert_equal [4, 4], split_digit(44)
    # assert_equal [8, 9, 0], split_digit(890)
    # assert_equal 32, square_digit(44)
    # assert_equal 1, digit_chain(44, @answers)
    # assert_equal 89, digit_chain(85, @answers)
    # assert_equal 89, digit_chain(89)
    # assert_equal 7, program(1)
    # assert_equal 8581146, program(7)
  end
end
