# flat_list = []

def flatten(array, flat_list):
    if isinstance(array, int):
        flat_list.append(array)
        return flat_list

    for i in array:
        flatten(i, flat_list)

    return flat_list

def stolen_flatten(array):
    if array == []:
        return array
    if isinstance(array[0], list):
        return stolen_flatten(array[0]) + stolen_flatten(array[1:])
    return array[:1] + stolen_flatten(array[1:])

def main():
    array = [[1, 2, [3]], [4], [5, [6, 7]]]
    flattened_array = [1, 2, 3, 4, 5, 6, 7]
    flat_array = flatten(array, [])
    stolen_flat_array = stolen_flatten(array)

    assert flat_array == flattened_array
    assert stolen_flat_array == flattened_array

if __name__ == "__main__":
    main()
