require "minitest/autorun"
require_relative "smorse"
class TestMeme < Minitest::Test
  def test_sos
    assert_equal smorse("sos"), "...---..."
  end

  def test_daily
    assert_equal smorse("daily"), "-...-...-..-.--"
  end

  def test_programmer
    assert_equal smorse("programmer"), ".--..-.-----..-..-----..-."
  end

  def test_bits
    assert_equal smorse("bits"), "-.....-..."
  end

  def test_three
    assert_equal smorse("three"), "-.....-..."
  end

end
