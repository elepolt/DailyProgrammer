def smorse(word)
  # The sorted morse code alphabet, a->z
  morse_code_alphabet = ['.-','-...','-.-.','-..','.','..-.','--.','....','..','.---','-.-','.-..','--','-.','---','.--.','--.-','.-.','...','-','..-','...-','.--','-..-','-.--','--..']
  alphabet = ('a'..'z').to_a

  # Pair up the actual alphabet with 
  morse_code_dict = {}
  alphabet.each_with_index do |alpha, i|
    morse_code_dict[alpha] =  morse_code_alphabet[i]
  end
  
  # Simply split the word into letters, and grab their morse code value from the dict
  smorsed = ''
  word.split('').each do |letter|
    smorsed += morse_code_dict[letter]
  end

  return smorsed
end