final_scores = []
inputs = [
  'abcde',
  'dbbaCEDbdAacCEAadcB',
  'EbAAdbBEaBaaBBdAccbeebaec'
]
inputs.each do |input|
  round = {}
  scores = input.split('')
  players = scores.collect{|letter| letter.downcase}.uniq
  players.each do |player|
    player_negative = player.upcase
    score = scores.count(player) - scores.count(player_negative)
    round[player] = score
  end
  # Sort by score and add round to final scores
  final_scores.push(round.sort_by{|name, score| -score }.to_h)

  # Shortened .each loop
  # players.each {|p| scores[p] = scores.count(p) - scores.count(p.upcase)}
end


puts final_scores