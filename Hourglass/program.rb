require 'byebug'

# This solution makes the assumption that our input is a square,
# that our hourglass is made up by a a formation of '3-1-3',
# and that the input is at least 3x3.
def hourglass_sum(input)
    max_sum = nil
    input.each_with_index do |y_axis, yidx|
        next if is_edge(yidx, input)
        y_axis.each_with_index do |x_axis, xidx|
            next if is_edge(xidx, y_axis)
            sum = get_hourglass(yidx, xidx, input).sum

            max_sum = sum if max_sum.nil? || max_sum < sum
        end
    end
    max_sum
end

# Since the hourglass is dependent on being a full square, we can always
# skip an iteration if the given index is on an edge
# Note: idx is zero indexed
def is_edge(idx, array)
    idx == 0 || idx == array.length - 1
end

# Given the x/y coords we are at, return the 7 spaces surrounding it
# IE: Get the square minus the 'waist' of the hourglass
def get_hourglass(yidx, xidx, input)
    square = []
    # This simply loops over the +/- 1 coords from our given x/y location
    (yidx-1..yidx+1).each do |y|
        (xidx-1..xidx+1).each do |x|
            # We want to skip [yidx][xidx-1] and [yidx][xidx+1]
            # as those are the two 'waist' pieces of our hourglass
            # and should not be included in the sum
            next if yidx == y && x != xidx
            square.push(input[y][x])
        end
    end
    square
end

require "minitest/autorun"
class TestProgram < Minitest::Test
    def setup
        @input = [
            [0, 3, 0, 0, 0],
            [0, 1, 0, 0, 0],
            [1, 1, 1, 0, 0],
            [0, 0, 2, 4, 4],
            [0, 0, 0, 2, 4]
        ]
    end
    def test_is_edge
        assert_equal(true, is_edge(0, @input))
        assert_equal(true, is_edge(4, @input))
        assert_equal(false, is_edge(1, @input))
        assert_equal(false, is_edge(3, @input))
    end

    def test_get_square
        assert_equal([0, 3, 0, 1, 1, 1, 1], get_hourglass(1, 1, @input))
        assert_equal([1, 0, 0, 4, 0, 2, 4], get_hourglass(3, 3, @input))
        
    end
    def test_program
        assert_equal 11, hourglass_sum(@input)
    end
end